<?php include('header.php'); ?>

<?php //echo form_open_multipart($this->config->item('admin_folder').'/boxes/form/'.$id); ?>

<div class="button_set">
	<input type="submit" value="Save"/>
</div>

<div id="gc_tabs">
	<ul>
		<li><a href="#gc_box_info">Box Information</a></li>
	</ul>
	<div id="gc_box_info">
		<div class="gc_field2">
			<label for="title">Shipping Tracking Number </label>
			<?php echo form_input($shipping); ?>
		</div>

		<div class="gc_field2">
			<label for="link">Remark </label>
			<?php echo form_textarea($link); ?>
		</div>

		<div class="gc_field2">
			<?php echo form_checkbox($new_window); ?> <label><?php echo lang('new_window');?></label>
		</div>

		
	</div>
	
</div>
</form>

<?php include('footer.php'); ?>