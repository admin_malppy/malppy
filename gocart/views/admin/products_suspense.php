<?php include('header.php'); ?>
<?php
		$atts = array(
              'width'      => '1280',
              'height'     => '768',
              'scrollbars' => 'yes',
              'status'     => 'yes',
              'resizable'  => 'yes',
              'screenx'    => '0',
              'screeny'    => '0'
            );
?>
<script type="text/javascript">
function areyousure()
{
	return confirm('<?php echo lang('confirm_delete_product');?>');
}
function areyousure2()
{
	return confirm('Are you sure you want to publish this product?');
}
function view_category_product(id)
{	
	if (id) {
		location.href='<?php echo  site_url($this->config->item('admin_folder').'/products/category/');?>/'+id;
	} else {
		location.href='<?php echo  site_url($this->config->item('admin_folder').'/products');?>';
	}	
}
</script>

<div id="breadcrumb">
	<ul>
    	<li><a href="<?php echo site_url($this->config->item('admin_folder').'/products');?>">Inventory</a></li>
        <?php if ($catid) { ?>
		<li><a href="<?php echo site_url($this->config->item('admin_folder').'/products');?>">Suspended Products</a></li>
		<li class="last"><a href="#"><?php echo $this->Category_model->get_category($catid)->name; ?></a></li>
		<?php } else { ?>
		<li class="last"><a href="#">Suspense</a></li>
		<?php } ?>
    </ul>
</div><!-- End of breadcrumb --> 

<div class="button_set" style="text-align:left;">
	<select name="catid" id="catid" class="gc_tf1" onchange="view_category_product(this.value)">
		<option value="">Show All Products</option>
		<?php
		define('ADMIN_FOLDER', $this->config->item('admin_folder'));
		define('SELECTED_CATID', $catid);
		$this->load->model('Product_model');
		$Product_model = new Product_model;
		function list_categories($cats, $sub='', $Product_model) {	
			foreach ($cats as $cat):?>
			<option id="cat_item_<?php echo $cat['category']->id;?>" value="<?php echo $cat['category']->id;?>" <?php if ($cat['category']->id==SELECTED_CATID)  { ?>selected<?php } ?>><?php echo  $sub.$cat['category']->name; ?> (<?php echo $Product_model->count_products($cat['category']->id); ?>)</option>
			<?php
			if (sizeof($cat['children']) > 0)
			{
				$sub2 = str_replace('-&nbsp;', '&nbsp;', $sub);
					$sub2 .=  '&nbsp;-&nbsp;';
				list_categories($cat['children'], $sub2, $Product_model);
			}
			endforeach;
		}
		$cats_tierd = $this->Category_model->get_categories_tierd();
		
		list_categories($cats_tierd,'',$Product_model);
		?>
	</select>
</div>

<?php echo form_open($this->config->item('admin_folder').'/products/bulk_save', array('id'=>'bulk_form'));?>
	<table class="gc_table" cellspacing="0" cellpadding="0">
		<thead>
			<tr>
				<th class="gc_cell_left">No</th>
				<th style="width:50px;"></th>
				<th>Product <?php echo lang('name');?></th>
				<th style="white-space:nowrap">Original <?php echo lang('sku');?></th>
				<th style="white-space:nowrap">Product Code</th>
				<th style="width:60px;"><?php echo lang('price');?></th>
				<th style="width:90px;"><?php echo lang('saleprice');?></th>
				<th style="width:50px;text-align:center;"><?php echo lang('quantity');?></th>
				<th>Reason</th>
				<th style="width:90px;text-align:center;">Date added</th>
				<th style="width:90px;text-align:center;">Last edited</th>
				<th style="width:90px;text-align:center;">User</th>
				<th class="gc_cell_right"></th>
			</tr>
		</thead>
		<tbody>
		<?php echo (count($products) < 1)?'<tr><td style="text-align:center;" colspan="10">There are currently no suspense products.</td></tr>':''?>
	<?php foreach ($products as $product):?>
			<tr class="gc_row">
				<td><?php echo ++$index; ?></td>
				<td>
				<?php 
                $this_product = $this->Product_model->get_product($product->id);
				
				$this_product->images	= (array)json_decode($this_product->images);
				$this_product->images	= array_values($this_product->images);
				$primary	= $this_product->images[0];

				$user = $this->Product_model->get_admin($product->adminid);
				
				foreach($this_product->images as $image) { if(isset($image->primary)) $primary	= $image; }
                ?>
				<?php if (strpos($primary->filename, 'http') === 0) { ?>
					<img src="<?php echo base_url(); ?>phpthumb/phpThumb.php?src=<?php echo $primary->filename;?>&w=50&h=50&zc=1" alt=""/>
				<?php } else { ?>
					<img src="<?php echo base_url('uploads/images/thumbnails/'.$primary->filename);?>" alt=""/>
				<?php } ?>
				</td>
				<td><?php echo strip_tags($product->name);?></td>
				<td><?php echo $product->num_iid;?></td>
				<td><?php echo $product->sku;?></td>
				<td><?php echo $product->price;?></td>
				<td><?php echo $product->saleprice;?></td>
				<td style="text-align:center;"><?php echo $product->quantity;?></td>
				<td><?php echo $product->reason;?></td>
				<td style="text-align:center;"><?php echo date("d-m-Y",strtotime($product->created));?></td>
				<td style="text-align:center;"><?php echo date("d-m-Y",strtotime($product->updated));?></td>
				<td style="text-align:center;"><?php echo $user;?></td>
				<td class="gc_cell_right list_buttons" style="white-space:nowrap">
					<?php echo anchor_popup(site_url($product->slug), 'Preview', $atts); ?>
					<a href="<?php echo  site_url($this->config->item('admin_folder').'/products/form/'.$product->id);?>"><?php echo lang('edit');?></a>					
					<a href="<?php echo  site_url($this->config->item('admin_folder').'/products/delete_suspense/'.$product->id);?>" onclick="return areyousure();"><?php echo lang('delete');?></a>
				</td>
			</tr>
	<?php endforeach; ?>
		</tbody>
	</table>
</form>

<?php include('footer.php'); ?>