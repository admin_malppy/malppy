<?php include('header.php'); ?>

<table class="gc_table" cellspacing="0" cellpadding="0">
	<thead>
		<tr>
			<th class="gc_cell_left">Ordered On</th>
			<th>Order Number</th>
			<th>Status</th>
			<th class="gc_cell_right"></th>
		</tr>
	</thead>
	<tbody>
	<?php echo (count($orders) < 1)?'<tr><td style="text-align:center;" colspan="6">No order found.</td></tr>':''?>
<?php foreach ($orders as $order): ?>
		<tr>
			<td>
				<?php echo $order->ordered_on; ?>
			</td>
			
			<td>
				<?php echo $order->order_number; ?>
			</td>
			
			<td>
				<?php echo $order->status; ?>
			</td>
			
			<td class="gc_cell_right list_buttons">
				<a href="<?php echo site_url($this->config->item('admin_folder').'/orders/view/'.$order->order_id);?>">View</a>
			</td>
		</tr>
<?php endforeach; ?>
	</tbody>
</table>

<?php include('footer.php');