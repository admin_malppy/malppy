<?php
//print_r($order->contents);

?>

<?php echo form_open($this->config->item('admin_folder').'/orders/pack', array('id'=>'pack_form'));?>
<input type="hidden" id="pack_status" name="pack_status" class="pack_status">
<table class="gc_table" cellspacing="0" cellpadding="0" style="margin:20px;width:96%">
	<thead>
		<tr>
			<th>Product Name</th>
			<th>Product Code</th>
			<!-- <th>Exp No</th> -->
			<th style="width:70px;text-align:center;">Quantity</th>
			<th>Supplier</th>
			<th style="text-align:center;">Weight</th>
			<!-- <th style="text-align:center;">Arrived</th> -->
		</tr>
	</thead>
	<tbody>
		<?php foreach($order->contents as $orderkey=>$product):?>
		<?php $prod = $this->Product_model->get_product($product['id']); ?>
		<?php $total_weight += $prod->weight; ?>
		<input type="hidden" name="action[<?php echo $product['itemid']; ?>][product_id]" value="<?php echo $product['id']; ?>">
		<input type="hidden" name="action[<?php echo $product['itemid']; ?>][item_id]" value="<?php echo $product['itemid']; ?>">
		<input type="hidden" name="action[<?php echo $product['itemid']; ?>][order_id]" value="<?php echo $order->id; ?>">
		
			
		<tr>
			<td><?php echo strip_tags($product['name']);?></td>
			<td><?php echo (trim($product['sku']) != '')?$product['sku']:'';?></td>
			<td style="text-align:center;"><?php echo $product['quantity'];?></td>
			<td><?php echo $prod->seller;?></td>
			<td style="text-align:center;"><?php echo $prod->weight;?></td>
			<!-- <td style="text-align:center;">
					<?php
					$pack = $this->Order_model->get_pack_status($product['id']);
					//echo $product['itemid'];
					$data	= array('name'=>'action['.$product['itemid'].'][pack]', 'value'=>1, 'checked'=>set_checkbox('action['.$product['itemid'].'][pack]', 1, (bool) $pack));
					echo form_checkbox($data);
				?>
			</td> -->
		</tr>
		<?php endforeach;?>
		<tr>
			<td colspan="4">Total Weight</td>
			<td style="text-align:center"><?php echo $total_weight; ?></td>
		</tr>
	</tbody>
</table>
</form>

<div class="button_set" style="margin-right:35px;">
	<a href="#" onclick="$('#pack_status').val(0);$('#pack_form').submit(); return false;">Not Pack</a>
	<!-- <a href="#" onclick="parent.get_order_detail(<?php echo $orderid; ?>); return false;">Not Pack</a> -->
	<a href="#" onclick="$('#pack_status').val(1);$('#pack_form').submit(); return false;">Pack</a>
</div>

