<?php require('header.php'); 
	
//set "code" for searches
if(!$code) {
	$code = '';
} else {
	$code = '/'.$code;
}

function sort_url($by, $sort, $sorder, $code, $admin_folder) {
	if ($sort == $by) {
		if ($sorder == 'asc') {
			$sort	= 'desc';
		} else {
			$sort	= 'asc';
		}
	} else {
		$sort	= 'asc';
	}
	$return = site_url($admin_folder.'/orders/index/'.$by.'/'.$sort.'/'.$code);
	return $return;
}
			

$pagination = '<tr><td class="gc_pagination" colspan="8"><table class="table_nav" style="width:100%" cellpadding="0" cellspacing="0"><tr><td style="width:0px; text-align:left;">';
 	
$pagination .= '</td><td style="text-align:center;">';
 	 	
$pagination .= $pages;
$pagination .= '</td><td style="width:50px; text-align:right;">';
$pagination .= '</td></tr></table></td></tr>';
?>
<?php
$app_key = '21064466';
$secret='c17ec2767f4273219601799105ad5326';
$timestamp=time()."000";
$message = $secret.'app_key'.$app_key.'timestamp'.$timestamp.$secret;
$mysign=strtoupper(hash_hmac("md5",$message,$secret));
setcookie("timestamp",$timestamp);
setcookie("sign",$mysign);
?>
<?php
foreach($orders as $order) {
	$products = $this->Order_model->get_items($order->id);

	foreach($products as $prod) {
		$bstat[$order->id][$prod['id']]['bought'] = $prod['bought'];

		$osellers[$this->Product_model->get_product($prod['id'])->seller]['seller'] = $this->Product_model->get_product($prod['id'])->seller;
		$osellers[$this->Product_model->get_product($prod['id'])->seller]['order_id'][] = $order->id;
		$osellers[$this->Product_model->get_product($prod['id'])->seller]['order_number'][] = $order->order_number;
		$osellers[$this->Product_model->get_product($prod['id'])->seller]['prod_array'][$prod['id']] = $prod['id'];
		$osellers[$this->Product_model->get_product($prod['id'])->seller]['products'][$prod['id']]['bought'] = $prod['bought'];
		$osellers[$this->Product_model->get_product($prod['id'])->seller]['products'][$prod['id']]['name'] = strip_tags($prod['name']);
		$osellers[$this->Product_model->get_product($prod['id'])->seller]['products'][$prod['id']]['id'] = $prod['id'];
		$osellers[$this->Product_model->get_product($prod['id'])->seller]['products'][$prod['id']]['total_quantity'] += $prod['quantity'];
		$osellers[$this->Product_model->get_product($prod['id'])->seller]['products'][$prod['id']]['total_weight'] += $prod['weight'];
		$osellers[$this->Product_model->get_product($prod['id'])->seller]['products'][$prod['id']]['total_price'] += $prod['price'];
	}
}

foreach($osellers as $oseller) {
	$osellers[$oseller['seller']]['prod_str'] = implode("_",$oseller['prod_array']);
}

foreach($bstat as $oid => $bs) {
	foreach($bs as $b) {
		if (in_array(0, $b)) {
			$s['id']		= $oid;
    		$s['status']	= "Pending";    
    		$this->Order_model->save_order($s);
			break;
		}
		$s['id']		= $oid;
    	$s['status']	= "Pending Delivery";    	
    	$this->Order_model->save_order($s);
	}	
}

foreach($orders as $order) {
	$products = $this->Order_model->get_items($order->id);

	foreach($products as $prod) {
		$osellers[$this->Product_model->get_product($prod['id'])->seller]['order_status'][] = $order->status;
	}
}
?>
<script src="http://a.tbcdn.cn/apps/top/x/sdk.js?appkey=21064466"></script>
<script>
TOP.init({
            appKey :<?php echo $app_key;?>,//appkey
            channelUrl : window.location.protocol + '//' + window.location.hostname + '/channel.html'
      });
</script>


<?php echo form_open($this->config->item('admin_folder').'/orders', array('id'=>'search_form')); ?>
	<input type="hidden" name="term" id="search_term" value=""/>
	<input type="hidden" name="start_date" id="start_date" value=""/>
	<input type="hidden" name="end_date" id="end_date" value=""/>
</form>

<?php echo form_open($this->config->item('admin_folder').'/orders/export', array('id'=>'export_form')); ?>
	<input type="hidden" name="term" id="export_search_term" value=""/>
	<input type="hidden" name="start_date" id="export_start_date" value=""/>
	<input type="hidden" name="end_date" id="export_end_date" value=""/>
</form>

<!-- <?php echo form_open($this->config->item('admin_folder').'/orders/bulk_delete', array('id'=>'delete_form')); ?> -->
<?php echo form_open($this->config->item('admin_folder').'/orders/bulk_export', array('id'=>'bulk_export_form')); ?>
<div id="breadcrumb">
	<ul>
    	<li><a href="<?php echo site_url($this->config->item('admin_folder').'/Orders');?>">Sales & Purchases</a></li>
    	<li class="last"><a href="<?php echo site_url($this->config->item('admin_folder').'/Orders');?>">Orders</a></li>
    </ul>
</div><!-- End of breadcrumb --> 
<br>

<script>
    /*TOP.ui("minicart", {
      container:".mini-cart",
      position:"top"
    });*/
</script>

<script>
    /*TOP.ui("login-btn", {
      container:".top-login-btn-container",
      type:"3,1",
      callback:{
         login:function(user){alert(user)},
         logout:function(){}
    }
    });*/
	
	//TOP.ui('login-btn', {
	//container: '#taobao-login',//组件容器
	//type:"3,1",
	//showAvatar: true, //是否显示头像
	//showUserLink: true,//是否生成链接 
	//showLogout: true, //是否显示退出
	//callback: {
	//	login:null, //登录成功回调函数
	//	logout: null//退出成功回调函数
    //    }})
	
</script>


<!-- <div class="button_set">
	<div id="taobao-login" style="float:left;margin-right:10px;"></div>
	<div class="mini-cart" style="float:left;"></div>
	
</div> -->

<!-- <div style="clear:both;height:20px;"></div> -->

<table cellspacing="0" cellpadding="0">
<?php echo $pagination; ?>
</table>
<br>

<h2>Orders</h2>
<table class="gc_table" cellspacing="0" cellpadding="0">
    <thead>
		<tr>
			<th class="gc_cell_left"><input type="checkbox" id="gc_check_all" /></th>
			<th><a href="<?php echo sort_url('order_number', $sort_by, $sortorder, $code, $this->config->item('admin_folder')); ?>"><?php echo lang('order')?> Number</a> <?php if ($sort_by=="order_number" && $sortorder=="desc") { ?><img src="<?php echo base_url('images/sortdown.png');?>" border="0"><?php } ?><?php if ($sort_by=="order_number" && $sortorder=="asc") { ?><img src="<?php echo base_url('images/sortup.png');?>" border="0"><?php } ?></th>
			<th><a href="<?php echo sort_url('ordered_on', $sort_by, $sortorder, $code, $this->config->item('admin_folder')); ?>"><?php echo lang('ordered_on')?></a> <?php if ($sort_by=="ordered_on" && $sortorder=="desc") { ?><img src="<?php echo base_url('images/sortdown.png');?>" border="0"><?php } ?><?php if ($sort_by=="ordered_on" && $sortorder=="asc") { ?><img src="<?php echo base_url('images/sortup.png');?>" border="0"><?php } ?></th>
			<!-- <th><a href="<?php echo sort_url('status', $sort_by, $sortorder, $code, $this->config->item('admin_folder')); ?>"><?php echo lang('status')?></a>  <?php if ($sort_by=="status" && $sortorder=="desc") { ?><img src="<?php echo base_url('images/sortdown.png');?>" border="0"><?php } ?><?php if ($sort_by=="status" && $sortorder=="asc") { ?><img src="<?php echo base_url('images/sortup.png');?>" border="0"><?php } ?></th> -->
			<!-- <th><a href="<?php echo sort_url('bill_lastname', $sort_by, $sortorder, $code, $this->config->item('admin_folder')); ?>"><?php echo lang('bill_to')?></a></th>
			<th><a href="<?php echo sort_url('ship_lastname', $sort_by, $sortorder, $code, $this->config->item('admin_folder')); ?>"><?php echo lang('ship_to')?></a></th> -->
			<th style="text-align:center;">Products</th>
			<th style="text-align:center;">Quantity</th>
			<th>Sales</th>
			<th>Cost</th>
			<!-- <th style="text-align:center;">Quantity</th> -->
			<!-- <th><a href="<?php echo sort_url('notes', $sort_by, $sortorder, $code, $this->config->item('admin_folder')); ?>"><?php echo lang('notes')?></a></th> -->
			<th class="gc_cell_right" style="text-align:center;">detail</th>
	    </tr>
		
	</thead>
 	<tfoot>
    <?php //echo $pagination?>
	</tfoot>
    <tbody>
		<!-- <tr>
			<td colspan="8" class="gc_table_tools"> -->
				<!-- <div class="gc_order_delete">
					<a onclick="submit_form();" class="button"><?php echo lang('form_delete')?></a>
				</div> -->
				<!-- <div class="gc_order_search">
					<?php echo lang('from')?> <input id="start_top"  value="" class="gc_tf1" type="text" /> 
						<input id="start_top_alt" type="hidden" name="start_date" />
					<?php echo lang('to')?> <input id="end_top" value="" class="gc_tf1" type="text" />
						<input id="end_top_alt" type="hidden" name="end_date" /> -->
					<!-- <?php //echo lang('term')?>Status <input id="top" type="text" class="gc_tf1" name="term" value="" />  -->
					<!-- <span class="button_set"><a href="#" onclick="do_search('top'); return false;"><?php echo lang('search')?></a> -->
					<!-- <a href="#" onclick="do_export('top'); return false;"><?php echo lang('xml_export')?></a></span> -->
					<!-- </span>
				</div> -->
			<!-- </td>
		</tr> -->
	<?php echo (count($orders) < 1)?'<tr><td style="text-align:center;" colspan="8">'.lang('no_orders') .'</td></tr>':''?>
    <?php foreach($orders as $order): ?>
	<?php
	if ($order->batch_id) continue;
	foreach($this->Order_model->get_items($order->id) AS $order_item) {
		$total_quantity[$order->id] += $order_item[quantity];
		
		$total_cost[$order->id] += $order_item['base_price']*$order_item[quantity];

		++$total_product[$order->id];
	}
	?>
	<tr>
		<td><input name="order[]" type="checkbox" value="<?php echo $order->id; ?>" class="gc_check"/></td>
		<td><?php echo $order->order_number; ?></td>
		<td style="white-space:nowrap"><?php echo date('m/d/y h:i a', strtotime($order->ordered_on)); ?></td>
		<!-- <td>
			<div id="status_container_<?php echo $order->id; ?>" style="position:relative; font-weight:bold; padding-left:0px;">
				<span id="status_<?php echo $order->id; ?>" class="<?php echo url_title($order->status); ?>"><?php echo $order->status; ?></span>
				<img style="position:absolute; left:2px;" src="<?php echo base_url('images/edit.gif');?>" alt="edit" title="edit" onclick="edit_status(<?php echo $order->id; ?>)"/>
			</div>
			<div id="edit_status_<?php echo $order->id; ?>" style="display:none; position:relative; white-space:nowrap;">
				<?php
				
				echo form_dropdown('status', $this->config->item('order_statuses'), $order->status, 'id="status_form_'.$order->id.'"');
				?>
				<a class="button" onClick="save_status(<?php echo $order->id; ?>)"><?php echo lang('form_save')?></a>
			</div>
		</td> -->
		<!-- <td style="white-space:nowrap"><?php echo $order->bill_lastname.', '.$order->bill_firstname; ?></td>
		<td style="white-space:nowrap"><?php echo $order->ship_lastname.', '.$order->ship_firstname; ?></td> -->
		<td style="text-align:center;"><?php echo $total_product[$order->id]; ?></td>	
		<td style="text-align:center;"><?php echo $total_quantity[$order->id]; ?></td>
		<td>$<?php echo $order->total; ?></td>
		<td>$<?php echo $total_cost[$order->id]; ?></td>			
		<!-- <td><div class="MainTableNotes"><?php echo $order->notes; ?></div></td> -->
		<td class="gc_cell_right list_buttons" style="text-align:center;">
			<!-- <a href="<?php echo site_url($this->config->item('admin_folder').'/orders/view/'.$order->id);?>"><?php echo lang('form_view')?></a>&nbsp; -->
			<?php //if ($order->status!="Pending Delivery") { ?>
			<!-- <a href="<?php echo site_url($this->config->item('admin_folder').'/orders/taobao/'.$order->id);?>">More</a> -->
			<a class="button" id="button_<?php echo $order->id; ?>" onclick="get_order_detail('<?php echo $order->id; ?>')">Expand</a>
			<?php //} ?>
		</td>
	</tr>
	<tr id="more_<?php echo $order->id; ?>" style="display:none">
		<td style="background-color:#eee" colspan="8" id="table_<?php echo $order->id; ?>"></td>
	</tr>
    <?php endforeach; ?>
		<!-- <tr>
			<td colspan="8" class="gc_table_tools">
				<div class="gc_order_delete">
					<a onclick="submit_form();" class="button"><?php echo lang('form_delete')?></a>
				</div>
				<div class="gc_order_search">
					<?php echo lang('from')?> <input id="start_bottom"  value="" class="gc_tf1" type="text" /> 
						<input id="start_bottom_alt" type="hidden" name="start_date" />
					<?php echo lang('to')?> <input id="end_bottom" value="" class="gc_tf1" type="text" />
						<input id="end_bottom_alt" type="hidden" name="end_date" />
					<?php //echo lang('term')?>Status <input id="top" type="text" class="gc_tf1" name="term" value="" /> 
					<span class="button_set"><a href="#" onclick="do_search('bottom'); return false;"><?php echo lang('search')?></a>
					<a href="#" onclick="do_export('bottom'); return false;"><?php echo lang('xml_export')?></a></span>
					</span>
				</div>
			</td>
		</tr> -->
    </tbody>
</table>
</form>
<div class="button_set" style="text-align:left;">
	<a href="#" onclick="$('#bulk_export_form').submit(); return false;">Export Order</a>
</div>


<script type="text/javascript">
$(document).ready(function(){
	
	$('#gc_check_all').click(function(){
		if(this.checked)
		{
			$('.gc_check').attr('checked', 'checked');
		}
		else
		{
			 $(".gc_check").removeAttr("checked"); 
		}
	});
	
	// set the datepickers individually to specify the alt fields
	$('#start_top').datepicker({dateFormat:'mm-dd-yy', altField: '#start_top_alt', altFormat: 'yy-mm-dd'});
	$('#start_bottom').datepicker({dateFormat:'mm-dd-yy', altField: '#start_bottom_alt', altFormat: 'yy-mm-dd'});
	$('#end_top').datepicker({dateFormat:'mm-dd-yy', altField: '#end_top_alt', altFormat: 'yy-mm-dd'});
	$('#end_bottom').datepicker({dateFormat:'mm-dd-yy', altField: '#end_bottom_alt', altFormat: 'yy-mm-dd'});
});

function do_search(val)
{
	$('#search_term').val($('#'+val).val());
	$('#start_date').val($('#start_'+val+'_alt').val());
	$('#end_date').val($('#end_'+val+'_alt').val());
	$('#search_form').submit();
}

function do_export(val)
{
	$('#export_search_term').val($('#'+val).val());
	$('#export_start_date').val($('#start_'+val+'_alt').val());
	$('#export_end_date').val($('#end_'+val+'_alt').val());
	$('#export_form').submit();
}

function submit_form()
{
	if($(".gc_check:checked").length > 0)
	{
		if(confirm('<?php echo lang('confirm_order_delete') ?>'))
		{
			$('#delete_form').submit();
		}
	}
	else
	{
		alert('<?php echo lang('error_no_orders_selected') ?>');
	}
}

function edit_status(id)
{
	$('#status_container_'+id).hide();
	$('#edit_status_'+id).show();
}

function save_status(id)
{
	$.post("<?php echo site_url($this->config->item('admin_folder').'/orders/edit_status'); ?>", { id: id, status: $('#status_form_'+id).val()}, function(data){
		$('#status_'+id).html('<span class="'+data+'">'+$('#status_form_'+id).val()+'</span>');
	});
	
	$('#status_container_'+id).show();
	$('#edit_status_'+id).hide();	
}
</script>

<script type="text/javascript">
function get_order_detail(orderid)
{
	if ($('#button_'+orderid).html() == '<span class="ui-button-text">Hide</span>') {
		$('#more_'+orderid).hide();
		$('#button_'+orderid).html('<span class="ui-button-text">Expand</span>');
	} else {
		$.ajax('<?php echo site_url($this->config->item('admin_folder').'/orders/items');?>/'+orderid, { data: {},
    		type: "POST",
    		beforeSend: function() {
        		$('#button_'+orderid).html('<span class="ui-button-text">Please wait...</span>');
    		},
    		error: function() {
        		alert("error"); 
    		},
    		success: function(data) {
        		$('#table_'+orderid).html(data);
				$('#more_'+orderid).show();
				$('#button_'+orderid).html('<span class="ui-button-text">Hide</span>');
				$('.button_set a').button();

				$('.paid_button').click(function(){
					var itemid = $(this).attr('itemid');
					var paid_status = 0;
					
					if(!$(this).attr('checked')) {
						$('#buy_'+itemid).show();
						paid_status = 0;
					} else {
						if (areyousure()) {
							$('#buy_'+itemid).hide();
							paid_status = 1;
						} else {
							$(this).removeAttr('checked'); 
							paid_status = 0;
						}
					}

					$.post('<?php echo site_url($this->config->item('admin_folder').'/orders/paid');?>/'+itemid,{paid_status:paid_status},function(data){
		
					});
				});

    		}
		});
	}
}

function get_seller_detail(seller, index)
{
	if ($('#button_'+index).html() == '<span class="ui-button-text">Hide</span>') {
		$('#view_'+index).hide();
		$('#button_'+index).html('<span class="ui-button-text">More</span>');
	} else {
		$.ajax('<?php echo site_url($this->config->item('admin_folder').'/orders/taobao_seller');?>/'+seller, { data: {},
    		type: "POST",
    		beforeSend: function() {
        		$('#button_'+index).html('<span class="ui-button-text">Please wait...</span>');
    		},
    		error: function() {
        		alert("error"); 
    		},
    		success: function(data) {
        		$('#table_'+index).html(data);
				$('#view_'+index).show();
				$('#button_'+index).html('<span class="ui-button-text">Hide</span>');
				$('.button_set a').button();
				$(".top-login-link").removeClass("ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only")

				$('.paid_button').click(function(){
					var itemid = $(this).attr('itemid');
					var paid_status = 0;
					
					if(!$(this).attr('checked')) {
						$('#buy_'+itemid).show();
						paid_status = 0;
					} else {
						if (areyousure()) {
							$('#buy_'+itemid).hide();
							paid_status = 1;
						} else {
							$(this).removeAttr('checked'); 
							paid_status = 0;
						}
					}

					$.post('<?php echo site_url($this->config->item('admin_folder').'/orders/paid');?>/'+itemid,{paid_status:paid_status},function(data){
		
					});
				});

    		}
		});
	}
}
</script>
<script>
	$('#notify').colorbox({
					width: '550px',
					height: '600px',
					iframe: true
		});

	function areyousure()
	{
	return confirm('Are you sure that you have paid this product to seller?');
	}
</script>
<?php include('footer.php'); ?>

<?php exit; ?>


<h2>List by Supplier</h2>
<table class="gc_table" cellspacing="0" cellpadding="0">
    <thead>
		<tr>
			<th class="gc_cell_left">Supplier Name</th>
			<th style="text-align:center;">Total Order Number</th>
			<th style="text-align:center;">Total Products</th>
			<th style="text-align:center;">Total Quantity</th>
			<!-- <th style="text-align:center;">Total Amount</th> -->
			<th>Status</th>
			<th class="gc_cell_right"></th>
	    </tr>
	</thead>
    <tbody>
		
	<?php echo (count($osellers) < 1)?'<tr><td style="text-align:center;" colspan="7">No order found.</td></tr>':''?>
    <?php foreach($osellers as $oseller): ?>
	<?php
		$quantity_ttl = 0; $price_ttl = 0;
		foreach($oseller['products'] as $oprod) {
			$quantity_ttl += $oprod['total_quantity'];
			$price_ttl += $oprod['total_price'];
		}
		++$index;

		
		
		foreach ($oseller['products'] AS $n) {
			if ($n['bought']==0) {
				$this_status = "Pending";
				break;
			}
			$this_status = "Pending Delivery";
		}
		

		/*if (count($oseller[order_status]) != count(array_unique($oseller[order_status]))) {
			$this_status = "Pending";
		} else {
			if (count($oseller[order_status]) == 1 && $oseller[order_status][0] == "Pending") {
				$this_status = "Pending";
			} else {
				$this_status = "Pending Delivery";
			}
		}*/
	?>
	<tr>
		<td><?php echo strip_tags($oseller['seller']); ?></td>
		<td align="center"><?php echo count($oseller['order_number']); ?></td>
		<td align="center"><?php echo count($oseller['products']); ?></td>
		<td align="center"><?php echo $quantity_ttl; ?></td>
		<!-- <td align="center"><?php echo number_format($price_ttl,2,'.',''); ?></td> -->
		<td><?php echo $this_status; ?></td>
		<td class="gc_cell_right list_buttons">
			<!-- <a href="<?php echo site_url($this->config->item('admin_folder').'/orders/view/33');?>">Button</a>&nbsp; -->
			<a class="button" id="button_<?php echo $index; ?>" onclick="get_seller_detail('<?php echo $oseller['prod_str']; ?>', '<?php echo $index; ?>')">More</a>
		</td>
	</tr>
	<tr id="view_<?php echo $index; ?>" style="display:none">
		<td style="background-color:#eee" colspan="6" id="table_<?php echo $index; ?>"></td>
	</tr>
	<?php endforeach; ?>
    </tbody>
</table>

<br><br><br><br>