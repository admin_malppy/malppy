<?php  
set_time_limit (50000);
session_start();

// ensure there was a thumb in the URL
if (!$_REQUEST['filename']) {
	error('no source file');
}

$size = '1920x1080';
list($width,$height) = explode('x',$size);

$image = $_REQUEST['filename'];

if (strpos($_REQUEST['filename'], 'http') === 0) {
	
} else {
	$image = $_SERVER['DOCUMENT_ROOT'].'/uploads/images/full/'.$_REQUEST['filename'];
}


// generate the thumbnail
require($_SERVER['DOCUMENT_ROOT'].'/phpthumb/phpthumb.class.php');
$phpThumb = new phpThumb();
//$phpThumb->setSourceFilename($_SERVER['DOCUMENT_ROOT'].'/uploads/images/full/'.$image);
$phpThumb->setSourceFilename($image);
$phpThumb->setParameter('w',$width);
$phpThumb->setParameter('h',$height);

if (!$phpThumb->GenerateThumbnail()) {
	error('cannot generate thumbnail');
}

$saved_file = $_SERVER['DOCUMENT_ROOT'].'/uploads/wysiwyg/'.$_REQUEST['num_iid'].'/'.basename($_REQUEST['id']).'.jpg';
$saved_filethumb = $_SERVER['DOCUMENT_ROOT'].'/uploads/wysiwyg/'.$_REQUEST['num_iid'].'/'.basename($_REQUEST['id']).'_thumb.jpg';

mkpath(dirname($saved_file),true);

// write the file
if (!$phpThumb->RenderToFile($saved_file)) {
	error('cannot save thumbnail');
} else {
	$phpThumb = new phpThumb();
	//$phpThumb->setSourceFilename($_SERVER['DOCUMENT_ROOT'].'/uploads/images/full/'.$image);
	$phpThumb->setSourceFilename($image);
	$phpThumb->setParameter('w',100);
	$phpThumb->setParameter('h',100);
	$phpThumb->GenerateThumbnail();
	$phpThumb->RenderToFile($saved_filethumb);
}

// ensure the image file exists
if (file_exists($saved_file)) {
	echo "success";
}

// basic error handling
function error($error) {
	header("HTTP/1.0 404 Not Found");
	echo '<h1>Not Found</h1>';
	echo '<p>The image you requested could not be found.</p>';
	echo "<p>An error was triggered: <b>$error</b></p>";
	exit();
}
//recursive dir function
function mkpath($path, $mode){
    is_dir(dirname($path)) || mkpath(dirname($path), $mode);
    return is_dir($path) || @mkdir($path,0777,$mode);
}
?>