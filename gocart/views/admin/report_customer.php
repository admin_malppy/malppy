<?php require('header.php'); ?>

<div id="breadcrumb">
	<ul>
    	<li><a href="<?php echo site_url($this->config->item('admin_folder').'/reports');?>">Reports</a></li>
        <li class="last"><a href="<?php echo site_url($this->config->item('admin_folder').'/reports_customer');?>">Customer Reports</a></li>
    </ul>
</div><!-- End of breadcrumb --> 

<h2 style="margin:10px 0px 10px 0px; padding:0px;">Sales Customer List</h2>
<div id="stock_container">
	<table class="gc_table" cellspacing="0" cellpadding="0">
	<thead>
		<tr>
			<?php /*<th>ID</th> uncomment this if you want it*/ ?>
			<th class="gc_cell_left" width="10%">Code</th>
			<th>Customer Name</th>
			<th width="15%">Registered?</th>
			<th width="15%" class="gc_cell_right">Seller</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($orders as $a=>$b):?>
		<?php $cust = $this->Customer_model->get_customer($a); ?>
		<tr class="gc_row">
			<?php /*<td style="width:16px;"><?php echo  $customer->id; ?></td>*/?>
			<td><?php echo  $cust->code; ?></td>
			<td><?php echo  $b['name']; ?></td>
			<td><?php echo  ($cust->code) ? "Yes": "No"; ?></td>
			<td class="gc_cell_right"><?php echo $b->seller; ?></a></td>
		</tr>
		<?php endforeach;?>
	</tbody>
</table>
</div>

<?php include('footer.php'); ?>