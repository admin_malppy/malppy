<?php include('header.php'); ?>

<?php
$order_count = $this->Order_model->get_orders_count('today');
$item_count = $this->Order_model->get_items_count('today');
$item_summary = $this->Order_model->get_items_summary('today');

$pending_order_count = $this->Order_model->get_pending_orders_count();
$pending_item_count = $this->Order_model->get_pending_items_count();
$pending_item_summary = $this->Order_model->get_pending_items_summary();
?>

<div id="breadcrumb">
	<ul>
		<li><a href="<?php echo site_url($this->config->item('admin_folder').'/orders');?>">Sales</a></li>
       	<li class="last"><a href="<?php echo site_url($this->config->item('admin_folder').'/orders/mini_status');?>">Mini Status</a></li>
    </ul>
</div><!-- End of breadcrumb --> 
<br>

<table class="gc_table" cellspacing="0" cellpadding="0" style="width:800px;">
    <thead>
		<tr>
			<th class="gc_cell_left" style="width:25%;text-align:center;">Today Order</th>
			<th style="width:25%;text-align:center;">Today Sales Volume</th>
			<th style="width:25%;text-align:center;">Today Sales Figures</th>
			<th class="gc_cell_right" style="width:25%;text-align:center;">Est. Weight</th>
	    </tr>
	</thead>

    <tbody>
		<tr>
			<td  class="gc_cell_left" style="text-align:center;font-size:14px;"><?php echo $order_count; ?></td>
			<td style="text-align:center;font-size:14px;"><?php echo $item_count; ?></td>
			<td style="text-align:center;font-size:14px;">BND <?php echo number_format($item_summary['price']); ?></td>
			<td class="gc_cell_right" style="text-align:center;font-size:14px;"><?php echo number_format($item_summary['weight']); ?>kg</td>
		</tr>	
    </tbody>
</table>
<br /><br />

<p>&nbsp;</p>

<table class="gc_table" cellspacing="0" cellpadding="0" style="width:800px;">
    <thead>
		<tr>
			<th class="gc_cell_left" style="width:25%;text-align:center;">Pending Order</th>
			<th style="width:25%;text-align:center;">Pending Sales Volume</th>
			<th style="width:25%;text-align:center;">Pending Sales Figures</th>
			<th class="gc_cell_right" style="width:25%;text-align:center;">Est. Weight</th>
	    </tr>
	</thead>

    <tbody>
		<tr>
			<td  class="gc_cell_left" style="text-align:center;font-size:14px;"><?php echo $pending_order_count; ?></td>
			<td style="text-align:center;font-size:14px;"><?php echo $pending_item_count; ?></td>
			<td style="text-align:center;font-size:14px;">BND <?php echo number_format($pending_item_summary['price']); ?></td>
			<td class="gc_cell_right" style="text-align:center;font-size:14px;"><?php echo number_format($pending_item_summary['weight']); ?>kg</td>
		</tr>
    </tbody>
</table>

<?php include('footer.php'); ?>
