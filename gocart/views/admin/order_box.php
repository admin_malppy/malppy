<?php require('header.php'); 

function sort_url($by, $sort, $sorder, $code, $admin_folder)
{
	if ($sort == $by)
	{
		if ($sorder == 'asc')
		{
			$sort	= 'desc';
		}
		else
		{
			$sort	= 'asc';
		}
	}
	else
	{
		$sort	= 'asc';
	}
	$return = site_url($admin_folder.'/orders/index/'.$by.'/'.$sort.'/'.$code);
	return $return;
}	
?>

<div id="breadcrumb">
	<ul>
    	<li><a href="<?php echo site_url($this->config->item('admin_folder').'/Orders');?>">Sales & Purchases</a></li>
    	<li class="last"><a href="<?php echo site_url($this->config->item('admin_folder').'/Orders/box');?>">Box Management</a></li>
    </ul>
</div><!-- End of breadcrumb --> 
<br>

<?php if (!$box_id) { ?>

<h2>Box Management</h2>

<?php echo form_open($this->config->item('admin_folder').'/orders/box', array('id'=>'search_form')); ?>
<div id="search_form" style="margin-bottom:10px">
	<input class="gc_tf1" type="text" name="term" id="search_term" value=""/>
	<input class="button" type="submit" value="Search"/>
</div>
</form>

<div class="button_set">
	<div style="text-align:left;float:left;margin-top:8px;font-size:15px"><b>Currenly there is: <?php echo count($boxes); ?> box</b></div>
	
	<div style="text-align:left;float:right">
	<!-- <a class="add_box" href="#" onclick="return false;">Add Box</a> -->
	<a href="<?php echo site_url($this->config->item('admin_folder').'/orders/new_box'); ?>">Add Box</a>
	</div>
</div>
	
<div style="clear:both"><br></div>

<?php echo form_open($this->config->item('admin_folder').'/logistic/bulk_save', array('id'=>'bulk_form'));?>

<table class="gc_table" cellspacing="0" cellpadding="0">
    <thead>
		<tr>
			<th class="gc_cell_left">Box Number</th>
			<th>Created Date</th>
			<th style="text-align:center;">Total Order</th>
			<th style="text-align:center;">Total Item</th>
			<th style="text-align:center;">Quantity</th>
			<th style="text-align:center;">Net Weight (kg)</th>
			<th class="gc_cell_right"></th>
	    </tr>
	</thead>
    <tbody>			
		<?php echo (count($boxes) < 1)?'<tr><td style="text-align:center;" colspan="8">No box found.</td></tr>':''?>
 	   	<?php foreach($boxes as $box): ?>
		<tr>
			<td><?php echo $box->box_number; ?></td>
			<td><?php echo $box->created_on; ?></td>
			<td style="text-align:center;"><?php echo $box->total_order; ?></td>
			<td style="text-align:center;"><?php echo $box->total_item; ?></td>
			<td style="text-align:center;"><?php echo $box->quantity; ?></td>
			<td style="text-align:center;"><?php echo $box->total_weight; ?></td>
			<td class="gc_cell_right list_buttons">
				<a href="<?php echo site_url($this->config->item('admin_folder').'/orders/box/'.$box->id);?>">Manage Box</a>
			</td>
		</tr>
   		<?php endforeach; ?>

    </tbody>
</table>

</form>

<?php } else { ?>

<h2><?php echo $box->box_number; ?></h2>

<?php echo form_open($this->config->item('admin_folder').'/orders/invoice_slip/'.$box->id, array('id'=>'ship_form'));?>
<div id="gc_tabs">
	<ul>
		<li><a href="#gc_shipping_info">Shipping Information</a></li>
	</ul>
	<div id="gc_shipping_info">
		<div class="gc_field2">
			<label for="title">Shipping Tracking Number </label>
			<?php if ($box->status!="shipping") { ?>
			<?php echo form_input(array('name'=>'shipping_number', 'value'=>'', 'class'=>'gc_tf1'));?></td>
			<?php } else { ?>
			: &nbsp<?php echo $shipment->shipment_number; ?>
			<?php } ?>
		</div>

		<div class="gc_field2">
			<label for="remark" style="vertical-align:top">Remark </label>
			<?php if ($box->status!="shipping") { ?>
			<?php
			$data	= array('id'=>'shipping_remark', 'name'=>'shipping_remark', 'value'=>set_value('shipping_remark', $shipping_remark), 'class'=>'gc_tf1', 'style'=>'height:80px');
			echo form_textarea($data);
			?>
			<?php } else { ?>
			: &nbsp<?php echo $shipment->remark; ?>
			<?php } ?>
		</div>

		<div class="button_set" style="margin-left:270px;text-align:left">
			<!-- <a href="#" onclick="return false;">Save & Preview Invoice</a> -->
			<?php if ($box->status!="shipping") { ?>
			<a onclick="$('#ship_form').trigger('submit');" class="button">Save & Preview Invoice</a>
			<?php } else { ?>
			<a onclick="location.href='<?php echo site_url('admin/orders/invoice_slip/'.$box->id.'/view');?>'"" class="button" style="margin-left:-270px;">View Invoice</a>
			<?php } ?>
		</div>
	</div>	
</div>
</form>

<div style="height:30px;"></div>

<table class="gc_table" cellspacing="0" cellpadding="0">
    <thead>
		<tr>
			<th class="gc_cell_left"><a href="<?php echo sort_url('order_number', $sort_by, $sortorder, $code, $this->config->item('admin_folder')); ?>">Order ID</a> <?php if ($sort_by=="order_number" && $sortorder=="desc") { ?><img src="<?php echo base_url('images/sortdown.png');?>" border="0"><?php } ?><?php if ($sort_by=="order_number" && $sortorder=="asc") { ?><img src="<?php echo base_url('images/sortup.png');?>" border="0"><?php } ?></th>
			<th><a href="<?php echo sort_url('ordered_on', $sort_by, $sortorder, $code, $this->config->item('admin_folder')); ?>"><?php echo lang('ordered_on')?></a> <?php if ($sort_by=="ordered_on" && $sortorder=="desc") { ?><img src="<?php echo base_url('images/sortdown.png');?>" border="0"><?php } ?><?php if ($sort_by=="ordered_on" && $sortorder=="asc") { ?><img src="<?php echo base_url('images/sortup.png');?>" border="0"><?php } ?></th>
				<th style="text-align:center;">Product</th>
			<th style="text-align:center;">Quantity</th>
			<th style="text-align:center;">Total Weight</th>
			<th style="text-align:center;">Status</th>
			<th class="gc_cell_right"></th>
	    </tr>
		
	</thead>
    <tbody>
	<?php echo (count($orders) < 1)?'<tr><td style="text-align:center;" colspan="8">No order found.</td></tr>':''?>
	<?php foreach($orders as $order): ?>
	<?php	
	foreach($this->Order_model->get_items($order->order_id) AS $order_item) {
		$total_quantity[$order->order_id] += $order_item[quantity];
		$total_weight[$order->order_id] += $order_item[weight];
		
		$total_cost[$order->order_id] += $order_item['base_price']*$order_item[quantity];

		++$total_product[$order->order_id];
		
	}
	?>
	<tr>
		<td><?php echo $order->order_number; ?></td>
		<td style="white-space:nowrap"><?php echo date('m/d/y h:i a', strtotime($order->ordered_on)); ?></td>
		<td style="text-align:center;"><?php echo $total_product[$order->order_id]; ?></td>	
		<td style="text-align:center;"><?php echo $total_quantity[$order->order_id]; ?></td>
		<td style="text-align:center;"><?php echo $total_weight[$order->order_id]; ?></td>
		<td style="text-align:center;"><?php echo ($order->is_packed) ? "packed" : "not pack" ;?></td>		

		<td class="gc_cell_right list_buttons">
			<a class="button" id="button_<?php echo $order->order_id; ?>" onclick="get_order_detail('<?php echo $order->order_id; ?>')">Expand</a>
			<?php if ($box->status!="shipping") { ?>
			&nbsp;&nbsp;&nbsp;<select class="gc_tf1" style="margin-top:5px;" onchange="grouping(this.value,'<?php echo $order->order_id; ?>')">
				<option value="">Group to</option>
				<?php foreach($boxes as $box): ?>
				<?php if ($box->id == $box_id) continue; ?>
				<?php if ($box->shipment_id) continue; ?>
				<option value="<?php echo $box->id; ?>"><?php echo $box->box_number; ?></option>
				<?php endforeach; ?>
			</select>
			<?php } ?>
		</td>
	</tr>
	<tr id="more_<?php echo $order->order_id; ?>" style="display:none">
		<td style="background-color:#eee" colspan="8" id="table_<?php echo $order->order_id; ?>"></td>
	</tr>
    <?php endforeach; ?>
		
    </tbody>
</table>

<?php } ?>


<script type="text/javascript">
$(document).ready(function(){
	
	// set the datepickers individually to specify the alt fields
	$('#shipped_on').datepicker({dateFormat:'mm-dd-yy', altField: '#start_top_alt', altFormat: 'yy-mm-dd'});
});
</script>
<script type="text/javascript">
function get_order_list(orderid)
{
	if ($('#button_'+orderid).html() == '<span class="ui-button-text">Hide</span>') {
		$('#more_'+orderid).hide();
		$('#button_'+orderid).html('<span class="ui-button-text">Expand</span>');
	} else {
		$.ajax('<?php echo site_url($this->config->item('admin_folder').'/orders/shipping');?>/'+orderid, { data: {orderid:orderid},
    		type: "POST",
    		beforeSend: function() {
        		$('#button_'+orderid).html('<span class="ui-button-text">Please wait...</span>');
    		},
    		error: function() {
        		alert("error"); 
    		},
    		success: function(data) {
        		$('#table_'+orderid).html(data);
				$('#more_'+orderid).show();
				$('#button_'+orderid).html('<span class="ui-button-text">Hide</span>');
				$('.button_set a').button();
    		}
		});
	}
}
function get_new_order_list(orderid)
{
	if ($('#button_'+orderid).html() == '<span class="ui-button-text">Cancel</span>') {
		$('#more_'+orderid).hide();
		$('#button_'+orderid).html('<span class="ui-button-text">Group Order</span>');
	} else {

		if ($('#shipno').val()=="") {
			alert("Please enter shipping number before continue.");
			return;
		}

		$.ajax('<?php echo site_url($this->config->item('admin_folder').'/orders/shipping');?>/'+orderid, { data: {shipno:$('#shipno').val(),shipped_on:$('#shipped_on').val(),new:'yes'},
    		type: "POST",
    		beforeSend: function() {
        		$('#button_'+orderid).html('<span class="ui-button-text">Please wait...</span>');
    		},
    		error: function() {
        		alert("error"); 
    		},
    		success: function(data) {
        		$('#table_'+orderid).html(data);
				$('#more_'+orderid).show();
				$('#button_'+orderid).html('<span class="ui-button-text">Cancel</span>');
				$('.button_set a').button();
    		}
		});
	}
}

function get_order_detail(orderid)
{
	if ($('#button_'+orderid).html() == '<span class="ui-button-text">Hide</span>') {
		$('#more_'+orderid).hide();
		$('#button_'+orderid).html('<span class="ui-button-text">Expand</span>');
	} else {
		$.ajax('<?php echo site_url($this->config->item('admin_folder').'/orders/items');?>/'+orderid, { data: {},
    		type: "POST",
    		beforeSend: function() {
        		$('#button_'+orderid).html('<span class="ui-button-text">Please wait...</span>');
    		},
    		error: function() {
        		alert("error"); 
    		},
    		success: function(data) {
        		$('#table_'+orderid).html(data);
				$('#more_'+orderid).show();
				$('#button_'+orderid).html('<span class="ui-button-text">Hide</span>');
				$('.button_set a').button();

				$('.paid_button').click(function(){
					var itemid = $(this).attr('itemid');
					var paid_status = 0;
					
					if(!$(this).attr('checked')) {
						$('#buy_'+itemid).show();
						paid_status = 0;
					} else {
						if (areyousure()) {
							$('#buy_'+itemid).hide();
							paid_status = 1;
						} else {
							$(this).removeAttr('checked'); 
							paid_status = 0;
						}
					}

					$.post('<?php echo site_url($this->config->item('admin_folder').'/orders/paid');?>/'+itemid,{paid_status:paid_status},function(data){
		
					});
				});

    		}
		});
	}
}

$(document).ready(function(){
	$('.add_box').click(function(){
		$.fn.colorbox({	href: '<?php echo site_url($this->config->item('admin_folder').'/orders/shipping');?>/'+$(this).attr('rel'), width:'600px', height:'500px'}, function(){
			//$('input:submit, input:button, button').button();
		});
	});
});
</script>
<script type="text/javascript">
function grouping(id,orderid)
{	
	if (id) {
		location.href='<?php echo  site_url($this->config->item('admin_folder').'/orders/group_order/');?>/'+id+'/'+orderid;
	}	
}
</script>
<!-- <table class="gc_table" cellspacing="0" cellpadding="0">
    <thead>
		<tr>
			<th>Box Number</th>
			<th>Created Date</th>
			<th style="text-align:center;">Cost / Item</th>
			<th style="text-align:center;">Quantity</th>
			<th>Buy from</th>
			<th>Purchase</th>
			<th class="gc_cell_right" style="text-align:center;">Received</th>
	    </tr>
		
	</thead>
    <tbody>
	<?php $index=0; foreach($oseller['products'] as $product): ?>
	<?php
	//echo "<pre>";
	//print_r($product);
	//echo "</pre>";
	?>
	<tr>
		<td><?php echo $product->id; ?></td>
		<td><?php echo $product[code]; ?></td>
		<td style="text-align:center;"><?php echo $product[costperitem]; ?></td>	
		<td style="text-align:center;"><?php echo $product[total_quantity]; ?></td>
		<td>
		<select>
			<option value=""></option>
			<option>Original</option>
			<option>Others</option>
		</select>
		</td>
		<td>
		<div id="buy_<?php echo $product['itemid']; ?>" class="button_set" nowrap class="gc_tf1" style="text-align:left;white-space:nowrap;">
		<?php //if ($buy_link) { 
			echo anchor_popup($buy_link, 'Buy', $atts); ?>
		<?php //} ?>
		</div>
		</td>			

		<td class="gc_cell_right list_buttons" style="text-align:center;">
		<?php echo form_input(array('name'=>'shipment[shipped_on]', 'id'=>'shipped_on', 'value'=>'', 'class'=>'gc_tf1'));?></td>
		</td>
	</tr>
    <?php endforeach; ?>
    </tbody>
</table>
<div style="height:80px;"></div> -->

<?php include('footer.php'); ?>