<?php require('header.php'); 

function sort_url($by, $sort, $sorder, $code, $admin_folder)
{
	if ($sort == $by)
	{
		if ($sorder == 'asc')
		{
			$sort	= 'desc';
		}
		else
		{
			$sort	= 'asc';
		}
	}
	else
	{
		$sort	= 'asc';
	}
	$return = site_url($admin_folder.'/orders/index/'.$by.'/'.$sort.'/'.$code);
	return $return;
}
?>

<div id="breadcrumb">
	<ul>
    	<li><a href="<?php echo site_url($this->config->item('admin_folder').'/Orders');?>">Sales & Purchases</a></li>
    	<li class="last"><a href="<?php echo site_url($this->config->item('admin_folder').'/Orders/box');?>">Pending Grouping</a></li>
    </ul>
</div><!-- End of breadcrumb --> 
<br>

<h2>Box Management</h2>

<div style="clear:both"></div>
	
<div class="button_set">
	<div style="text-align:left;float:left;margin-top:8px;font-size:15px"><b>Currenly there is: <?php echo count($boxes); ?> box</b></div>
	
	<div style="text-align:left;float:right">
	<!-- <a class="add_box" href="#" onclick="return false;">Add Box</a> -->
	<a href="<?php echo site_url($this->config->item('admin_folder').'/orders/new_box'); ?>">Add Box</a>
	</div>
</div>
	
<div style="clear:both"><br></div>

<?php echo form_open($this->config->item('admin_folder').'/logistic/bulk_save', array('id'=>'bulk_form'));?>

<table class="gc_table" cellspacing="0" cellpadding="0">
    <thead>
		<tr>
			<th class="gc_cell_left">Box Number</th>
			<th>Created Date</th>
			<th style="text-align:center;">Total Order</th>
			<th style="text-align:center;">Total Item</th>
			<th style="text-align:center;">Quantity</th>
			<th style="text-align:center;">Net Weight (kg)</th>
			<th class="gc_cell_right"></th>
	    </tr>
	</thead>
    <tbody>			
		<?php echo (count($boxes) < 1)?'<tr><td style="text-align:center;" colspan="8">No box found.</td></tr>':''?>
 	   	<?php foreach($boxes as $box): ?>
		<tr>
			<td><?php echo $box->box_number; ?></td>
			<td><?php echo $box->created_on; ?></td>
			<td style="text-align:center;"><?php echo $box->total_order; ?></td>
			<td style="text-align:center;"><?php echo $box->total_item; ?></td>
			<td style="text-align:center;"><?php echo $box->quantity; ?></td>
			<td style="text-align:center;"><?php echo $box->total_weight; ?></td>
			<td class="gc_cell_right list_buttons">
				<a href="<?php echo site_url($this->config->item('admin_folder').'/orders/box/'.$box->id);?>">Manage Box</a>
			</td>
		</tr>
   		<?php endforeach; ?>

    </tbody>
</table>

</form>

<div style="height:80px"></div>

<h2>Pending Grouping</h2>

<!-- <?php echo form_open($this->config->item('admin_folder').'/orders/bulk_group', array('id'=>'group_form'));?> -->
<table class="gc_table" cellspacing="0" cellpadding="0">
    <thead>
		<tr>
			<th class="gc_cell_left"><a href="<?php echo sort_url('order_number', $sort_by, $sortorder, $code, $this->config->item('admin_folder')); ?>">Order ID</a> <?php if ($sort_by=="order_number" && $sortorder=="desc") { ?><img src="<?php echo base_url('images/sortdown.png');?>" border="0"><?php } ?><?php if ($sort_by=="order_number" && $sortorder=="asc") { ?><img src="<?php echo base_url('images/sortup.png');?>" border="0"><?php } ?></th>
			<th><a href="<?php echo sort_url('ordered_on', $sort_by, $sortorder, $code, $this->config->item('admin_folder')); ?>"><?php echo lang('ordered_on')?></a> <?php if ($sort_by=="ordered_on" && $sortorder=="desc") { ?><img src="<?php echo base_url('images/sortdown.png');?>" border="0"><?php } ?><?php if ($sort_by=="ordered_on" && $sortorder=="asc") { ?><img src="<?php echo base_url('images/sortup.png');?>" border="0"><?php } ?></th>
				<th style="text-align:center;">Product</th>
			<th style="text-align:center;">Quantity</th>
			<th style="text-align:center;">Total Weight</th>
			<th style="text-align:center;">Status</th>
			<th class="gc_cell_right"></th>
	    </tr>
		
	</thead>
    <tbody>
	<?php echo (count($orders) < 1)?'<tr><td style="text-align:center;" colspan="8">No pending grouping order found.</td></tr>':''?>
	<?php foreach($orders as $order): ?>
	<?php	
	foreach($this->Order_model->get_items($order->id) AS $order_item) {
		$total_quantity[$order->id] += $order_item[quantity];
		$total_weight[$order->id] += $order_item[weight];
		
		$total_cost[$order->id] += $order_item['base_price']*$order_item[quantity];

		++$total_product[$order->id];
		
	}
	?>
	<tr>
		<td><?php echo $order->order_number; ?></td>
		<td style="white-space:nowrap"><?php echo date('m/d/y h:i a', strtotime($order->ordered_on)); ?></td>
		<td style="text-align:center;"><?php echo $total_product[$order->id]; ?></td>	
		<td style="text-align:center;"><?php echo $total_quantity[$order->id]; ?></td>
		<td style="text-align:center;"><?php echo $total_weight[$order->id]; ?></td>
		<td style="text-align:center;"><?php echo ($order->is_packed) ? "packed" : "not pack" ;?></td>			

		<td class="gc_cell_right list_buttons">
			<a class="button" id="button_<?php echo $order->id; ?>" onclick="get_order_detail('<?php echo $order->id; ?>')">Pack Status</a>
			<?php if ($order->is_packed) { ?>
			&nbsp;&nbsp;&nbsp;<select class="gc_tf1" style="margin-top:5px;" onchange="grouping(this.value,'<?php echo $order->id; ?>')">
			<!-- &nbsp;&nbsp;&nbsp;<select name="order[<?php echo $order->id; ?>]" class="gc_tf1" style="margin-top:5px;"> -->
				<option value="">Group to</option>
				<?php foreach($boxes as $box): ?>
				<?php if ($box->shipment_id) continue; ?>
				<option value="<?php echo $box->id; ?>"><?php echo $box->box_number; ?></option>
				<?php endforeach; ?>
			</select>
			<?php } ?>
		</td>
	</tr>
	<tr id="more_<?php echo $order->id; ?>" style="display:none">
		<td style="background-color:#eee" colspan="8" id="table_<?php echo $order->id; ?>"></td>
	</tr>
    <?php endforeach; ?>
		
    </tbody>
</table>
<!-- </form> -->

<!-- <div class="button_set">
	<div style="text-align:left;float:right">
	<a href="#" onclick="$('#group_form').submit(); return false;">Group All</a>
	</div>
</div> -->

<script type="text/javascript">
function get_order_list(orderid)
{
	if ($('#button_'+orderid).html() == '<span class="ui-button-text">Hide</span>') {
		$('#more_'+orderid).hide();
		$('#button_'+orderid).html('<span class="ui-button-text">Expand</span>');
	} else {
		$.ajax('<?php echo site_url($this->config->item('admin_folder').'/orders/shipping');?>/'+orderid, { data: {orderid:orderid},
    		type: "POST",
    		beforeSend: function() {
        		$('#button_'+orderid).html('<span class="ui-button-text">Please wait...</span>');
    		},
    		error: function() {
        		alert("error"); 
    		},
    		success: function(data) {
        		$('#table_'+orderid).html(data);
				$('#more_'+orderid).show();
				$('#button_'+orderid).html('<span class="ui-button-text">Hide</span>');
				$('.button_set a').button();
    		}
		});
	}
}

function get_order_detail(orderid)
{
	if ($('#button_'+orderid).html() == '<span class="ui-button-text">Hide</span>') {
		$('#more_'+orderid).hide();
		$('#button_'+orderid).html('<span class="ui-button-text">Pack Status</span>');
	} else {
		$.ajax('<?php echo site_url($this->config->item('admin_folder').'/orders/group_items');?>/'+orderid, { data: {},
    		type: "POST",
    		beforeSend: function() {
        		$('#button_'+orderid).html('<span class="ui-button-text">Please wait...</span>');
    		},
    		error: function() {
        		alert("error"); 
    		},
    		success: function(data) {
        		$('#table_'+orderid).html(data);
				$('#more_'+orderid).show();
				$('#button_'+orderid).html('<span class="ui-button-text">Hide</span>');
				$('.button_set a').button();

				$('.paid_button').click(function(){
					var itemid = $(this).attr('itemid');
					var paid_status = 0;
					
					if(!$(this).attr('checked')) {
						$('#buy_'+itemid).show();
						paid_status = 0;
					} else {
						if (areyousure()) {
							$('#buy_'+itemid).hide();
							paid_status = 1;
						} else {
							$(this).removeAttr('checked'); 
							paid_status = 0;
						}
					}

					$.post('<?php echo site_url($this->config->item('admin_folder').'/orders/paid');?>/'+itemid,{paid_status:paid_status},function(data){
		
					});
				});

    		}
		});
	}
}

function grouping(id,orderid)
{	
	if (id) {
		location.href='<?php echo  site_url($this->config->item('admin_folder').'/orders/group_order/');?>/'+id+'/'+orderid;
	}	
}

$(document).ready(function(){
	$('.add_box').click(function(){
		$.ajax('<?php echo site_url($this->config->item('admin_folder').'/orders/new_box');?>', { data: {},
   			type: "POST",
   			beforeSend: function() {},
    		error: function() {
        		alert("error"); 
    		},
    		success: function(data) {
       			$('#new_box').html(data);
				$('.button_set a').button();
   			}
		});
	});
});
</script>

<?php include('footer.php'); ?>