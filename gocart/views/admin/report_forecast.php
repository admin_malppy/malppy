<?php require('header.php'); ?>

<?php
//echo "<pre>";
//print_r($categories);
//echo "</pre>";

/*
High - The high order quantity of the material item.
Qty - The current quantity on hand.
Avl - The current quantity available. This is the quantity on hand minus the quantity on open work orders.
Ord - The current quantity on order.
Sales - The unit sales for the material item in for the period configured in the report parameters.
Forecast - High - Avl - Ord + Sales.
*/
?>

<div id="breadcrumb">
	<ul>
    	<li><a href="<?php echo site_url($this->config->item('admin_folder').'/reports');?>">Reports</a></li>
        <li class="last"><a href="<?php echo site_url($this->config->item('admin_folder').'/reports_customer');?>">Sales Forecast</a></li>
    </ul>
</div><!-- End of breadcrumb --> 

<h2 style="margin:10px 0px 10px 0px; padding:0px;">Sales Forecast</h2>
<div id="stock_container">
	<table class="gc_table" cellspacing="0" cellpadding="0">
	<thead>
		<tr>
			<th class="gc_cell_left" style="text-align:center;">Category</th>
			<th style="text-align:center;">January</th>
			<th style="text-align:center;">February</th>
			<th style="text-align:center;">March</th>
			<th style="text-align:center;">April</th>
			<th style="text-align:center;">May</th>
			<th class="gc_cell_right" style="text-align:center;">June</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($categories AS $cat):?>
		<?php if ($cat['category']->name=='Sales!') continue; ?>
		<tr class="gc_row">
			<td class="gc_cell_left" align="center"><?php echo $cat['category']->name; ?></td>
			<td align="center"><?php echo rand(0,10*$Product_model->count_products($cat['category']->id)); ?></td>
			<td align="center"><?php echo rand(0,10*$Product_model->count_products($cat['category']->id)); ?></td>
			<td align="center"><?php echo rand(0,10*$Product_model->count_products($cat['category']->id)); ?></td>
			<td align="center"><?php echo rand(0,10*$Product_model->count_products($cat['category']->id)); ?></td>
			<td align="center"><?php echo rand(0,10*$Product_model->count_products($cat['category']->id)); ?></td>
			<td class="gc_cell_right" align="center"><?php echo rand(0,10*$Product_model->count_products($cat['category']->id)); ?></td>
		</tr>
		<?php if (sizeof($cat['children']) > 0) { ?>
		<?php foreach ($cat['children'] AS $subcat):?>		
		<tr class="gc_row">
			<td class="gc_cell_left" align="center"><?php echo  $subcat['category']->name; ?></td>
			<td align="center"><?php echo rand(0,10*$Product_model->count_products($subcat['category']->id)); ?></td>
			<td align="center"><?php echo rand(0,10*$Product_model->count_products($subcat['category']->id)); ?></td>
			<td align="center"><?php echo rand(0,10*$Product_model->count_products($subcat['category']->id)); ?></td>
			<td align="center"><?php echo rand(0,10*$Product_model->count_products($subcat['category']->id)); ?></td>
			<td align="center"><?php echo rand(0,10*$Product_model->count_products($subcat['category']->id)); ?></td>
			<td class="gc_cell_right" align="center"><?php echo rand(0,10*$Product_model->count_products($subcat['category']->id)); ?></td>
		</tr>
		<?php endforeach;?>
		<?php } ?>
		<tr>
			<td colspan="7">&nbsp;</td>
		</tr>
		<?php endforeach;?>
	</tbody>
</table>
</div>

<?php include('footer.php'); ?>