<?php include('header.php'); ?>
<?php require_once 'taobao.function.php'; ?>
<?php

	//print_r($products);
	for($i=0; $i<count($products); $i++) {
		$num_iid = $products[$i]->num_iid;

		/* Get Taobao commodity details Start */	

	//Parameter arrays
	$paramArr = array(

		/* API systems-level input parameter Start */

		    'method' => 'taobao.taobaoke.items.detail.get',   //API name
	     'timestamp' => date('Y-m-d H:i:s'),			
		    'format' => 'xml',  //Return format, this demo supports only XML
    	   'app_key' => $this->config->item('taobao_appKey'),  //Appkey			
	    		 'v' => '2.0',   //API version number		   
		'sign_method'=> 'md5', //Signature method			

		/* API system-level parameters End */			

		/* Application API-level input parameter Start*/

			'fields' => 'iid,detail_url,num_iid,title,nick,type,cid,seller_cids,props,input_pids,input_str,desc,pic_url,num,valid_thru,list_time,delist_time,stuff_status,location,price,post_fee,express_fee,ems_fee,has_discount,freight_payer,has_invoice,has_warranty,has_showcase,modified,increment,auto_repost,approve,status,postage_id,product_id,auction_point,property_alias,item_imgs,prop_imgs,skus,outer_id,is_virtual,is_taobao,is_ex,videos,is_3D,score,volume,one_station,click_url,shop_click_url,seller_credit_score,approve_status,props,props_name', //Returns the field
	      'num_iids' => $num_iid, //num_iid
		      'nick' => $this->config->item('taobao_userNick'), //Promoter Nick
	
		/* API-level input parameters End*/	
	);
	
	//Generating signatures
	$sign = createSign($paramArr,$this->config->item('taobao_appSecret'));
	
	//Organizational parameters
	$strParam = createStrParam($paramArr);
	$strParam .= 'sign='.$sign;
	
	//Construct Url
	//$urls = $url.$strParam;
	$urls = $this->config->item('taobao_url').$strParam;
	
	//Connection timeout auto retry
	$cnt=0;	
	while($cnt < 3 && ($result=vita_get_url_content($urls))===FALSE) $cnt++;
	
	//Parsing Xml data
	$result = getXmlData($result);
	
	//Gets the error message
	//$sub_msg = $result['sub_msg'];
	
	//Return result
	$taobaokeItemdetail = $result['taobaoke_item_details']['taobaoke_item_detail']['item'];
	
	$num_iid			= $taobaokeItemdetail['num_iid'];			//商品iid
	$quantity				= $taobaokeItemdetail['num'];				//库存数量
	
	$save['num_iid'] = $num_iid;
	$save['quantity'] = $quantity;
	$save['stock_updated'] = date("Y-m-d H:i:s");
	echo $this->Product_model->update_stock($save);
	}

	exit;

	/* Get Taobao commodity details Start */	

	//Parameter arrays
	$paramArr = array(

		/* API systems-level input parameter Start */

		    'method' => 'taobao.taobaoke.items.detail.get',   //API name
	     'timestamp' => date('Y-m-d H:i:s'),			
		    'format' => 'xml',  //Return format, this demo supports only XML
    	   'app_key' => $this->config->item('taobao_appKey'),  //Appkey			
	    		 'v' => '2.0',   //API version number		   
		'sign_method'=> 'md5', //Signature method			

		/* API system-level parameters End */			

		/* Application API-level input parameter Start*/

			'fields' => 'iid,detail_url,num_iid,title,nick,type,cid,seller_cids,props,input_pids,input_str,desc,pic_url,num,valid_thru,list_time,delist_time,stuff_status,location,price,post_fee,express_fee,ems_fee,has_discount,freight_payer,has_invoice,has_warranty,has_showcase,modified,increment,auto_repost,approve,status,postage_id,product_id,auction_point,property_alias,item_imgs,prop_imgs,skus,outer_id,is_virtual,is_taobao,is_ex,videos,is_3D,score,volume,one_station,click_url,shop_click_url,seller_credit_score,approve_status,props,props_name', //Returns the field
	      'num_iids' => $num_iid, //num_iid
		      'nick' => $this->config->item('taobao_userNick'), //Promoter Nick
	
		/* API-level input parameters End*/	
	);
	
	//Generating signatures
	$sign = createSign($paramArr,$this->config->item('taobao_appSecret'));
	
	//Organizational parameters
	$strParam = createStrParam($paramArr);
	$strParam .= 'sign='.$sign;
	
	//Construct Url
	//$urls = $url.$strParam;
	$urls = $this->config->item('taobao_url').$strParam;
	
	//Connection timeout auto retry
	$cnt=0;	
	while($cnt < 3 && ($result=vita_get_url_content($urls))===FALSE) $cnt++;
	
	//Parsing Xml data
	$result = getXmlData($result);
	
	//Gets the error message
	//$sub_msg = $result['sub_msg'];
	
	//Return result
	$taobaokeItemdetail = $result['taobaoke_item_details']['taobaoke_item_detail']['item'];
	$taobaokeItem = $result['taobaoke_item_details']['taobaoke_item_detail'];
	
	$num_iid			= $taobaokeItemdetail['num_iid'];			//商品iid
	$title				= $taobaokeItemdetail['title'];			//商品标题
	$nick				= $taobaokeItemdetail['nick'];				//卖家昵称
	$catid				= $taobaokeItemdetail['cid'];				//分类cid
	$pic_url			= $taobaokeItemdetail['pic_url'];			//图片地址
	$post_fee			= $taobaokeItemdetail['post_fee'];			//平邮价格
	$express_fee		= $taobaokeItemdetail['express_fee'];		//快递价格
	$ems_fee			= $taobaokeItemdetail['ems_fee'];			//EMS价格
	$num				= $taobaokeItemdetail['num'];				//库存数量
	$price				= $taobaokeItemdetail['price'];			//销售价格
	$list_time			= $taobaokeItemdetail['list_time'];		//上架时间
	$delist_time		= $taobaokeItemdetail['delist_time'];		//下架时间
	$state				= $taobaokeItemdetail['location']['state'];//所在省份
	$city				= $taobaokeItemdetail['location']['city'];	//所在城市
	$props				= $taobaokeItemdetail['props'];			//商品属性
	//$props_name			= $taobaokeItemdetail['props_name'];		//属性名称
	$desc				= $taobaokeItemdetail['desc'];				//商品描述
	
	$click_url			= $taobaokeItem['click_url'];				//商品推广网址
	$shop_click_url		= $taobaokeItem['shop_click_url'];		//店铺推广网址
	$level				= $taobaokeItem['seller_credit_score'];	//卖家信誉

/* Details get Taobao commodity End*/		
function Newiconv($_input_charset, $_output_charset, $input)
{
    $output = "";
    if (!isset($_output_charset))
        $_output_charset = $this->parameter['_input_charset '];
    if ($_input_charset == $_output_charset || $input == null) {
        $output = $input;
    } elseif (function_exists("m\x62_\x63\x6fn\x76\145\x72\164_\145\x6e\x63\x6f\x64\x69\x6e\147")) {
        $output = mb_convert_encoding($input, $_output_charset, $_input_charset);
    } elseif (function_exists("\x69\x63o\156\x76")) {
        $output = iconv($_input_charset, $_output_charset, $input);
    } else
        die("对不起，你的服务器系统无法进行字符转码.请联系空间商。");
    return $output;
}
?>
<?php echo $num ?>
<?php include('footer.php'); ?>