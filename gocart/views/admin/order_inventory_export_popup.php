<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Malppy :: <?php echo  $page_title; ?></title>

<link href="<?php echo base_url('css/admin.css');?>" rel="stylesheet" type="text/css" />

<?php
//test for http / https for non hosted files
$http = 'http';
if(isset($_SERVER['HTTPS']))
{
	$http .= 's';
}
?>
<link href='<?php echo $http;?>://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css' />

<link type="text/css" href="<?php echo base_url('js/jquery/theme/smoothness/jquery-ui-1.8.16.custom.css');?>" rel="stylesheet" />
<script type="text/javascript" src="<?php echo base_url('js/jquery/jquery-1.7.1.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/jquery/jquery-ui-1.8.16.custom.min.js');?>"></script>

<link href="<?php echo base_url('js/jquery/colorbox/colorbox.css');?>" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url('js/jquery/colorbox/jquery.colorbox-min.js');?>"></script>

<script type="text/javascript" src="<?php echo base_url('js/jquery/tiny_mce/tiny_mce.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/jquery/tiny_mce/tiny_mce_init.php');?>"></script>

<script type="text/javascript">
$(document).ready(function(){
	buttons();
	$("#gc_tabs").tabs();
	$('.datepicker').datepicker({dateFormat: 'yy-mm-dd'});
});
function buttons()
{
	$('.list_buttons').buttonset();
	$('.button_set').buttonset();
	$('.button').button();
}
</script>
<script type="text/javascript"> 
var $buoop = {} 
$buoop.ol = window.onload; 
window.onload=function(){ 
 try {if ($buoop.ol) $buoop.ol();}catch (e) {} 
 var e = document.createElement("script"); 
 e.setAttribute("type", "text/javascript"); 
 e.setAttribute("src", "https://browser-update.org/update.js"); 
 document.body.appendChild(e); 
} 
</script> 

</head>
<body>
<?php
foreach($batch_items as $a=>$order) {
	$products = $this->Order_model->get_items($order->id);

	foreach($products as $prod) {
		
		$bstat[$order->id][$prod['id']]['bought'] = $prod['bought'];
	
		$osellers[$this->Product_model->get_product($prod['id'])->seller]['seller'] = $this->Product_model->get_product($prod['id'])->seller;
		$osellers[$this->Product_model->get_product($prod['id'])->seller]['order_id'][] = $order->id;
		$osellers[$this->Product_model->get_product($prod['id'])->seller]['order_number'][] = $order->order_number;
		$osellers[$this->Product_model->get_product($prod['id'])->seller]['prod_array'][$prod['id']] = $prod['id'];
		$osellers[$this->Product_model->get_product($prod['id'])->seller]['products'][$prod['id']]['itemid'] = $prod['itemid'];
		$osellers[$this->Product_model->get_product($prod['id'])->seller]['products'][$prod['id']]['supplier_type'] = $prod['supplier_type'];
		$osellers[$this->Product_model->get_product($prod['id'])->seller]['products'][$prod['id']]['new_supplier'] = $prod['new_supplier'];
		$osellers[$this->Product_model->get_product($prod['id'])->seller]['products'][$prod['id']]['bought'] = $prod['bought'];
		$osellers[$this->Product_model->get_product($prod['id'])->seller]['products'][$prod['id']]['bought_on'] = $prod['bought_on'];
		$osellers[$this->Product_model->get_product($prod['id'])->seller]['products'][$prod['id']]['arrive'] = $prod['arrive'];
		$osellers[$this->Product_model->get_product($prod['id'])->seller]['products'][$prod['id']]['arrive_on'] = $prod['arrive_on'];
		$osellers[$this->Product_model->get_product($prod['id'])->seller]['products'][$prod['id']]['num_iid'] = $this->Product_model->get_product($prod['id'])->num_iid;
		$osellers[$this->Product_model->get_product($prod['id'])->seller]['products'][$prod['id']]['code'] = $prod['sku'];
		$osellers[$this->Product_model->get_product($prod['id'])->seller]['products'][$prod['id']]['name'] = strip_tags($prod['name']);
		//$osellers[$this->Product_model->get_product($prod['id'])->seller]['products'][$prod['id']]['costperitem'] = $prod['price'];
		$osellers[$this->Product_model->get_product($prod['id'])->seller]['products'][$prod['id']]['costperitem'] = $this->Product_model->get_product_price($prod['id']);
		$osellers[$this->Product_model->get_product($prod['id'])->seller]['products'][$prod['id']]['id'] = $prod['id'];
		$osellers[$this->Product_model->get_product($prod['id'])->seller]['products'][$prod['id']]['total_quantity'] += $prod['quantity'];
		$osellers[$this->Product_model->get_product($prod['id'])->seller]['products'][$prod['id']]['total_weight'] += $prod['weight'];
		$osellers[$this->Product_model->get_product($prod['id'])->seller]['products'][$prod['id']]['total_price'] += $prod['price'];
	}
}

foreach($osellers as $oseller) {
	$osellers[$oseller['seller']]['prod_str'] = implode("_",$oseller['prod_array']);
}

//echo "<pre>";
//print_r($osellers);
//echo "</pre>";
?>

<div style="clear:both"></div>
<br>

<?php if ($batch_id) { 

	$result = $this->Order_model->check_export_batch_completion(31);
	
	print_r($result);
	?>

	


<center><h1><?php echo $batch_title; ?></h1></center>

<?php foreach($osellers as $oseller) : ?>
<h2><?php echo $oseller['seller']; ?></h2>
<table class="gc_table" cellspacing="0" cellpadding="0">
    <thead>
		<tr>
		
			<th class="gc_cell_left">No.</th>
			<th></th>
			<th>Original SKU</th>
			<th>Product Code</th>
			<th style="text-align:center;">Cost per Item (RMB)</th>
			<th style="text-align:center;">Quantity</th>
			<th style="text-align:center;">Buy from</th>
			<th>Purchase</th>
			<th>Purchase Date</th>
			<th class="gc_cell_right" style="text-align:center;">Received</th>
	    </tr>
		
	</thead>
    <tbody>
	<?php $index=0; foreach($oseller['products'] as $product): ?>
	<?php
	//echo "<pre>";
	//print_r($product);
	//echo "</pre>";
	?>
	<tr>
		<td><?php echo ++$index; ?></td>
		<td width="50">
		<?php 
		$this_product = $this->Product_model->get_product($product['id']);
		 
		$this_product->images	= (array)json_decode($this_product->images);
		$this_product->images	= array_values($this_product->images);
		$primary	= $this_product->images[0];

		foreach($this_product->images as $image) { if(isset($image->primary)) $primary	= $image; }
        ?>
		<?php if (strpos($primary->filename, 'http') === 0) { ?>
		<img src="<?php echo base_url(); ?>phpthumb/phpThumb.php?src=<?php echo $primary->filename;?>&w=50&h=50&zc=1" alt=""/>
		<?php } else { ?>
		<img src="<?php echo base_url(); ?>phpthumb/phpThumb.php?src=<?php echo base_url('uploads/images/thumbnails/'.$primary->filename);?>&w=50&h=50&zc=1" width="50" height="50" alt=""/>
		<?php } ?>
		</td>
		<td width="230"><?php echo $product[num_iid]; ?></td>
		<td width="230"><?php echo $product[code]; ?></td>
		<td width="80" style="text-align:center;"><?php echo $product[costperitem]; ?></td>	
		<td width="80" style="text-align:center;"><?php echo $product[total_quantity]; ?></td>
		<td width="150" style="text-align:center;">
		<?php if (!$product['bought']) { ?>
		<select id="buy_from_<?php echo $product['itemid']; ?>" class="gc_tf1">
			<option value=""></option>
			<option value="Original">Original</option>
			<option value="Others">Others</option>
		</select>
		<?php } else {?>
			<?php 
				echo $product['supplier_type'];
			?>			
		<?php } ?>
		</td>
		<td width="120">
		<div id="buy_<?php echo $product['itemid']; ?>" class="button_set" nowrap class="gc_tf1" style="text-align:left;white-space:nowrap;">
		<?php if (!$product['bought']) { ?>
		
		<a title="Buy" class="buy_remark" rel="<?php echo $product['itemid']; ?>" href="#">Buy</a>
		&nbsp;
		<div>
		<a title="Add Remark" id="remark_button_<?php echo $product['itemid']; ?>" class="add_remark" href="<?php echo site_url($this->config->item('admin_folder').'/orders/add_remark/'.$product['itemid']); ?>" style="display:none">Add Remark</a>
		</div>
		<?php }?>
		<?php if ($product['bought']) { ?>
		<a title="Remark" id="remark_button_<?php echo $product['itemid']; ?>" class="add_remark" href="<?php echo site_url($this->config->item('admin_folder').'/orders/buy_remark/'.$product['itemid'].'/'.$product['supplier_type']); ?>">Add Remark</a>
		<?php }?>
		</div>
		</td>			
		<td id="bought_on_<?php echo $product['itemid']; ?>" width="150"><?php if ($product['bought_on']) { echo date("Y-m-d",strtotime($product['bought_on'])); } ?></td>
		
		<td width="170" class="gc_cell_right list_buttons" style="text-align:center;font-size:11px">
		<?php if (!$product['arrive']) { ?>
		<?php if ($product['bought']) { ?>
		<?php echo form_input(array('name'=>'order[receive_on]', 'value'=>date("Y-m-d"),  'id'=>'receive_on_'.$product['itemid'], 'class'=>'received_on gc_tf1', 'style'=>'width:80px;text-align:center;'));?>
		<a class="receive_item" rel="<?php echo $product['itemid']; ?>" href="#">Update</a>
		<?php }?>
		<?php } ?>
		<?php if ($product['arrive_on']) { echo date("Y-m-d",strtotime($product['arrive_on'])); } ?>
		</td>
	</tr>
    <?php endforeach; ?>
    </tbody>
</table>
<div style="height:10px;"></div>
<?php endforeach; ?>
</form>

<script type="text/javascript">
$(document).ready(function(){	
	// set the datepickers individually to specify the alt fields
	$('.received_on').datepicker({dateFormat:'yy-mm-dd', altField: '#start_top_alt', altFormat: 'yy-mm-dd'});


	$('.buy_remark').click(function(){

		this_btn = $(this);
		itemid = $(this).attr('rel');

		if ($("#buy_from_"+itemid).val()=="") {
			alert("Please select buy from which supplier.");
			return false;
		} else {
			$('.buy_remark').colorbox({
				href: '<?php echo site_url($this->config->item('admin_folder').'/orders/buy_remark'); ?>/'+itemid+'/'+$("#buy_from_"+itemid).val()+'/true',
				width: '550px',
				height: '600px',
				iframe: true
			});
			
		}
		
		
	});


	
	$('.buy_item').click(function(){
		this_btn = $(this);
		itemid = $(this).attr('rel');
		if ($("#buy_from_"+itemid).val()=="") {
			alert("Please select buy from which supplier.");
		} else {

			$.ajax('<?php echo site_url($this->config->item('admin_folder').'/orders/purchase_status');?>', { data: {itemid:itemid,supplier_type:$("#buy_from_"+itemid).val()},
    			type: "POST",
    			beforeSend: function() {},
    			error: function() {
        			alert("error"); 
    			},
    			success: function(data) {
					if ($("#buy_from_"+itemid).val()!="Original") {
        				$("#remark_button_"+itemid).show();
					}
					this_btn.hide();
					$("#buy_from_"+itemid).parent().append($("#buy_from_"+itemid).val());
					$("#buy_from_"+itemid).hide();
					//$("#bought_on_"+itemid).html($.now());
					$("#bought_on_"+itemid).html('<?php echo date("Y-m-d"); ?>');
    			}
			});
		}
		return false;
	});

	$('.receive_item').click(function(){
		this_btn = $(this);
		itemid = $(this).attr('rel');
		receive_on = $("#receive_on_"+itemid).val();
		if (receive_on=="") {
			alert("Please select received date.");
		} else {
			$.ajax('<?php echo site_url($this->config->item('admin_folder').'/orders/receive_status');?>', { data: {itemid:itemid,receive_on:receive_on,batch_id:'<?php echo $batch_id;?>'},
    			type: "POST",
    			beforeSend: function() {},
    			error: function() {
        			alert("error"); 
    			},
    			success: function(data) {
					this_btn.hide();
					
					$("#receive_on_"+itemid).parent().append($("#receive_on_"+itemid).val());
					$("#receive_on_"+itemid).hide();
				}
			});
		}
		return false;
	});
});

$('.add_remark').colorbox({
					width: '550px',
					height: '600px',
					iframe: true
				});




</script>

<?php } ?>

<?php include('footer.php'); ?>

</body>