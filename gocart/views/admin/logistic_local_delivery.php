<?php require('header.php'); ?>

<div id="breadcrumb">
	<ul>
		<li><a href="<?php echo site_url($this->config->item('admin_folder').'/logistic');?>">Logistic</a></li>
       	<li class="last"><a href="<?php echo site_url($this->config->item('admin_folder').'/logistic/local_delivery');?>">Packing & Delivery</a></li>
    </ul>
</div><!-- End of breadcrumb --> 
<br>

<?php foreach ($shipment AS $ship) { ?>

<h1><?php echo $ship['shipment_number']; ?></h1>

<?php echo form_open($this->config->item('admin_folder').'/logistic/bulk_date', array('id'=>'bulk_form_'.$ship['shipid']));?>
<table class="gc_table" cellspacing="0" cellpadding="0">
    <thead>
		<tr>
			<th class="gc_cell_left">No</th>
			<th>Customer ID</th>
			<th>Order ID</th>
			<th style="text-align:center;">Packed</th>
			<th style="text-align:center;">Date for BT</th>
			<th style="text-align:center;">DO</th>
			<th>Remark</th>
			<th class="gc_cell_right"></th>
	    </tr>
		<?php echo $pagination; ?>
	</thead>
 	<tfoot>
    <?php echo $pagination?>
	</tfoot>
    <tbody>
		
	<?php echo (count($ship['orders']) < 1)?'<tr><td style="text-align:center;" colspan="10">'.lang('no_orders') .'</td></tr>':''?>
    <?php foreach($ship['orders'] as $order): ?>
	<?php if ($order->status == "Closed") continue; ?>
		<tr>
			<td><?php echo ++$index; ?></td>
			<td><?php echo $this->Customer_model->get_customer_code($order->customer_id); ?></td>
			<td style="white-space:nowrap"><?php echo $order->order_number; ?></td>
			<td style="text-align:center;">
			<?php
				$data	= array('name'=>'order['.$order->id.'][is_packed]', 'value'=>1, 'checked'=>set_checkbox('logistic['.$order->id.'][is_packed]', 1, (bool)$order->is_packed));
				echo form_checkbox($data);
			?>
			</td>
			<td style="text-align:center;">
			<?php if (!$order->bt_on) { ?>
			<?php echo form_input(array('name'=>'order['.$order->id.'][bt_date]', 'value'=>'',  'id'=>'bt_date_'.$order->id, 'class'=>'bt_date gc_tf1', 'style'=>'width:80px;text-align:center;'));?>
			<!-- <a class="bt_btn button" rel="<?php echo $order->id; ?>" href="#">Update</a> -->
			<?php } else { ?>
			<?php echo $order->bt_on; ?>
			<?php } ?>
			</td>
			<td style="text-align:center;">
			<?php if (!$order->do_on) { ?>
			<?php echo form_input(array('name'=>'order['.$order->id.'][do_date]', 'value'=>'',  'id'=>'do_date_'.$order->id, 'class'=>'do_date gc_tf1', 'style'=>'width:80px;text-align:center;'));?>
			<!-- <a class="do_btn button" rel="<?php echo $order->id; ?>" href="#">Update</a> -->
			<?php } else { ?>
			<?php echo $order->do_on; ?>
			<?php } ?>
			<!-- <a class="button" href="<?php echo site_url($this->config->item('admin_folder').'/logistic/do_slip/'.$order->id);?>" target="_blank">View</a> --></td>			
			<td><span id="remark_<?php echo $order->id; ?>" class="<?php echo url_title($order->remark); ?>"><?php echo $order->remark; ?></span></td>
			<td class="gc_cell_right list_buttons">
				<a href="<?php echo site_url($this->config->item('admin_folder').'/logistic/do_slip/'.$order->id);?>" target="_blank">Print</a>
				<!-- <a href="#">Add Remark</a>&nbsp; -->
				<a class="button" onclick="edit_remark(<?php echo $order->id; ?>)">Add Remark</a>
				<a href="<?php echo site_url($this->config->item('admin_folder').'/logistic/close_order/'.$order->id);?>" onclick="return close_order(<?php echo $order->id; ?>);">Close</a>&nbsp;
			
				<div id="remark_container_<?php echo $order->id; ?>" style="position:relative; font-weight:bold; padding-left:20px;">
				<!-- <img style="position:absolute; left:2px;" src="<?php echo base_url('images/edit.gif');?>" alt="edit" title="edit"/> -->
				
				</div>
				<div id="edit_remark_<?php echo $order->id; ?>" style="display:none; position:relative; white-space:nowrap;">
				<textarea name="remark" id="remark_form_<?php echo $order->id; ?>" style="width:200px;"><?php echo $order->remark;?></textarea><br>
				<a onClick="save_remark(<?php echo $order->id; ?>)">Done</a>
				</div>
			</td>
		</tr>
    <?php endforeach; ?>
		
    </tbody>
</table>
</form>

<?php echo form_open($this->config->item('admin_folder').'/logistic/bulk_close', array('id'=>'close_form_'.$ship['shipid']));?>
<input type="hidden" name="shipment" value="<?php echo $ship['shipid']; ?>">
</form>

<div class="button_set">
	<div style="text-align:left;float:right">
	<a href="#" onclick="$('#bulk_form_<?php echo $ship['shipid']; ?>').submit(); return false;">Save All</a>
	<a href="#" onclick="$('#close_form_<?php echo $ship['shipid']; ?>').submit(); return false;">Close All</a>
	</div>
</div>

<div style="height:50px;"></div>
<?php }?>

<script type="text/javascript">
$(document).ready(function(){
	$('input:button').button();
	// set the datepickers individually to specify the alt fields
	$('.bt_date').datepicker({dateFormat:'yy-mm-dd', altField: '#start_top_alt', altFormat: 'yy-mm-dd'});
	$('.do_date').datepicker({dateFormat:'yy-mm-dd', altField: '#start_top_alt', altFormat: 'yy-mm-dd'});

	$('.bt_btn').click(function(){
		this_btn = $(this);
		orderid = $(this).attr('rel');
		bt_on = $("#bt_date_"+orderid).val();

		if (bt_on=="") {
			alert("Please select Date for BT.");
		} else {
			$.ajax('<?php echo site_url($this->config->item('admin_folder').'/orders/save_bt_date');?>', { data: {orderid:orderid,bt_on:bt_on},
    			type: "POST",
    			beforeSend: function() {},
    			error: function() {
        			alert("error"); 
    			},
    			success: function(data) {
					this_btn.hide();
					$("#bt_date_"+orderid).parent().append($("#bt_date_"+orderid).val());
					$("#bt_date_"+orderid).hide();
				}
			});
		}
		return false;
	});

	$('.do_btn').click(function(){
		this_btn = $(this);
		orderid = $(this).attr('rel');
		do_on = $("#do_date_"+orderid).val();

		if (do_on=="") {
			alert("Please select Date for DO.");
		} else {
			$.ajax('<?php echo site_url($this->config->item('admin_folder').'/orders/save_do_date');?>', { data: {orderid:orderid,do_on:do_on},
    			type: "POST",
    			beforeSend: function() {},
    			error: function() {
        			alert("error"); 
    			},
    			success: function(data) {
					this_btn.hide();
					$("#do_date_"+orderid).parent().append($("#do_date_"+orderid).val());
					$("#do_date_"+orderid).hide();
				}
			});
		}
		return false;
	});
});
function close_order(orderid) {
	return confirm('Confirm close this order?');
}

function edit_remark(id)
{
	$('#remark_container_'+id).hide();
	$('#edit_remark_'+id).show();
}

function save_remark(id)
{
	$.post("<?php echo site_url($this->config->item('admin_folder').'/orders/edit_remark'); ?>", { id: id, remark: $('#remark_form_'+id).val()}, function(data){
		$('#remark_'+id).html('<span class="'+data+'">'+$('#remark_form_'+id).val()+'</span>');
	});
	
	$('#remark_container_'+id).show();
	$('#edit_remark_'+id).hide();	
}

</script>

<?php include('footer.php'); ?>