<?php include('header.php');?>

<?php if(isset($finished)) : ?>

<script type="text/javascript"> $(function() { parent.location.reload(); }); </script>

<?php else : ?>

<div style="text-align:left">
<form id="remark_form" action="<?php echo site_url($this->config->item('admin_folder').'/logistic/update_status/'.$shipment_id);?>" method="post" />
<input type="hidden" name="update" value="true">
<input type="hidden" name="shipment_id" value="<?php echo $shipment_id; ?>">
<?php if(!empty($errors)) : ?>
<div id="err_box" class="error">
	<?php echo $errors ?>
</div>
<?php endif; ?>
<table width="100%" cellspacing="10">
<tr>
	<th align="center"><h1>Status Update</h1></th>
</tr>
<tr>
	<th align="left">Pack and sent on <input type="text" id="ship_arrived_on" name="ship_arrived_on" value="<?php echo $shipment->shipped_on; ?>" size="20" class="gc_tf1"/></th>
</tr>
<tr>
	<th align="left">Shipping arrived on <input type="text" id="ship_sent_on" name="ship_sent_on" value="<?php echo $shipment->sent_on; ?>" size="20" class="gc_tf1"/></th>
</tr>
<tr>
	<td></td>
</tr>
<tr>
	<td><a onclick="$('#remark_form').trigger('submit');" class="button">Update</a></td>
</tr>

</table>
</form>
</div>
<?php endif; ?>

<script type="text/javascript">
$(document).ready(function(){	
	// set the datepickers individually to specify the alt fields
	$('#ship_arrived_on').datepicker({dateFormat:'yy-mm-dd', altField: '#start_top_alt', altFormat: 'yy-mm-dd'});
	$('#ship_sent_on').datepicker({dateFormat:'yy-mm-dd', altField: '#start_top_alt', altFormat: 'yy-mm-dd'});
});
</script>

<?php include('footer.php');