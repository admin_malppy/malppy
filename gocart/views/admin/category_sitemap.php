<?php include('header.php'); ?>
<script type="text/javascript">
function areyousure()
{
	return confirm('<?php echo lang('confirm_delete_category');?>');
}
</script>

<div id="breadcrumb">
	<ul>
		<li><a href="<?php echo site_url($this->config->item('admin_folder').'/products');?>">Inventory</a></li>
       	<li class="last"><a href="<?php echo site_url($this->config->item('admin_folder').'/categories/sitemap');?>">Index</a></li>
    </ul>
</div><!-- End of breadcrumb --> 
<br>

<table class="gc_table" cellspacing="0" cellpadding="0">
    
	<tbody>
		<?php echo (count($categories) < 1)?'<tr><td style="text-align:center;" colspan="3">'.lang('no_categories').'</td></tr>':''?>
		<?php
		define('ADMIN_FOLDER', $this->config->item('admin_folder'));
		$this->load->model('Product_model');
		$Product_model = new Product_model;
		$site_url = site_url($this->config->item('admin_folder').'/products/category/');
		function list_categories($cats, $sub='', $Product_model, $site_url) {	
			foreach ($cats as $cat):?>
			<tr class="gc_row">
				<td><?php echo  $sub.$cat['category']->name; ?> (<a href="<?php echo $site_url."/".$cat['category']->id;?>"><?php echo $Product_model->count_products($cat['category']->id); ?></a>)</td>
			</tr>
			<?php
			if (sizeof($cat['children']) > 0)
			{
				$sub2 = str_replace('&rarr;&nbsp;', '&nbsp;', $sub);
					$sub2 .=  '&nbsp;&nbsp;&nbsp;&rarr;&nbsp;';
				list_categories($cat['children'], $sub2, $Product_model, $site_url);
			}
			endforeach;
		}
		
		list_categories($categories,'',$Product_model, $site_url);
		?>
	</tbody>
</table>
<?php include('footer.php');