<?php
$additional_header_info = '<style type="text/css">#gc_page_title {text-align:center;}</style>';
include('header.php'); ?>
<?php
$company	= array('id'=>'bill_company', 'class'=>'bill input', 'name'=>'company', 'value'=> set_value('company'));
$first		= array('id'=>'bill_firstname', 'class'=>'bill input bill_req', 'name'=>'firstname', 'value'=> set_value('firstname',$firstname));
$last		= array('id'=>'bill_lastname', 'class'=>'bill input bill_req', 'name'=>'lastname', 'value'=> set_value('lastname',$lastname));
$email		= array('id'=>'bill_email', 'class'=>'bill input bill_req', 'name'=>'email', 'value'=>set_value('email',$email));
$phone		= array('id'=>'bill_phone', 'class'=>'bill input bill_req', 'name'=>'phone', 'value'=> set_value('phone'));
?>
<link rel="stylesheet" href="<?php echo base_url();?>css/dropkick.css" type="text/css"/>
<script src="<?php echo base_url();?>js/jquery.dropkick-1.0.0.js" type="text/javascript"></script>
<link type="text/css" href="http://projects2.boxedge.com/malppy/js/jquery/theme/smoothness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
  <script type="text/javascript">
  var slct_box = jQuery.noConflict();
   slct_box(document).ready(function() {
      slct_box('#gender').dropkick();
    });
  </script>
<style>
#ui-datepicker-div .ui-datepicker-month, #ui-datepicker-div .ui-datepicker-year {
	top:0px;
	visibility:visible;
}
</style>
<div class="main_wrap">

<div id="static_container">
    	<div id="static_first_portion">
			<div id="static_left">
				<h1>Login or Create an Account</h1>
			</div><!-- End of static_left -->
			
				
        
        <div id="reg_wrap">          
        	<div id="registration">
				
				<?php if (!empty($error)) { ?>
				<div id="errmsg">
				<?php
				if ($this->session->flashdata('message'))
				{
					echo '<div class="gmessage">'.$this->session->flashdata('message').'</div>';
				}
				if ($this->session->flashdata('error'))
				{
					echo '<div class="error">'.$this->session->flashdata('error').'</div>';
				}
				if (!empty($error))
				{
					echo '<div class="error">'.$error.'</div>';
				}
				?>
				</div>
				<?php } ?>
			
  				<div id="reg">
				<?php echo form_open('secure/register_social'); ?>
				<input type="hidden" name="submitted" value="submitted" />
				<input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
				<input type="hidden" name="uid" value="<?php echo $uid; ?>" />
                	<h1>Register a new account</h1>
                    
					<div class="form_label"> <?php echo lang('address_firstname');?> *</div>
					<div class="form_input">
						<?php echo form_input($first);?>
					</div>	
                   	
					<div class="clear"></div>
					<div class="form_label"><?php echo lang('address_lastname');?> *</div>
					<div class="form_input">
						<?php echo form_input($last);?>
					</div>
					
					<div class="clear"></div>
					<div class="form_label">Email *</div>
					<div class="form_input">
						<?php echo form_input($email);?>
					</div>
					
					
					<div class="clear"></div>
					<div class="form_label">Gender</div>
					<div class="form_input">
						<select name="gender" id="gender">
                                  <option value="">Select</option>
                                  <option value="Male">Male</option>
                                  <option value="Female">Female</option>
                                </select>
					</div>	
					
					
					<div class="clear"></div>
					<div class="form_label_birth">Birthday</div>
					<div class="form_input">
						<div class="birth_col"><input type="text" value="" id="birth_col" /></div>
					</div>	
					
					<div class="clear"></div>
					
					<div id="form_submit">
						<p>* Required fields</p>
						<input id="register" type="submit" value="" />
						
					</div>
									
                    
                </div>                                  

        	</div><!-- End of login -->  
        </div><!-- End of login_wrap -->
		
	</div><!-- End of First Portion -->
    
    
		<div class="clear"></div>
     </div><!-- End of Container -->	
  <div class="clear"></div>
 
</div> 


<script type="text/javascript">

slct_box(document).ready(function(){
	slct_box('#birth_col').datepicker({ dateFormat: 'mm-dd-yy', changeMonth: true, changeYear: true, yearRange: '-100y:c+nn',
            maxDate: '-1d', altFormat: 'yy-mm-dd' });
});

</script>
<?php include('footer.php');