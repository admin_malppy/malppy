<?php include('header.php'); ?>
<!-- <link rel="stylesheet" href="<?php echo base_url();?>css/dropkick.css" type="text/css"/>
<script src="<?php echo base_url();?>js/jquery.dropkick-1.0.0.js" type="text/javascript"></script>
  <script type="text/javascript">
  var slct_box = jQuery.noConflict();
   slct_box(document).ready(function() {
      slct_box('#color').dropkick();
	  slct_box('#size').dropkick();
	  slct_box('.product_quantity').dropkick();
	  
    });
  </script>
 -->
<style>

.top_border {
    border-bottom: 1px solid #D5D5D5;
    margin: 10px 20px 30px;
}

#breadcrumb {
    margin: 0 20px;
    padding-bottom: 5px;
    padding-top: 10px;
}
#single_container {
    margin-top: -31px;
	width: 999px;
}
#single_product_essential_wrap {
    margin-top: 0;
    width: 750px;
	padding:0px;
	float: left;
}
#single_product_essential {
    padding-left: 20px;
}
#single_product_detail {
    padding-left: 15px;
    padding-right: 20px;
}
#single_product_desciption {
    float: left;
    padding-right: 20px;
    width: 360px;
}

#new_recomd ul li {
    float: left;
    list-style: none outside none;
    width: 50%;
    
}

.flexiwrap {
	height: 250px;
	width: auto;
}
#we_recomd_wrap {
    border-top: none;
    margin: 0 auto;
    padding-bottom: 20px;
    width: 220px;
}
#we_recomd_wrap div#new_recomd div#new_product_wrap div.flexiwrap {
    height: 290px;
    width: 220px;
}
.single_product_carousel_detail {
    line-height: 12px;
    margin:0px auto;
    padding: 0;
    width: 90px;
    text-align: left;
}
#new_product_wrap {
    margin-left: 0;
}
#new_recomd {
    height: auto;
    padding-top: 0;
    width: 990px;
}

#gc_tabs {
border:1px solid #FFF; 
width:100%;
background:#c2c2c2 none; padding:0px;
}
.ui-widget-header {
border:0; background:#FFF none; font-family:Verdana;
border-radius: 0px;
font-size:12px;
}
#gc_tabs .ui-widget-header h2 {
	font-size:12px;
	padding:0px;
}
#gc_tabs .ui-widget-content {
border:1px solid #aaaaaa; 
background:#ffffff none;
padding: 20px;
}

#gc_tabs .ui-widget-content ul {
	padding-left:30px;
}

#gc_tabs .ui-widget-content img {
	max-width:653px;
}

.ui-state-default, .ui-widget-content .ui-state-default {
background:#f6f6f6 none; border:1px solid #cccccc;
}
.ui-state-active, .ui-widget-content .ui-state-active {
background:#ffffff none; border:1px solid #aaaaaa;
}
.ui-tabs .ui-tabs-nav {
    padding: 0.2em 1em 0;
}
.ui-tabs .ui-tabs-nav li a {
	padding: 0.5em 2em;
}
.ui-tabs-panel {
	min-height:100px;
}

</style>

<script type="text/javascript">
jQuery(document).ready(function(){	
	jQuery("#gc_tabs").tabs();
}); 
</script>

<?php
	if ($this->session->flashdata('message'))
	{
		//echo '<div class="gmessage">'.$this->session->flashdata('message').'</div>';
		?>
		<script>
		alert('<?php echo $this->session->flashdata('message');?>');
		</script>
		<?php
	}
	if ($this->session->flashdata('error'))
	{
		//echo '<div class="error">'.$this->session->flashdata('error').'</div>';
		?>
		<script>
		alert('<?php echo $this->session->flashdata('error');?>');
		</script>
		<?php
	}
	if (!empty($error))
	{
		//echo '<div class="error">'.$error.'</div>';
		?>
		<script>
		alert('<?php echo $error;?>');
		</script>
		<?php
	}
?>

<div class="main_wrap">
	<div id="single_container">
			
		<div id="breadcrumb">
            <ul style="float:left;">
				<li><a href="<?php echo base_url();?>">Home</a></li>
            	<?php if ($this->Category_model->get_category($product->categories[1])->parent_id) { ?><li><a href="<?php echo @$this->Category_model->get_category($this->Category_model->get_category($product->categories[1])->parent_id)->slug; ?>"><?php echo @$this->Category_model->get_category($this->Category_model->get_category($product->categories[1])->parent_id)->name; ?></a></li><?php } ?>
                <li class="last"><a href="<?php echo $this->Category_model->get_category($product->categories[1])->slug; ?>" style="color: #006699;"><?php echo $this->Category_model->get_category($product->categories[1])->name; ?></a></li>
                <!-- <li><a href="#"><?php echo $product->name; ?></a></li> -->
        	</ul>

			<span style="float:right;"><img src="<?php echo base_url();?>images/header_fb.png" width="100" height="25" alt="fb" /></span>
        	<div class="clear"></div>
        </div><!-- End of breadcrumb --> 


		<div class="top_border"></div>
    	<div id="single_first_portion">
       
        
        <div id="single_product_essential_wrap">       

        	<div id="single_product_essential">
            	

                
                <div id="single_product_image_wrap">
                	<div id="single_product_image">
					<?php
					//get the primary photo for the product
					$photo	= '<img src="'.base_url('images/nopicture.png').'" alt="'.lang('no_image_available').'"/>';
					
					$primary	= $product->product_image;
					
					if($primary) {	
						
						if (strpos($primary, 'http') === 0) {
							$photo	= '<img src="'.base_url().'phpthumb/phpThumb.php?src='.$primary.'&w=300&h=390&far=1" alt="" border="" width="300" height="390"/>';
						} else {
							$photo	= '<img src="'.base_url().'phpthumb/phpThumb.php?src='.base_url('uploads/images/full/'.$primary).'&w=300&h=390&zc=1&f=png" width="300" height="390"'.$product->slug.'"/>';
						}
					}
					echo $photo;
					?>
					
                    <!-- <a class="cloud-zoom" href="<?php echo base_url('images/nopicture.png'); ?>" id="zoom1" rel="adjustX: 10, adjustY:-4, softFocus:true"><img align="left" alt="" src="<?php echo base_url('images/nopicture.png'); ?>" /></a> -->
   
    				<div class="mygallery">
	

</div>
                         
                    </div><!-- End of single_product_image --> 
                </div><!-- End of single_product_image_wrap --> 
                                    <?php echo form_open('cart/add_to_cart');?>
								
                <div id="single_product_desciption">
                    <h1><span class="color333333"><?php echo $product->name; ?></span></h1>
                    <?php if($product->promoprice == "0.00") {  ?>
					<h2><span class="colorcc0000"><?php echo format_currency($product->saleprice); ?></span></h2>
                    <?php } else { ?>
					<h2><span class="colorcc0000"><strike><?php echo format_currency($product->saleprice); ?></strike><br><?php echo format_currency($product->promoprice); ?></span></h2>
                    <?php } ?>
					<?php if (!$this->Customer_model->check_is_wishlist($product->id)) { ?>
					<p><a class="wl_btn" href="cart/add_to_wishlist/<?php echo $product->id;?>">+ Add to wishlist</a></p>
					<?php } ?>
					<p><?php echo $product->excerpt; ?></p>
                     <div class="clear"></div>
							<div id="product_size" style="width:100%">
								
		<?php if(count($options) > 0 && (bool)$product->track_stock && $product->quantity > 0): ?>
		<div class="product_section">
		<?php	
		foreach($options as $option):
			$required	= '';
			if($option->required)
			{
				$required = ' <span class="red">*</span>';
			}
			?>
			<div class="option_container" style="float:left;margin-right:20px;">
				<div class="product_excerpt"><?php echo $option->name.$required;?></div>
				<?php
				/*
				this is where we generate the options and either use default values, or previously posted variables
				that we either returned for errors, or in some other releases of Go Cart the user may be editing
				and entry in their cart.
				*/
						
				//if we're dealing with a textfield or text area, grab the option value and store it in value
				if($option->type == 'checklist')
				{
					$value	= array();
					if($posted_options && isset($posted_options[$option->id]))
					{
						$value	= $posted_options[$option->id];
					}
				}
				else
				{
					$value	= $option->values[0]->value;
					if($posted_options && isset($posted_options[$option->id]))
					{
						$value	= $posted_options[$option->id];
					}
				}
						
				if($option->type == 'textfield'):?>
				
					<input type="textfield" id="input_<?php echo $option->id;?>" name="option[<?php echo $option->id;?>]" value="<?php echo $value;?>" />
				
				<?php elseif($option->type == 'textarea'):?>
					
					<textarea id="input_<?php echo $option->id;?>" name="option[<?php echo $option->id;?>]"><?php echo $value;?></textarea>
				
				<?php elseif($option->type == 'droplist'):?>
					<select name="option[<?php echo $option->id;?>]">
						<option value=""><?php echo lang('choose_option');?></option>
				
					<?php foreach ($option->values as $values):
						$selected	= '';
						if($value == $values->id)
						{
							$selected	= ' selected="selected"';
						}?>
						
						<option<?php echo $selected;?> value="<?php echo $values->id;?>">
							<?php echo($values->price != 0)?'('.format_currency($values->price).') ':''; echo $values->name;?>
						</option>
					<?php endforeach;?>
					</select>
					<div class="clear" style="height:10px;"></div>
				<?php elseif($option->type == 'radiolist'):
						foreach ($option->values as $values):

							$checked = '';
							if($value == $values->id)
							{
								$checked = ' checked="checked"';
							}?>
							
							<div>
							<input<?php echo $checked;?> type="radio" name="option[<?php echo $option->id;?>]" value="<?php echo $values->id;?>"/>
							<?php echo($values->price != 0)?'('.format_currency($values->price).') ':''; echo $values->name;?>
							</div>
						<?php endforeach;?>
				
				<?php elseif($option->type == 'checklist'):
					foreach ($option->values as $values):

						$checked = '';
						if(in_array($values->id, $value))
						{
							$checked = ' checked="checked"';
						}?>
						<div class="gc_option_list">
						<input<?php echo $checked;?> type="checkbox" name="option[<?php echo $option->id;?>][]" value="<?php echo $values->id;?>"/>
						<?php echo($values->price != 0)?'('.format_currency($values->price).') ':''; echo $values->name;?>
						</div>
					<?php endforeach ?>
				<?php endif;?>
				</div>
		<?php endforeach;?>
	</div>
	<?php endif; ?>		
								
						</div>

						<div class="clear"></div>

                        <div id="product_size">
                           <!--  <h2>Choose your size</h2>                   
                                 <select name="country" id="size">
                                  <option value="">Select you size</option>
                                  <option value="AU">8</option>
                                  <option value="CA">9</option>
                                  <option value="DE">10</option>
                                </select> -->
								
								
								<input type="hidden" name="cartkey" value="<?php echo $this->session->flashdata('cartkey');?>" />
								<input type="hidden" name="id" value="<?php echo $product->id?>"/>
								<div style="text-align:left; overflow:hidden;">
									<?php if(!$this->config->item('allow_os_purchase') && ((bool)$product->track_stock && $product->quantity <= 0)) : ?>
										<!-- <h2 class="red"><?php echo lang('out_of_stock');?></h2> -->
									<?php else: ?>
										<?php if((bool)$product->track_stock && $product->quantity <= 0):?>
											<div class="red" style="color:#FF0000;"><small><?php echo lang('out_of_stock');?></small></div>
										<?php endif; ?>
										<?php if(!$product->fixed_quantity) : ?>
											<!-- <h2><?php echo lang('quantity') ?></h2> <input class="product_quantity" type="text" name="quantity" value="" size="6"/> -->
											<input class="product_quantity" type="hidden" name="quantity" value="1"/>
										<?php endif; ?>

										<?php if((bool)$product->track_stock && $product->quantity > 0):?>
										<input class="add_to_cart_btn" type="image" src="images/buyitnow.png" value="<?php echo lang('form_add_to_cart');?>" /> 
										<?php endif; ?>							
			
										<!-- <a href="#"><img src="images/buyitnow.png" width="175" height="50" alt="buy" /></a>  -->
									<?php endif;?>
								</div>
								</form>
								
                            
                        </div>
                        <div id="product_color">
							<?php if((bool)$product->track_stock && $product->quantity > 0):?>
							<h2><span class="colorff9900"><?php echo lang('quantity') ?></span></h2> 
								<select name="quantity" id="quantity" style="width:60px;">
                                  	<option value="1">1</option>
                               		<option value="2">2</option>
                               		<option value="3">3</option>
									<option value="4">4</option>
                               		<option value="5">5</option>
                               		<option value="6">6</option>
									<option value="7">7</option>
                               		<option value="8">8</option>
                               		<option value="9">9</option>
									<option value="10">10</option>
                               </select>
							<?php endif;?>
                            <!-- <h2><span class="colorff9900">Size conversion chart</span></h2>         
                                  <select name="country" id="color">
                                  <option value="">Select you color</option>
                                  <option value="AU">Black</option>
                                  <option value="CA">Brown</option>
                                  <option value="DE">Red</option>
                                </select> -->
                                <div class="clear"></div> 
                               
          
                        </div> <!-- End of product_color -->   

					<div class="clear"></div>

							<div class="addthis">
							<div id="social_sharing">
							<!-- AddThis Button BEGIN -->
							<div class="addthis_toolbox addthis_default_style ">
							<a class="addthis_button_preferred_1"></a>
							<a class="addthis_button_preferred_2"></a>
							<a class="addthis_button_preferred_3"></a>
							<a class="addthis_button_preferred_4"></a>
							<a class="addthis_button_compact"></a>
							<a class="addthis_counter addthis_bubble_style"></a>
							</div>
							<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4e4ed7263599fdd0"></script>
							<!-- AddThis Button END -->
						    </div> <!-- End of social_sharing --> 
 						   </div> <!-- End of addthis -->   
                 
                </div><!-- End of single_product_detail --> 
                
                

				<div class="clear"></div>
                
              <div id="single_product_detail">

				<div id="gc_tabs">
					<ul>
						<li><a href="#gc_info"><h2><span class="color006699">INFO & CARE</span></h2></a></li>
						<li><a href="#gc_specification"><h2><span class="color006699">SPECIFICATION</span></h2></a></li>
					</ul>

					<div id="gc_info">
						<?php echo $product->info; ?>				
					</div>
					<div id="gc_specification">
						<?php echo $product->specification; ?>				
					</div>
				</div>
				
				


				<p>&nbsp;</p>

                <h2><span class="color006699">PRODUCTS DETAILS</span></h2>
                
                <div class="product_description_wrap">
               		<?php echo $product->description; ?>
				</div>
				
				<p>&nbsp;</p>

				<div class="top_border"></div>

				<!-- Rating Script -->
				<div id="rate_pane">
				<label class="color006699" style="float:left;margin-left:20px;margin-right:10px;font-size:14px;">Rate this product:</label>
				<input name="star1" type="radio" class="star"/>
				<input name="star1" type="radio" class="star"/>
				<input name="star1" type="radio" class="star"/>
				<input name="star1" type="radio" class="star"/>
				<input name="star1" type="radio" class="star"/>
				</div>

				<div class="clear"></div>

				<div id="disqus_thread" style="margin-top:30px;margin-left:20px;width:640px;"></div>
				<script type="text/javascript">
				/* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
				var disqus_shortname = 'malppy'; // required: replace example with your forum shortname
				var disqus_identifier = 'malppy'+"_"+'<?php echo $product->id;?>';
				var disqus_title = '<?php echo addslashes($product->name);?>';
				/* * * DON'T EDIT BELOW THIS LINE * * */
				(function() {
				var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
				dsq.src = 'http://' + disqus_shortname + '.disqus.com/embed.js';
				(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
				})();
				</script>
				<noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
				<a href="http://disqus.com" class="dsq-brlink"></a>
				<script type="text/javascript">
				/* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
				var disqus_shortname = 'malppy'; // required: replace example with your forum shortname
				var disqus_identifier = 'malppy'+"_"+'<?php echo $product->id;?>';
				var disqus_title = '<?php echo addslashes($product->name);?>';

				/* * * DON'T EDIT BELOW THIS LINE * * */
				(function () {
				var s = document.createElement('script'); s.async = true;
				s.type = 'text/javascript';
				s.src = 'http://' + disqus_shortname + '.disqus.com/count.js';
				(document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
				}());
				</script>

				<p><br><br></p>
                
                </div><!-- End of single_product_detail -->
        	</div><!-- End of single_product_essential -->  
        </div><!-- End of single_product_essential_wrap -->

		<div id="right_recomment" style="float: right; margin-right: 25px; width:220px">
			
			<?php if(!empty($related)):?>
			<p class="title14">We Recommend</p>
			
			<div id="we_recomd_wrap">
        
        	<div id="new_recomd" style="width:100%">       
		    <div id="new_product_wrap" style="width:100%">
                <ul style="width:100%">

					<?php
					$cat_counter=1;
					foreach($related as $product):
					?>
					<?php
						$photo	= '<img src="'.base_url('images/nopicture.png').'" alt="'.lang('no_image_available').'"/>';
						$product->images	= array_values($product->images);

						if(!empty($product->images[0]))
						{
							$primary	= $product->images[0];
							foreach($product->images as $photo)
							{
								if(isset($photo->primary))
								{
									$primary	= $photo;
								}
							}

							if (strpos($primary->filename, 'http') === 0) {
								$photo	= '<img src="'.base_url().'phpthumb/phpThumb.php?src='.$primary->filename.'&w=90&h=117&zc=1" alt="'.$product->seo_title.'"/>';
							} else {
								$photo	= '<img src="'.base_url().'phpthumb/phpThumb.php?src='.base_url('uploads/images/small/'.$primary->filename).'&w=90&h=117&zc=1" alt="'.$product->seo_title.'"/>';
							}

							//$photo	= '<img src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$product->seo_title.'" width="120" height="120" />';
						}
					?>
                    <li>
                    	<div class="recommend_product_wrap">
							<a href="<?php echo site_url($product->slug); ?>">
								<?php echo $photo; ?>
							</a>
	                    	<div class="single_product_carousel_detail">
	                        	<p style="font-size:10px;"><?php echo character_limiter($product->name,25);?></p>
	                            <p style="font-size:10px;"><span class="color000"><strong><?php echo format_currency($product->saleprice); ?></strong></span></p>
	                        </div>
                        </div>
                    </li>
					<?php endforeach; ?>             
                    
                </ul>
            </div>  <!-- End of new_product_wrap -->  	
			
        </div><!-- End of new_arrival -->
		<?php endif; ?>
		</div>

		<div class="clear"></div>
		</div>

	</div><!-- End of First Portion -->
		<div class="clear"></div>
     </div><!-- End of Container -->	
  <div class="clear"></div>
</div><!-- End of Main Wrapper -->
<div class="clear"></div>

        <div class="gap"></div>

<script type="text/javascript">
/*jQuery(function(){ 
	jQuery('.category_box').width();
	jQuery('#new_recomd ul').each(function(){
		jQuery(this).children().equalHeights();
	});	
});*/
</script>


<?php include('footer.php'); ?>

<?php exit; ?>

<div id="we_recomd_wrap">
        <?php if(!empty($related)):?>
        <div id="new_recomd" >       
		    <div id="new_product_wrap">
                <ul class="recomd">

					<?php
					$cat_counter=1;
					foreach($related as $product):
					?>
					<?php
						$photo	= '<img src="'.base_url('images/nopicture.png').'" alt="'.lang('no_image_available').'"/>';
						$product->images	= array_values($product->images);

						if(!empty($product->images[0]))
						{
							$primary	= $product->images[0];
							foreach($product->images as $photo)
							{
								if(isset($photo->primary))
								{
									$primary	= $photo;
								}
							}

							if (strpos($primary->filename, 'http') === 0) {
								$photo	= '<img src="'.base_url().'phpthumb/phpThumb.php?src='.$primary->filename.'&w=220&h=220&far=1" alt="'.$product->seo_title.'"/>';
							} else {
								$photo	= '<img src="'.base_url().'phpthumb/phpThumb.php?src='.base_url('uploads/images/small/'.$primary->filename).'&w=220&h=220&far=1" alt="'.$product->seo_title.'"/>';
							}

							//$photo	= '<img src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$product->seo_title.'" width="220" height="220" />';
						}
					?>
                    <li>
						<a href="<?php echo site_url($product->slug); ?>">
							<?php echo $photo; ?>
						</a><!-- <img src="images/single_carousel.jpg" width="220" height="220"  alt="We Recommend" /> -->
                    	<div class="single_product_carousel_detail">
                        	<h2><?php echo $product->name;?></h2>
                            <h3><span class="color333333"><?php echo $product->sku;?></span></h3>
                            <h3><span class="color000"><strong><?php echo format_currency($product->saleprice); ?></strong></span></h3>
                        </div>
                    </li>
					<?php endforeach; ?>
                                     
                    
                </ul>
            </div>  <!-- End of new_product_wrap -->  	
			
        </div><!-- End of new_arrival -->
		<?php endif; ?>
</div>


  
<div class="clear"></div>
<div id="social_sharing">
	<!-- AddThis Button BEGIN -->
	<div class="addthis_toolbox addthis_default_style ">
	<a class="addthis_button_preferred_1"></a>
	<a class="addthis_button_preferred_2"></a>
	<a class="addthis_button_preferred_3"></a>
	<a class="addthis_button_preferred_4"></a>
	<a class="addthis_button_compact"></a>
	<a class="addthis_counter addthis_bubble_style"></a>
	</div>
	<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4e4ed7263599fdd0"></script>
	<!-- AddThis Button END -->
</div>

<div id="product_left">
	<div id="product_image">
		<?php
		//get the primary photo for the product
		$photo	= '<img src="'.base_url('images/nopicture.png').'" alt="'.lang('no_image_available').'"/>';

		if(count($product->images) > 0 )
		{	
			$primary	= $product->images[0];
			foreach($product->images as $image)
			{
				if(isset($image->primary))
				{
					$primary	= $image;
				}
			}

			$photo	= '<a href="'.base_url('uploads/images/medium/'.$primary->filename).'" rel="gallery" title="'.$primary->caption.'"><img src="'.base_url('uploads/images/small/'.$primary->filename).'" alt="'.$product->slug.'"/></a>';
		}
		echo $photo;
	
	
		if(!empty($primary->caption)):?>
		<div id="product_caption">
			<?php echo $primary->caption;?>
		</div>
		<?php endif;?>
	</div>

	<?php

	$img_counter	= 1;
	if(count($product->images) > 0):?>
	<div id="product_thumbnails">
		<?php foreach($product->images as $image): 
			if($image != $primary):
		?>
			<div class="product_thumbnail" <?php if($img_counter == 3){echo'style="margin-right:0px;"'; $img_counter=1;}else{$img_counter++;}?>>
				<a rel="gallery" href="<?php echo base_url('uploads/images/medium/'.$image->filename);?>" title="<?php echo $image->caption;?>"><img src="<?php echo base_url('uploads/images/thumbnails/'.$image->filename);?>"/></a>
			</div>
		<?php endif;
		endforeach;?>

	</div>
	<?php endif;?>
</div>


<?php echo form_open('cart/add_to_cart');?>
	
<input type="hidden" name="cartkey" value="<?php echo $this->session->flashdata('cartkey');?>" />
<input type="hidden" name="id" value="<?php echo $product->id?>"/>

<div id="product_right">	
	<div class="product_section">
		<div class="product_sku"><?php echo lang('sku');?>: <?php echo $product->sku; ?></div> 
		<?php if($product->saleprice > 0):?>
			<span class="price_slash"><?php echo lang('product_price');?> <?php echo format_currency($product->price); ?></span>
			<span class="price_sale"><?php echo lang('product_sale');?> <?php echo format_currency($product->saleprice); ?></span>
		<?php else: ?>
			<span class="price_reg"><?php echo lang('product_price');?> <?php echo format_currency($product->price); ?></span>
		<?php endif;?>
	</div>
	
	<?php if(count($options) > 0): ?>
		<div class="product_section">
		<h2><?php echo lang('available_options');?></h2>
		<?php	
		foreach($options as $option):
			$required	= '';
			if($option->required)
			{
				$required = ' <span class="red">*</span>';
			}
			?>
			<div class="option_container">
				<div style="font-family: "Century_Gothic";"><?php echo $option->name.$required;?></div>
				<?php
				/*
				this is where we generate the options and either use default values, or previously posted variables
				that we either returned for errors, or in some other releases of Go Cart the user may be editing
				and entry in their cart.
				*/
						
				//if we're dealing with a textfield or text area, grab the option value and store it in value
				if($option->type == 'checklist')
				{
					$value	= array();
					if($posted_options && isset($posted_options[$option->id]))
					{
						$value	= $posted_options[$option->id];
					}
				}
				else
				{
					$value	= $option->values[0]->value;
					if($posted_options && isset($posted_options[$option->id]))
					{
						$value	= $posted_options[$option->id];
					}
				}
						
				if($option->type == 'textfield'):?>
				
					<input type="textfield" id="input_<?php echo $option->id;?>" name="option[<?php echo $option->id;?>]" value="<?php echo $value;?>" />
				
				<?php elseif($option->type == 'textarea'):?>
					
					<textarea id="input_<?php echo $option->id;?>" name="option[<?php echo $option->id;?>]"><?php echo $value;?></textarea>
				
				<?php elseif($option->type == 'droplist'):?>
					<select name="option[<?php echo $option->id;?>]">
						<option value=""><?php echo lang('choose_option');?></option>
				
					<?php foreach ($option->values as $values):
						$selected	= '';
						if($value == $values->id)
						{
							$selected	= ' selected="selected"';
						}?>
						
						<option<?php echo $selected;?> value="<?php echo $values->id;?>">
							<?php echo($values->price != 0)?'('.format_currency($values->price).') ':''; echo $values->name;?>
						</option>
						
					<?php endforeach;?>
					</select>
				<?php elseif($option->type == 'radiolist'):
						foreach ($option->values as $values):

							$checked = '';
							if($value == $values->id)
							{
								$checked = ' checked="checked"';
							}?>
							
							<div>
							<input<?php echo $checked;?> type="radio" name="option[<?php echo $option->id;?>]" value="<?php echo $values->id;?>"/>
							<?php echo($values->price != 0)?'('.format_currency($values->price).') ':''; echo $values->name;?>
							</div>
						<?php endforeach;?>
				
				<?php elseif($option->type == 'checklist'):
					foreach ($option->values as $values):

						$checked = '';
						if(in_array($values->id, $value))
						{
							$checked = ' checked="checked"';
						}?>
						<div class="gc_option_list">
						<input<?php echo $checked;?> type="checkbox" name="option[<?php echo $option->id;?>][]" value="<?php echo $values->id;?>"/>
						<?php echo($values->price != 0)?'('.format_currency($values->price).') ':''; echo $values->name;?>
						</div>
					<?php endforeach ?>
				<?php endif;?>
				</div>
		<?php endforeach;?>
	</div>
	<?php endif; ?>
	<div class="product_section">	
	
		<div style="text-align:center; overflow:hidden;">
			<?php if(!$this->config->item('allow_os_purchase') && ((bool)$product->track_stock && $product->quantity <= 0)) : ?>
				<h2 class="red"><?php echo lang('out_of_stock');?></h2>				
			<?php else: ?>
				<?php if((bool)$product->track_stock && $product->quantity <= 0):?>
					<div class="red"><small><?php echo lang('out_of_stock');?></small></div>
				<?php endif; ?>
				<?php if(!$product->fixed_quantity) : ?>
					<?php echo lang('quantity') ?> <input class="product_quantity" type="text" name="quantity" value=""/>
				<?php endif; ?>
				<input class="add_to_cart_btn" type="submit" value="<?php echo lang('form_add_to_cart');?>" /> 
			<?php endif;?>
		</div>
	</div>
		
	</form>
	<div class="tabs">
		<ul>
			<li><a href="#description_tab"><?php echo lang('tab_description');?></a></li>
			<?php if(!empty($related)):?><li><a href="#related_tab"><?php echo lang('tab_related_products');?></a></li><?php endif;?>
		</ul>
		<div id="description_tab">
			<?php echo $product->description; ?>
		</div>
		
		<?php if(!empty($related)):?>
		<div id="related_tab">
			<?php
			$cat_counter=1;
			foreach($related as $product):
				if($cat_counter == 1):?>

				<div class="category_container">

				<?php endif;?>

				<div class="category_box">
					<div class="thumbnail">
						<?php
						$photo	= '<img src="'.base_url('images/nopicture.png').'" alt="'.lang('no_image_available').'"/>';
						$product->images	= array_values($product->images);

						if(!empty($product->images[0]))
						{
							$primary	= $product->images[0];
							foreach($product->images as $photo)
							{
								if(isset($photo->primary))
								{
									$primary	= $photo;
								}
							}

							if (strpos($primary->filename, 'http') === 0) {
								$photo	= '<a href="'.base_url().'phpthumb/phpThumb.php?src='.$primary->filename.'&w=220&h=220&far=1" rel="gallery" title="'.$primary->caption.'"><img src="'.$primary->filename.'" alt="'.$product->slug.'" width="300"/></a>';
							} else {
								$photo	= '<a href="'.base_url('uploads/images/medium/'.$primary->filename).'" rel="gallery" title="'.$primary->caption.'"><img src="'.base_url('uploads/images/small/'.$primary->filename).'" alt="'.$product->slug.'"/></a>';
							}
							//$photo	= '<img src="'.base_url('uploads/images/thumbnails/'.$primary->filename).'" alt="'.$product->seo_title.'"/>';
						}
						?>
						<a href="<?php echo site_url($product->slug); ?>">
							<?php echo $photo; ?>
						</a>
					</div>
					<div class="gc_product_name">
						<a href="<?php echo site_url($product->slug); ?>"><?php echo $product->name;?></a>
					</div>
					<?php if($product->excerpt != ''): ?>
					<div class="excerpt"><?php echo $product->excerpt; ?></div>
					<?php endif; ?>
					<div>
						<?php if($product->saleprice > 0):?>
							<span class="gc_price_slash"><?php echo lang('product_price');?> <?php echo $product->price; ?></span>
							<span class="gc_price_sale"><?php echo lang('product_sale');?> <?php echo $product->saleprice; ?></span>
						<?php else: ?>
							<span class="gc_price_reg"><?php echo lang('product_price');?> <?php echo $product->price; ?></span>
						<?php endif; ?>
	                    <?php if((bool)$product->track_stock && $product->quantity < 1) { ?>
							<div class="gc_stock_msg"><?php echo lang('out_of_stock');?></div>
						<?php } ?>
					</div>
				</div>
			
				<?php 
				$cat_counter++;
				if($cat_counter == 5):?>
			
				
				</div>

				<?php 
				$cat_counter = 1;
				endif;
			endforeach;
		
			if($cat_counter != 1):?>
					<br class="clear"/>
				</div>
			<?php endif;?>
		</div>
		<?php endif;?>
	</div>

</div>

<script type="text/javascript"><!--
$(function(){ 
	$('.tabs').tabs();
	
	$('a[rel="gallery"]').colorbox({ width:'80%', height:'80%', scalePhotos:true });
	
	$('#related_tab').width($('#description_tab').width());

	var w	= parseInt(($('#related_tab').width()/4)-33);

	$('.category_box').width();
	$('.category_container').each(function(){
		$(this).children().equalHeights();
	});	
});
//--></script>