<?php include('header.php');?>



<script>
var jQuery = jQuery.noConflict();
jQuery(document).ready(function(){
	jQuery('.delete_address').click(function(){
		if(jQuery('.delete_address').length > 1)
		{
			if(confirm('<?php echo lang('delete_address_confirmation');?>'))
			{
				jQuery.post("<?php echo site_url('secure/delete_address');?>", { id: jQuery(this).attr('rel') },
					function(data){
						jQuery('#address_'+data).remove();
						jQuery('#address_list .my_account_address').removeClass('address_bg');
						jQuery('#address_list .my_account_address:even').addClass('address_bg');
					});
			}
		}
		else
		{
			alert('<?php echo lang('error_must_have_address');?>');
		}	
	});
	
	jQuery('.edit_address').click(function(){
		jQuery.fn.colorbox({	href: '<?php echo site_url('secure/address_form'); ?>/'+jQuery(this).attr('rel'), width:'600px', height:'500px'}, function(){
			jQuery('input:submit, input:button, button').button();
		});
	});

	jQuery('.share_wishlist').click(function(){
		jQuery.fn.colorbox({	href: '<?php echo site_url('secure/my_wishlist'); ?>/'+jQuery(this).attr('rel'), width:'600px', height:'500px'}, function(){
			
		});
	});
	
	if (jQuery.browser.webkit) {
	    jQuery('input:password').attr('autocomplete', 'off');
	}
});


function set_default(address_id, type)
{
	jQuery.post('<?php echo site_url('secure/set_default_address') ?>/',{id:address_id, type:type});
}


</script>

<div class="main_wrap">
	<div class="container">

<div id="my_account_container">

<?php
//$company	= array('id'=>'company', 'class'=>'input', 'name'=>'company', 'value'=> set_value('company', $customer['company']));
$first		= array('id'=>'firstname', 'class'=>'input', 'name'=>'firstname', 'value'=> set_value('firstname', $customer['firstname']));
$last		= array('id'=>'lastname', 'class'=>'input', 'name'=>'lastname', 'value'=> set_value('lastname', $customer['lastname']));
$email		= array('id'=>'email', 'class'=>'input', 'name'=>'email', 'value'=> set_value('email', $customer['email']));
$phone		= array('id'=>'phone', 'class'=>'input', 'name'=>'phone', 'value'=> set_value('phone', $customer['phone']));

$password	= array('id'=>'password', 'class'=>'input', 'name'=>'password', 'value'=>'');
$confirm	= array('id'=>'confirm', 'class'=>'input', 'name'=>'confirm', 'value'=>'');
?>	
	<div id="my_account_info">
	
		<?php
		if(validation_errors())
		{
			echo '<div id="errmsg" style="width:925px;"><div class="error">'.validation_errors().'</div></div><div class="clear"></div>';
		}
		?>
		<!-- <p style="margin-left:10px;"><b>Hello <?php echo $this_customer['firstname']; ?> <?php echo $this_customer['lastname']; ?>!</b></p> -->
		<div class="clear">&nbsp;</div>
		<div id="my_information">
			<?php echo form_open('secure/my_account'); ?>
				<h1>Account Information</h1>
				
				<!-- <div class="form_label"> <?php echo lang('account_company');?></div>
				<div class="form_input">
					<?php echo form_input($company);?>
				</div>	 -->
                  	
				<div class="clear"></div>
				<div class="form_label"><?php echo lang('account_firstname');?><b class="r"> *</b></div>
				<div class="form_input">
					<?php echo form_input($first);?>
				</div>
				<div class="form_label"><?php echo lang('account_lastname');?><b class="r"> *</b></div>
				<div class="form_input">
					<?php echo form_input($last);?>
				</div>
				
				<div class="clear"></div>
				<div class="form_label"><?php echo lang('account_email');?><b class="r"> *</b></div>
				<div class="form_input">
					<?php echo form_input($email);?>
				</div>
				<div class="form_label"><?php echo lang('account_phone');?><b class="r"></b></div>
				<div class="form_input">
					<?php echo form_input($phone);?>
				</div>
				<!-- <div class="form_wrap">
					<div>
						<?php echo lang('account_company');?><br/>
						<?php echo form_input($company);?>
					</div>
				</div>
				<div class="form_wrap">
					<div>
						<?php echo lang('account_firstname');?><b class="r"> *</b><br/>
						<?php echo form_input($first);?>
					</div>
					<div >
						<?php echo lang('account_lastname');?><b class="r"> *</b><br/>
						<?php echo form_input($last);?>
					</div>
				</div> 
				
				<div class="form_wrap">
					<div>
						<?php echo lang('account_email');?><b class="r"> *</b><br/>
						<?php echo form_input($email);?>
					</div>
					<div >
						<?php echo lang('account_phone');?><b class="r"> *</b><br/>
						<?php echo form_input($phone);?>
					</div>
				</div>-->
				
				<div class="form_wrap">
					<div>
						<input type="checkbox" name="email_subscribe" value="1" <?php if((bool)$customer['email_subscribe']) { ?> checked="checked" <?php } ?>/> <small>Subscribe to our newsletter</small>
					</div>
					
				</div>
				<div class="clear"></div>
				<div style="margin-top:20px; margin-bottom:20px; padding:0px; float:none; text-align:left; font-weight:bold;"><small><?php echo lang('account_password_instructions');?></small></div>
					
				
				<div class="clear"></div>
				<div class="form_label"><?php echo lang('account_password');?><b class="r"> *</b></div>
				<div class="form_input">
					<?php echo form_input($password);?>
				</div>
				<div class="form_label"><?php echo lang('account_confirm');?><b class="r"> *</b></div>
				<div class="form_input">
					<?php echo form_input($confirm);?>
				</div>
				<!-- <div class="clear"></div>
				<div class="form_wrap">
					<div style="margin-top:20px; margin-bottom:0px; padding:0px; float:none; text-align:left;"><small><?php echo lang('account_password_instructions');?></small></div>
					<div>
						<?php echo lang('account_password');?><br/>
						<?php echo form_password($password);?>
					</div>
					<div >
						<?php echo lang('account_confirm');?><br/>
						<?php echo form_password($confirm);?>
					</div>
				</div> -->
				<div class="clear"></div>
				<br><br>
				<div class="form_wrap" style="text-align:center;">
					<input type="submit" class="submit" value="Save Information"  />
				</div>
			</form>
			<p><br><br></p>
		</div>
		
		<div id="address_manager">
			<input type="button" class="edit_address right" rel="0" value="<?php echo lang('add_address');?>"/>
			<h1><?php echo lang('address_manager');?></h1>
			<script type="text/javascript">
			//$(document).ready(function(){
				//$('#address_list .my_account_address:even').addClass('address_bg');	
			//});
			</script>
			<div id="address_list">
				
			<?php
			$c = 1;
			foreach($addresses as $a):?>
				<div class="my_account_address" id="address_<?php echo $a['id'];?>">
					<div class="address_toolbar">
						<input type="button"class="delete_address" rel="<?php echo $a['id'];?>" value="<?php echo lang('form_delete');?>" />
						<input type="button"class="edit_address" rel="<?php echo $a['id'];?>" value="<?php echo lang('form_edit');?>" />
						<br>
						<input type="radio" name="bill_chk" onclick="set_default(<?php echo $a['id'] ?>, 'bill')" <?php if($customer['default_billing_address']==$a['id']) echo 'checked="checked"'?> /> <?php echo lang('default_billing');?> <input type="radio" name="ship_chk" onclick="set_default(<?php echo $a['id'] ?>,'ship')" <?php if($customer['default_shipping_address']==$a['id']) echo 'checked="checked"'?>/> <?php echo lang('default_shipping');?>
					</div>
					<?php
					$b	= $a['field_data'];
					echo nl2br(format_address($b));
					?>
				</div>
			<?php endforeach;?>
			</div>
		</div>
		<br class="gc_clr"/>
	</div>
	<br class="gc_clr"/>
</div>
<br class="gc_clr"/>
<div class="clear"></div>
<h2 class="title" style="padding:20px 0px 10px 0px; border-bottom:2px solid #ccc; margin-bottom:10px;text-align:center;">My Wishlist <span style="font-size:11px;font-weight:normal">(<a href="#" class="share_wishlist" onclick="return false;">Share</a>)</span></h2>
<table class="cart_table" cellpadding="0" cellspacing="0" border="0">
	<thead>
		<tr>
			<th class="product_info">Product Name</th>
			<th>Product Code</th>
			<th></th>
		</tr>
	</thead>

	<tbody class="cart_items" style="text-align:left;">
	<?php
	foreach($wishlists as $wishlist): ?>
		<tr class="cart_spacer"><td colspan="7"></td></tr>
		<tr class="cart_item">
			<td>
				<?php $p = $this->Product_model->get_product($wishlist->product_id); 
				
				echo strip_tags($p->name);
				
				?>
			</td>
			<td><?php echo $p->sku; ?></td>
			<td><a href="<?php echo site_url($p->slug); ?>">View Detail</a></td>
		</tr>
		
	<?php endforeach;?>
	</tbody>
</table>

<br class="gc_clr"/>
<div class="clear"></div>
<div style="text-align:center;">
<h2 class="title" style="padding:20px 0px 10px 0px; border-bottom:2px solid #ccc; margin-bottom:10px;"><?php echo lang('order_history');?></h2>
<?php if($orders):
	echo $orders_pagination;
?>
<table class="cart_table" cellpadding="0" cellspacing="0" border="0">
	<thead>
		<tr>
			<th class="product_info"><?php echo lang('order_date');?></th>
			<th><?php echo lang('order_number');?></th>
			<th><?php echo lang('order_status');?></th>
		</tr>
	</thead>

	<tbody class="cart_items" style="text-align:left;">
	<?php
	foreach($orders as $order): ?>
		<tr class="cart_spacer"><td colspan="7"></td></tr>
		<tr class="cart_item">
			<td>
				<?php $d = format_date($order->ordered_on); 
				
				$d = explode(' ', $d);
				echo $d[0].' '.$d[1].', '.$d[3];
				
				?>
			</td>
			<td><?php echo $order->order_number; ?></td>
			<td><?php echo $order->status;?></td>
		</tr>
		
	<?php endforeach;?>
	</tbody>
</table>
<?php else: ?>
	<?php echo lang('no_order_history');?>
<?php endif;?>

<br><br><br>

	</div>
</div>

<?php include('footer.php');?>
