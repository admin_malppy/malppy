
	<div class="clear"></div>
	  <div  id="about_clearance_social">
        			<div id="clearance"><img src="<?php echo base_url();?>images/clearance.png" width="200" height="300" alt="clearance" /></div>
                    <div id="freeshipping"><img src="<?php echo base_url();?>images/freeshiping.png" width="200" height="300" alt="freeshipping" /></div>
                    <div id="fblike_box"><img src="<?php echo base_url();?>images/fblike_box.png" width="260" height="300" alt="fb" /></div>
				<?php echo form_open('cart/newsletter');?>
				<div id="subscribe">
                    	<h1>Subscribe to our fun and informative weekly newsletter</h1>
                        <div id="subscrb"><input name="newsletter_email" type="text" /></div>
                        <div id="newsltr_submit"><button type="submit">&nbsp;</button></div>
                        <div class="clear"></div>
        				<p>Personal data will not be disclosed to any third parties. You can unsubscribe from our newsletter at any time. Thanks a lot !</p>
        				<img src="<?php echo base_url();?>images/maplpy.png" width="270" height="37" />   
        		</div><!-- End of subscribe -->
				</form>
        </div><!-- End of clearance_social -->
        <div class="gap"></div>
		<div class="clear"></div>
        
		<?php  if($this->uri->uri_string() == '') : ?>

		<div class="clear"></div>
		<?php endif; ?>

		<div id="footer_wrap">

 
 

 


	
    <div id="footer_cus_serv">
    	<h1>Customer Services</h1>
        <ul>
        	<li><a href="<?php echo base_url();?>contact">Contact Info</a></li>
            <li><a href="<?php echo base_url();?>returns-shipping">Returns Shipping</a></li>
            <li><a href="<?php echo base_url();?>faq">FAQ's </a></li>
            <li><a href="<?php echo base_url();?>customer-service-center">Customer Service Center</a></li>
            <li><a href="<?php echo base_url();?>safe-shopping-guarantee">Safe Shopping Guarantee </a></li>
            <li><a href="<?php echo base_url();?>secure-shopping">Secure Shopping</a></li>            
        </ul>
	
    </div><!-- End of footer_cus_serv -->
    
    <div id="footer_about">
    	<h1>About</h1>
        <ul>
        	<li><a href="<?php echo base_url();?>who-we-are">MALPPY</a></li>
            <li><a href="<?php echo base_url();?>careers">Careers</a></li>
			<li><a href="<?php echo base_url();?>press">Press/Media</a></li> 
            <li><a href="<?php echo base_url();?>terms">Terms &amp; Conditions</a></li>
            <li><a href="<?php echo base_url();?>policy">Privacy Policy</a></li>       
        </ul>	
    </div><!-- End of footer_about -->    




   
  <div id="footer_join_us">
   	<h1>Join Us On</h1>
      <ul>
       	  <li><a href="http://www.facebook.com"><img src="<?php echo base_url();?>images/footer_fb.png" width="105" height="25" alt="fb" /></a></li>
      	  <li><img src="<?php echo base_url();?>images/header_fb.png" width="100" height="25" alt="fb" /></li>
		</ul>	
    </div><!-- End of footer_join_us -->
    
     <div id="footer_shop_us">
      <h1>Shop With Us</h1>
      <ul>
	  	  <?php foreach ($this->categories as $cat) { ?>
       	  <li><a href="<?php echo $cat['category']->slug; ?>"><?php echo $cat['category']->name; ?></a></li>
		  <?php } ?>
      </ul>	
	
    </div><!-- End of footer_shop_us -->
    
     <div id="footer_payment_method">
     <h1>Our Payment Methods</h1>
     <div class="pay_method"><a href="#"><img src="<?php echo base_url();?>images/visa.png" width="32" height="23" alt="visa" /></a></div>
     <div class="pay_method"><a href="#"><img src="<?php echo base_url();?>images/master.png" width="32" height="23" alt="visa" /></a></div>
     <div class="pay_method"><a href="#"><img src="<?php echo base_url();?>images/paypal.png" width="32" height="23" alt="visa" /></a></div>
     <div class="pay_method"><a href="#"><img src="<?php echo base_url();?>images/cash.png" width="32" height="23" alt="visa" /></a></div>
     <div class="pay_method"><a href="#"><img src="<?php echo base_url();?>images/master2.png" width="32" height="23" alt="visa" /></a></div>
     <div class="footer_space"></div>
     <div class="clear"></div>
     	<h1>We deliver by</h1>
    	<a href="http://www.ups.com" target="_blank"><img src="<?php echo base_url();?>images/ups.png" width="75" height="75" alt="ups" /></a>
    </div><!-- End of footer_payment_method -->    
	
    <div class="clear"></div>   
    <div class="footer_copyright">     
    	COPYRIGHT (C) <a href="http://www.malppy.com">MALPPY.COM</a>. LTD. ALL RIGHTS RESERVED
    </div>
    
</div><!-- End of footer_wrap -->

</div><!-- End of main_wrapper -->
</body>
</html>