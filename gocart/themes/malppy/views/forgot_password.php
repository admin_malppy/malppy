<?php 
$additional_header_info = '<style type="text/css">#page_title {text-align:center;}</style>';
include('header.php'); ?>

<div class="main_wrap">

	<div id="static_container">
    	<div id="static_first_portion">
        <div id="static_left">
        	<h1>Password</h1>
		</div><!-- End of static_left -->
        
        <div id="login_wrap">          
		
			<?php if (!empty($error)) { ?>
				<div id="errmsg">
				<?php
				if ($this->session->flashdata('message'))
				{
					echo '<div class="gmessage">'.$this->session->flashdata('message').'</div>';
				}
				if ($this->session->flashdata('error'))
				{
					echo '<div class="error">'.$this->session->flashdata('error').'</div>';
				}
				if (!empty($error))
				{
					echo '<div class="error">'.$error.'</div>';
				}
				?>
				</div>
				<?php } ?>
		
        	<div id="login">
               
			   	<?php echo form_open('secure/forgot_password') ?>
				<input type="hidden" value="submitted" name="submitted"/>
  				<div id="reg_customer">
                	<h1>Forgot Password</h1>
                  <input type="text" name="email" value="Email address" onfocus="if(this.value == 'Email address') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Email address';}" />
                    <input type="submit" value="Reset Password" name="<?php echo lang('form_submit');?>"/>
                </form>  
					<div class="clear"></div>
					
<div id="login_form_links">
			<a href="<?php echo site_url('secure/login'); ?>"><?php echo lang('return_to_login');?></a>
		</div>
				</div>     
				             

				              
                

        	</div><!-- End of static -->  
        </div><!-- End of static_wrap -->
	</div><!-- End of First Portion -->
    
    
    
		<div class="clear"></div>
     </div><!-- End of Container -->	
	 
</div> 
</div>

<!-- <div id="login_container_wrap">
		<div id="login_container">
		<?php echo form_open('secure/forgot_password') ?>
			<table>
				<tr>
					<td><?php echo lang('email');?></td>
					<td><input type="text" name="email" class="gc_login_input"/></td>
				</tr>
			</table>
			<div class="center">
					<input type="hidden" value="submitted" name="submitted"/>
					<input type="submit" value="Reset Password" name="<?php echo lang('form_submit');?>"/>
			</div>
		</form>
		
	</div>
</div> -->

<?php include('footer.php'); ?>
