<script type="text/javascript">
jQuery(document).ready(function() {	
	//disable the form elements under billing if the checkbox is checked
	if(jQuery('#different_address').is(':checked'))
	{
		toggle_billing_address_form(true);
	}
	
	//add the disabled class to to disabled fields
	jQuery('*:disabled').addClass('disabled');

	// automatically copy values when the checkbox is checked
	jQuery('.ship').change(function(){
		if(jQuery('#different_address').is(':checked'))
		{
			copy_shipping_address();
		}
	});	
	
	// populate zone menu with country selection
	jQuery('#ship_country_id').change(function(){
			populate_zone_menu('ship');
		});

	jQuery('#bill_country_id').change(function(){
			populate_zone_menu('bill');
		});	

});
// context is ship or bill
function populate_zone_menu(context, value)
{
	jQuery.post('<?php echo site_url('locations/get_zone_menu');?>',{id:jQuery('#'+context+'_country_id').val()}, function(data) {
		jQuery('#'+context+'_zone_id').html(data);
		
		//if the ship country is changed, and copy shipping address is checked, then reset the billing address to blank
		if(context == 'ship' && jQuery('#different_address').is(':checked'))
		{
			jQuery('#bill_zone_id').html(data).val(jQuery('#bill_zone_id option:first').val());
		}
	});
}
function toggle_billing_address_form(checked)
{
	if(!checked)
	{
		clear_billing_address();
		jQuery('.bill').attr('disabled', false);
		jQuery('.bill').removeClass('disabled');
	}
	else
	{
		copy_shipping_address();
		jQuery('.bill').attr('disabled', true);
		jQuery('.bill').addClass('disabled');
	}
}

function clear_billing_address()
{
	jQuery('.bill').val('');
}

function copy_shipping_address()
{
	//jQuery('#bill_company').val(jQuery('#ship_company').val());
	jQuery('#bill_firstname').val(jQuery('#ship_firstname').val());
	jQuery('#bill_lastname').val(jQuery('#ship_lastname').val());
	jQuery('#bill_address1').val(jQuery('#ship_address1').val());
	jQuery('#bill_address2').val(jQuery('#ship_address2').val());
	jQuery('#bill_city').val(jQuery('#ship_city').val());
	jQuery('#bill_zip').val(jQuery('#ship_zip').val());
	jQuery('#bill_phone').val(jQuery('#ship_phone').val());
	jQuery('#bill_email').val(jQuery('#ship_email').val());
	jQuery('#bill_country_id').val(jQuery('#ship_country_id').val());

	// repopulate and set zone field
	jQuery('#bill_zone_id').html(jQuery('#ship_zone_id').html());
	jQuery('#bill_zone_id').val(jQuery('#ship_zone_id').val());
}

function save_customer()
{
	jQuery('#save_customer_loader').show();
	// temporarily enable the billing fields (if disabled)
	if(jQuery('#different_address').is(':checked'))
	{
		jQuery('.bill').attr('disabled', false);
		jQuery('.bill').removeClass('disabled');
	}
	//send data to server
	form_data  = jQuery('#customer_info_form').serialize();
	
	jQuery.post('<?php echo site_url('checkout/save_customer') ?>', form_data, function(response)
	{
		if(typeof response != "object") // error
		{
			display_error('customer', '<?php echo lang('communication_error');?>');
			return;
		}
		
		if(response.status=='success')
		{
			//populate the information from ajax, so someone cannot use developer tools to edit the form after it's saved
			jQuery('#customer_info_fields').html(response.view);
			
			jQuery('input:button, button').button();			
			 // and update the summary to show proper tax information / discounts
			 update_summary();
		}
		else if(response.status=='error')
		{
			display_error('customer', response.error);
			jQuery('#save_customer_loader').hide();
		}
	}, 'json');
}
</script>

<?php /* Only show this javascript if the user is logged in */ ?>
<?php if($this->Customer_model->is_logged_in(false, false)) : ?>
<script type="text/javascript">
	
	var address_type = 'ship';
	jQuery(document).ready(function(){
		jQuery('.address_picker').click(function(){
			jQuery.colorbox({href:'#address_manager', inline:true, height:'400px'});
			address_type = jQuery(this).attr('rel');
		});
	});

	<?php
	$add_list = array();
	foreach($customer_addresses as $row) {
		// build a new array
		$add_list[$row['id']] = $row['field_data'];
	}
	$add_list = json_encode($add_list);
	echo "eval(addresses=$add_list);";
	?>
		
	function populate_address(address_id)
	{
		if(address_id=='') return;

		// update the visuals

		// - this is redundant, but it updates the visuals before the operation begins
		if(shipping_required && address_type=='ship')
		{
			jQuery('#shipping_loading').show();
			jQuery('#shipping_method_list').hide();
		}

		// - populate the fields
		jQuery.each(addresses[address_id], function(key, value){
			jQuery('#'+address_type+'_'+key).val(value);

			// repopulate the zone menu and set the right value if we change the country
			if(key=='zone_id')
			{
				zone_id = value;
			}
		});	
		
		// - save the address id
		jQuery('#'+address_type+'_address_id').val(address_id);

		// repopulate the zone list, set the right value, then copy all to billing
		jQuery.post('<?php echo site_url('locations/get_zone_menu');?>',{id:jQuery('#'+address_type+'_country_id').val()}, function(data) {
			// - uncheck the option box if they choose a billing address
			if(address_type=='bill')
			{
				jQuery('#different_address').attr('checked', false);
				jQuery('.bill').attr('disabled', false);
				jQuery('.bill').removeClass('disabled');
				jQuery('#bill_zone_id').html(data);
				jQuery('#bill_zone_id').val(zone_id);
			} 
			else 
			{
				// set the right zone values
				jQuery('#ship_zone_id').html(data);
				jQuery('#ship_zone_id').val(zone_id);

				if(jQuery('#different_address').is(':checked'))
				{
					// copy the rest of the fields
					copy_shipping_address();
				}	
			}
		});		
	}
	
</script>
<?php endif;?>

<?php
$countries = $this->Location_model->get_countries_menu();

if(!empty($customer['ship_address']['country_id']))
{
	$ship_zone_menu	= $this->Location_model->get_zones_menu($customer['ship_address']['country_id']);
}
else
{
	// if this is set, it means we've got a blank address. Set the state field to an empty initial value
	$ship_zone_menu = array(''=>'')+$this->Location_model->get_zones_menu(array_shift(array_keys($countries)));
}

if(!empty($customer['bill_address']['country_id']))
{
	$bill_zone_menu	= $this->Location_model->get_zones_menu($customer['bill_address']['country_id']);
}
else
{
	$bill_zone_menu = array(''=>'')+$this->Location_model->get_zones_menu(array_shift(array_keys($countries)));
}

//form elements

//$b_company	= array('id'=>'bill_company', 'class'=>'bill input', 'name'=>'bill_company', 'value'=> @$customer['bill_address']['company']);
$b_address1	= array('id'=>'bill_address1', 'class'=>'bill input bill_req', 'name'=>'bill_address1', 'value'=>@$customer['bill_address']['address1']);
$b_address2	= array('id'=>'bill_address2', 'class'=>'bill input', 'name'=>'bill_address2', 'value'=> @$customer['bill_address']['address2']);
$b_first	= array('id'=>'bill_firstname', 'class'=>'bill input bill_req', 'name'=>'bill_firstname', 'value'=> @$customer['bill_address']['firstname']);
$b_last		= array('id'=>'bill_lastname', 'class'=>'bill input bill_req', 'name'=>'bill_lastname', 'value'=> @$customer['bill_address']['lastname']);
$b_email	= array('id'=>'bill_email', 'class'=>'bill input bill_req', 'name'=>'bill_email', 'value'=>@$customer['bill_address']['email']);
$b_phone	= array('id'=>'bill_phone', 'class'=>'bill input bill_req', 'name'=>'bill_phone', 'value'=> @$customer['bill_address']['phone']);
$b_city		= array('id'=>'bill_city', 'class'=>'bill input bill_req', 'name'=>'bill_city', 'value'=>@$customer['bill_address']['city']);
$b_zip		= array('id'=>'bill_zip', 'maxlength'=>'10', 'class'=>'bill input bill_req', 'name'=>'bill_zip', 'value'=> @$customer['bill_address']['zip']);


//$s_company	= array('id'=>'ship_company', 'class'=>'ship input', 'name'=>'ship_company', 'value'=> @$customer['ship_address']['company']);
$s_address1	= array('id'=>'ship_address1', 'class'=>'ship input ship_req', 'name'=>'ship_address1', 'value'=>@$customer['ship_address']['address1']);
$s_address2	= array('id'=>'ship_address2', 'class'=>'ship input', 'name'=>'ship_address2', 'value'=> @$customer['ship_address']['address2']);
$s_first	= array('id'=>'ship_firstname', 'class'=>'ship input ship_req', 'name'=>'ship_firstname', 'value'=> @$customer['ship_address']['firstname']);
$s_last		= array('id'=>'ship_lastname', 'class'=>'ship input ship_req', 'name'=>'ship_lastname', 'value'=> @$customer['ship_address']['lastname']);
$s_email	= array('id'=>'ship_email', 'class'=>'ship input ship_req', 'name'=>'ship_email', 'value'=>@$customer['ship_address']['email']);
$s_phone	= array('id'=>'ship_phone', 'class'=>'ship input ship_req', 'name'=>'ship_phone', 'value'=> @$customer['ship_address']['phone']);
$s_city		= array('id'=>'ship_city', 'class'=>'ship input ship_req', 'name'=>'ship_city', 'value'=>@$customer['ship_address']['city']);
$s_zip		= array('id'=>'ship_zip', 'maxlength'=>'10', 'class'=>'ship input ship_req', 'name'=>'ship_zip', 'value'=> @$customer['ship_address']['zip']);

?>
<style>
.form_label {
    width: 115px;
}
</style>

	<div id="customer_error_box" class="error" style="display:none"></div>
	<form id="customer_info_form">
		<h2 class="title"><?php echo lang('customer_information');?></h2>
		<div id="my_information1">
		<div id="shipping_address">
			<div class="form_wrap">
				<h2 class="title"><?php echo lang('shipping_address');?></h2>
			</div>
			<!-- <div class="form_wrap" style="margin-left:280px;display:block">
				<?php if($this->Customer_model->is_logged_in(false, false)) : ?>
				<div>
					<input class="address_picker" type="button" value="<?php echo lang('choose_address');?>" rel="ship" />
				</div>
				<?php endif; ?>
			</div> -->

			<input type="hidden" name="ship_address_id" id="ship_address_id" />

			<!-- <div class="form_label"> <?php echo lang('address_company');?> *</div>
				<div class="form_input">
				<?php echo form_input($s_company);?>
			</div>	
			
			<div class="clear"></div> -->

			<div class="form_label"> <?php echo lang('address_firstname');?> *</div>
				<div class="form_input">
				<?php echo form_input($s_first);?>
			</div>	

			<div class="form_label"> <?php echo lang('address_lastname');?> *</div>
				<div class="form_input">
				<?php echo form_input($s_last);?>
			</div>

			<div class="clear"></div>

			<div class="form_label"> <?php echo lang('address_email');?> *</div>
				<div class="form_input">
				<?php echo form_input($s_email);?>
			</div>

			<div class="form_label"> <?php echo lang('address_phone');?> *</div>
				<div class="form_input">
				<?php echo form_input($s_phone);?>
			</div>

			<div class="clear"></div>
			
			<div class="form_label"> <?php echo lang('address');?> *</div>
				<div class="form_input">
				<?php echo form_input($s_address1).'<br/>'.form_input($s_address2);?>
			</div>

			<div class="clear"></div>

			<div class="form_label"> <?php echo lang('address_city');?> *</div>
				<div class="form_input">
				<?php echo form_input($s_city);?>
			</div>

			<div class="form_label"> <?php echo lang('address_postcode');?> *</div>
				<div class="form_input">
				<?php echo form_input($s_zip);?>
			</div>

			<div class="clear"></div>

			<div class="form_label"> <?php echo lang('address_country');?> *</div>
			<?php echo form_dropdown('ship_country_id',$countries, @$customer['ship_address']['country_id'], 'id="ship_country_id" class="ship input ship_req"');?>
			
			<div class="form_label"> <?php echo lang('address_state');?> *</div>
			<?php echo form_dropdown('ship_zone_id',$ship_zone_menu, @$customer['ship_address']['zone_id'], 'id="ship_zone_id" class="ship input ship_req"');?>
			
			<div class="clear"></div>
		
			<div class="form_label"></div>
			<input type="checkbox" id="different_address" name="ship_to_bill_address" value="yes" <?php echo set_checkbox('ship_to_bill_address', 'yes', @$customer['ship_to_bill_address']);?> onclick="toggle_billing_address_form(this.checked)"> <?php echo lang('use_address_for_billing');?>
			
			<div class="clear"></div>
			
			<p>&nbsp;</p>
			<p>&nbsp;</p>
		</div>
		<div id="billing_address" style="float: left;">
			<div class="form_wrap">
				<h2 class="title"><?php echo lang('billing_address')?></h2>
			</div>
			<!-- <div class="form_wrap" style="margin-left:280px;display:block">
				<?php if($this->Customer_model->is_logged_in(false, false)) : ?>
				<div>
					<input class="address_picker" type="button" value="<?php echo lang('choose_address');?>" rel="bill" />
				</div>
				<?php endif; ?>
			</div> -->

			<input type="hidden" name="bill_address_id" id="bill_address_id" />
				
			<!-- <div class="form_label"> <?php echo lang('address_company');?> *</div>
				<div class="form_input">
				<?php echo form_input($b_company);?>
			</div>	
			
			<div class="clear"></div> -->

			<div class="form_label"> <?php echo lang('address_firstname');?> *</div>
				<div class="form_input">
				<?php echo form_input($b_first);?>
			</div>	

			<div class="form_label"> <?php echo lang('address_lastname');?> *</div>
				<div class="form_input">
				<?php echo form_input($b_last);?>
			</div>

			<div class="clear"></div>

			<div class="form_label"> <?php echo lang('address_email');?> *</div>
				<div class="form_input">
				<?php echo form_input($b_email);?>
			</div>

			<div class="form_label"> <?php echo lang('address_phone');?> *</div>
				<div class="form_input">
				<?php echo form_input($b_phone);?>
			</div>

			<div class="clear"></div>
			
			<div class="form_label"> <?php echo lang('address');?> *</div>
				<div class="form_input">
				<?php echo form_input($b_address1).'<br/>'.form_input($b_address2);?>
			</div>

			<div class="clear"></div>

			<div class="form_label"> <?php echo lang('address_city');?> *</div>
				<div class="form_input">
				<?php echo form_input($b_city);?>
			</div>

			<div class="form_label"> <?php echo lang('address_postcode');?> *</div>
				<div class="form_input">
				<?php echo form_input($b_zip);?>
			</div>

			<div class="clear"></div>

			<div class="form_label"> <?php echo lang('address_country');?> *</div>
			<?php echo form_dropdown('bill_country_id',$countries, @$customer['bill_address']['country_id'], 'id="bill_country_id" class="bill input bill_req"');?>
				
			
			<div class="form_label"> <?php echo lang('address_state');?> *</div>
			<?php echo form_dropdown('bill_zone_id',$bill_zone_menu, @$customer['bill_address']['zone_id'], 'id="bill_zone_id" class="bill input bill_req"');?>
				
			<div class="clear"></div>
		
			<p>&nbsp;</p>
			<p>&nbsp;</p>
		</div>
		</div>
		<div class="clear"></div>
		<div id="my_information1">
		
		</div>
		
	</form>
	
	<table width="100%">
		<tr>
			<td align="center"><a href="#"  onclick="save_customer()"><img src="<?php echo base_url();?>images/submit.png" alt="<?php echo lang('form_continue');?>" /></a>
			<!-- <input type="button" value="<?php echo lang('form_continue');?>" onclick="save_customer()"/> --></td>
			<td><img id="save_customer_loader" alt="loading" src="<?php echo base_url('images/ajax-loader.gif');?>" style="display:none;"/></td>
		</tr>
	</table>
	


<?php if($this->Customer_model->is_logged_in(false, false)) : ?>
<div id="stored_addresses" style="display:none;">
	<div id="address_manager">
		<h3 style="text-align:center;"><?php echo lang('your_addresses');?></h3>
		<script type="text/javascript">
		$(document).ready(function(){
			$('#address_list .my_account_address:even').addClass('address_bg');
		});
		</script>
		<div id="address_list">
			
		<?php
		$c = 1;
		foreach($customer_addresses as $a):?>
			<div class="my_account_address" id="address_<?php echo $a['id'];?>">
				<div class="address_toolbar">
					<input type="button" class="choose_address" onclick="populate_address(<?php echo $a['id'];?>); jQuery.colorbox.close()" value="<?php echo lang('form_choose');?>" />
				</div>
				<?php
				$b	= $a['field_data'];
				echo nl2br(format_address($b));
				?>
			</div>
		<?php endforeach;?>
		</div>
	</div>
</div>
<?php endif;?>