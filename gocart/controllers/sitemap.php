<?php

class Sitemap extends CI_Controller {	
	
	function __construct()
	{		
		parent::__construct();
		$this->load->model('Page_model');
		$this->load->library('sitemaps');
		
	}
	
	function index()
	{
		$posts = $this->Page_model->get_pages();

		//echo "<pre>";
		//print_r($posts);
		//echo "</pre>";

        foreach($posts AS $post)
        {
            $item = array(
                "loc" => site_url($post->slug),
                // ISO 8601 format - date("c") requires PHP5
                //"lastmod" => date("c", strtotime($post->last_modified)),
                "changefreq" => "hourly",
                "priority" => "0.8"
            );
            
            $this->sitemaps->add_item($item);
        }
        
        // file name may change due to compression
        $file_name = $this->sitemaps->build("sitemap_blog.xml");

        $reponses = $this->sitemaps->ping(site_url($file_name));
        
        // Debug by printing out the requests and status code responses
         //print_r($reponses);

        redirect(site_url($file_name));
	}
}