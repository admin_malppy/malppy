<?php

class Orders extends Admin_Controller {	

	function __construct()
	{		
		parent::__construct();

		remove_ssl();
		$this->load->model('Order_model');
		$this->load->model('Search_model');
		$this->load->model('location_model');
		$this->load->model('Settings_model');
		$this->load->model('Product_model');
		$this->load->helper(array('formatting', 'utility'));
		$this->lang->load('order');
	}
	
	function index($sort_by='order_number',$sortorder='desc', $code=0, $page=0, $rows=50)
	{
		$this->load->helper('form');
		$this->load->helper('date');
		$data['message']	= $this->session->flashdata('message');
		$data['page_title']	= lang('orders');
		$data['code']		= $code;
		$term				= false;
		
		
		
		$post	= $this->input->post(null, false);
		if($post)
		{
			//if the term is in post, save it to the db and give me a reference
			$term	= json_encode($post);
			$data['code']	= $this->Search_model->record_term($term);
			
			//reset the term to an object for use
			$term	= (object)$post;
		}
		elseif ($code)
		{
			$term	= $this->Search_model->get_term($code);
			$term	= json_decode($term);
		} 

 		$data['term']	= $term;
 		$data['orders']	= $this->Order_model->get_orders_by_seller($term, $sort_by, $sortorder, $rows, $page);

		$data['total']	= $this->Order_model->get_orders_count($term);

		$this->load->library('pagination');
		
		$config['base_url'] = $this->config->item('admin_folder').'/orders/index/'.$sort_by.'/'.$sortorder.'/'.$code.'/';
		$config['total_rows'] = $data['total'];
		$config['per_page'] = $rows;
		
		$this->pagination->initialize($config);
	
		$data['sort_by']	= $sort_by;
		$data['sortorder']	= $sortorder;
		$data['pages']		= $this->pagination->create_links();
				
		$this->load->view($this->config->item('admin_folder').'/orders', $data);
	}

	function index2($sort_by='order_number',$sortorder='desc', $code=0, $page=0, $rows=50)
	{
		$this->load->helper('form');
		$this->load->helper('date');
		$data['message']	= $this->session->flashdata('message');
		$data['page_title']	= lang('orders');
		$data['code']		= $code;
		$term				= false;
		
		$post	= $this->input->post(null, false);
		if($post)
		{
			//if the term is in post, save it to the db and give me a reference
			$term	= json_encode($post);
			$data['code']	= $this->Search_model->record_term($term);
			
			//reset the term to an object for use
			$term	= (object)$post;
		}
		elseif ($code)
		{
			$term	= $this->Search_model->get_term($code);
			$term	= json_decode($term);
		} 

 		$data['term']	= $term;
 		$data['orders']	= $this->Order_model->get_orders($term, $sort_by, $sortorder, $rows, $page);
		$data['total']	= $this->Order_model->get_orders_count($term);

		$this->load->library('pagination');
		
		$config['base_url'] = $this->config->item('admin_folder').'/orders/index/'.$sort_by.'/'.$sortorder.'/'.$code.'/';
		$config['total_rows'] = $data['total'];
		$config['per_page'] = $rows;
		
		$this->pagination->initialize($config);
	
		$data['sort_by']	= $sort_by;
		$data['sortorder']	= $sortorder;
		$data['pages']		= $this->pagination->create_links();
				
		$this->load->view($this->config->item('admin_folder').'/orders', $data);
	}

	function items($id)
	{
		$this->load->helper(array('form', 'date'));
		$this->load->library('form_validation');

		$this->form_validation->set_rules('notes', 'lang:notes');
		$this->form_validation->set_rules('status', 'lang:status', 'required');
		if ($this->form_validation->run() == TRUE)
		{
			$save			= array();
			$save['id']		= $id;
			$save['notes']	= $this->input->post('notes');
			$save['status']	= $this->input->post('status');
			
			$data['message']	= lang('message_order_updated');
			
			$this->Order_model->save_order($save);
		}
		//get the order information, this way if something was posted before the new one gets queried here
		$data['page_title']	= "Process Order";
		$data['order']		= $this->Order_model->get_order($id);
		$data['orderid']	= $id;
		
		$currency = $this->Settings_model->get_settings('currency');
		$data['bnd_to_rmb']	= $currency['bnd_to_rmb'];
		
		$this->load->view($this->config->item('admin_folder').'/order_items', $data);
		
	}

	function group_items($id)
	{
		$this->load->helper(array('form', 'date'));
		$this->load->library('form_validation');

		$this->form_validation->set_rules('notes', 'lang:notes');
		$this->form_validation->set_rules('status', 'lang:status', 'required');
		if ($this->form_validation->run() == TRUE)
		{
			$save			= array();
			$save['id']		= $id;
			$save['notes']	= $this->input->post('notes');
			$save['status']	= $this->input->post('status');
			
			$data['message']	= lang('message_order_updated');
			
			$this->Order_model->save_order($save);
		}
		//get the order information, this way if something was posted before the new one gets queried here
		$data['page_title']	= "Process Order";
		$data['order']		= $this->Order_model->get_order($id);
		$data['orderid']	= $id;
		
		$currency = $this->Settings_model->get_settings('currency');
		$data['bnd_to_rmb']	= $currency['bnd_to_rmb'];
		
		$this->load->view($this->config->item('admin_folder').'/order_group_items', $data);
		
	}
	
	function export()
	{
		$this->load->model('customer_model');
		$this->load->helper('download_helper');
		$post	= $this->input->post(null, false);
		$term	= (object)$post;
		
		$data['orders']	= $this->Order_model->get_orders($term);		

		foreach($data['orders'] as &$o)
		{
			$o->items	= $this->Order_model->get_items($o->id);
		}

		force_download_content('orders.xml', $this->load->view($this->config->item('admin_folder').'/orders_xml', $data, true));
		
	}

	function inventory($id)
	{
		$this->load->helper('form');
		$this->load->model('order_model');
		$data['page_title']	= "Inventory";

		$currency = $this->Settings_model->get_settings('currency');
		$data['bnd_to_rmb']	= $currency['bnd_to_rmb'];
		
		$data[batches] = $this->Order_model->get_batches();
		$data[order_summary] = $this->Order_model->get_orders_summary();
		
		if ($id) {
			$data[batch_id] = $id;

			$data[batch] = $this->Order_model->get_batch($id);

			$data[batch_items] = $this->Order_model->get_batch_items($id);
			
			$data[batch_title] =$data[batch]->batch_number." (".date("d/m/Y h:i",strtotime($data[batch]->created_on)).")";
		}
			
		$this->load->view($this->config->item('admin_folder').'/order_inventory', $data);
		
	}

	
	function export_popup($id) //Same as view_inventory but in pop up window can delete either unused one later
	{
		$this->load->helper('form');
		$this->load->model('order_model');
		$data['page_title']	= "Inventory";

		$data[batches] = $this->Order_model->get_batches();
		$data[order_summary] = $this->Order_model->get_orders_summary();
		
		if ($id) {
			$data[batch_id] = $id;

			$data[batch] = $this->Order_model->get_batch($id);

			$data[batch_items] = $this->Order_model->get_batch_items($id);
			
			$data[batch_title] =$data[batch]->batch_number." (".date("d/m/Y h:i",strtotime($data[batch]->created_on)).")";
		}
		
		$this->load->view($this->config->item('admin_folder').'/order_inventory_export_popup',$data);
	
	}
	
	function pending_grouping()
	{
		$this->load->helper('form');
		$this->load->model('order_model');
		$data['page_title']	= "Pending Grouping";

		$data['boxes'] = $this->Order_model->get_boxes();

		$all_orders = $this->Order_model->get_orders_by_box();

		foreach($all_orders as $od) {
			$pds = $this->Order_model->get_items($od->id);

			foreach($pds as $pd) {
				$bstat[$od->id][$pd['id']]['bought'] = $pd['bought'];
			}
		}

		foreach($bstat as $oid => $bs) {
			$this_status = $this->Order_model->get_order($oid)->status;
			if ($this_status == "Closed") break;

			foreach($bs as $b) {
				if (in_array(0, $b)) {
					$s['id']		= $oid;
    				$s['status']	= "Pending";  
										
    				$this->Order_model->save_order($s);
					break;
				}
				$s['id']		= $oid;
    			$s['status']	= "Pending Delivery"; 
				
    			$this->Order_model->save_order($s);
			}	
		}

		$data['orders']	= $this->Order_model->get_orders_by_box('','','','','','Pending Delivery');
		
		$this->load->view($this->config->item('admin_folder').'/order_pending_grouping', $data);
	}

	function group_order($id, $orderid)
	{
		$save['box_id'] = $id;
		$save['order_id'] = $orderid;

		$this->Order_model->save_box_item($save);

		redirect($this->config->item('admin_folder').'/orders/pending_grouping');
	}

	function box($id)
	{
		$this->load->helper('form');
		$this->load->model('order_model');
		$data['page_title']	= "Box Management";

		$search = $this->input->post('term');

		$data[boxes] = $this->Order_model->get_boxes($search);
		$data[box_id] = $id;

		if ($id) {
			$data[box] = $this->Order_model->get_box($id);
			$data['orders']	= $this->Order_model->get_orders_by_box($id);

			$data[shipment] = $this->Order_model->get_shipment($data[box]->shipment_id);
		}

		$this->load->view($this->config->item('admin_folder').'/order_box', $data);
	}

	function box_form()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('order_model');
		$data['page_title']	= "Add Box";

		$this->load->view($this->config->item('admin_folder').'/order_box_form', $data);
	}

	function box_content($id)
	{
		$this->load->helper(array('form', 'date'));
		
		//get the order information, this way if something was posted before the new one gets queried here
		$data['page_title']	= "Orders";

		if ($id) {
			$data['orders']	= $this->Order_model->get_shipment_orders($id);
			
			foreach($data['orders']	as $no=>$order)
			{
				$order_number = $this->Order_model->get_order($order->order_id)->order_number;
				$data['orders'][$no]->order_number = $order_number;
				$detail = $this->Order_model->get_items($order->order_id);
				$weight = $detail[0]['weight'];
				
				$data['orders'][$no]->weight = $weight;
			}
		} else {
			$data['orders']	= $this->Order_model->get_new_shipping_orders();

			foreach($data['orders'] as $no=>$order)
			{
				$detail = $this->Order_model->get_items($order->id);
				$weight = $detail[0]['weight'];
				
				$data['orders'][$no]->weight = $weight;
			}
		}

		$currency = $this->Settings_model->get_settings('currency');
		$data['bnd_to_rmb']	= $currency['bnd_to_rmb'];
		
		$this->load->view($this->config->item('admin_folder').'/box_order', $data);
		
	}

	function new_box()
	{
		$this->Order_model->add_box();
		redirect($this->config->item('admin_folder').'/orders/pending_grouping');
	}
	
	function view($id)
	{
		$this->load->helper(array('form', 'date'));
		$this->load->library('form_validation');
		//$this->load->model('Gift_card_model');
			
		$this->form_validation->set_rules('notes', 'lang:notes');
		$this->form_validation->set_rules('status', 'lang:status', 'required');
		if ($this->form_validation->run() == TRUE)
		{
			$save			= array();
			$save['id']		= $id;
			$save['notes']	= $this->input->post('notes');
			$save['status']	= $this->input->post('status');
			
			$data['message']	= lang('message_order_updated');
			
			$this->Order_model->save_order($save);
		}
		//get the order information, this way if something was posted before the new one gets queried here
		$data['page_title']	= lang('view_order');
		$data['order']		= $this->Order_model->get_order($id);
		
		// we need to see if any items are gift cards, so we can generate an activation link
		/*foreach($data['order']->contents as $orderkey=>$product)
		{
			if(isset($product['is_gc']))
			{
				if($this->Gift_card_model->is_active($product['code']))
				{
					$data['order']->contents[$orderkey]['gc_status'] = '[ '.lang('giftcard_is_active').' ]';
				} else {
					$data['order']->contents[$orderkey]['gc_status'] = ' [ <a href="'. base_url() . $this->config->item('admin_folder').'/giftcards/activate/'. $product['code'].'">'.lang('activate').'</a> ]';
				}
			}
		}*/
		
		$this->load->view($this->config->item('admin_folder').'/order', $data);
		
	}
	
	function taobao($id)
	{
		$this->load->helper(array('form', 'date'));
		$this->load->library('form_validation');

		$this->form_validation->set_rules('notes', 'lang:notes');
		$this->form_validation->set_rules('status', 'lang:status', 'required');
		if ($this->form_validation->run() == TRUE)
		{
			$save			= array();
			$save['id']		= $id;
			$save['notes']	= $this->input->post('notes');
			$save['status']	= $this->input->post('status');
			
			$data['message']	= lang('message_order_updated');
			
			$this->Order_model->save_order($save);
		}
		//get the order information, this way if something was posted before the new one gets queried here
		$data['page_title']	= "Process Order";
		$data['order']		= $this->Order_model->get_order($id);
		$data['orderid']	= $id;
		
		$currency = $this->Settings_model->get_settings('currency');
		$data['bnd_to_rmb']	= $currency['bnd_to_rmb'];
		
		$this->load->view($this->config->item('admin_folder').'/order_taobao', $data);
		
	}

	function taobao_seller($str)
	{
		$this->load->helper(array('form', 'date'));
		$this->load->library('form_validation');

		$data['page_title']	= "Process Order";

		$product_ids = explode("_",$str);

		foreach ($product_ids AS $product_id) {
			$data['products'][] = $this->Product_model->get_product($product_id);
		}

		$currency = $this->Settings_model->get_settings('currency');
		$data['bnd_to_rmb']	= $currency['bnd_to_rmb'];
			
		$this->load->view($this->config->item('admin_folder').'/order_taobao_seller', $data);
	}

	function paid($id)
	{
		$save['id']		= $id;
		$save['bought']	= $this->input->post('paid_status');
		
		$this->Order_model->update_paid($save);
	}

	function deliver($id)
	{
		$save['id']		= $id;
		$save['arrive']	= $this->input->post('deliver_status');
		
		$this->Order_model->update_paid($save);
	}

	function delivery($id)
	{
		$this->load->helper(array('form', 'date'));
		$this->load->library('form_validation');
		//$this->load->model('Gift_card_model');
		
		$this->form_validation->set_rules('notes', 'lang:notes');
		$this->form_validation->set_rules('status', 'lang:status', 'required');
		if ($this->form_validation->run() == TRUE)
		{
			$save			= array();
			$save['id']		= $id;
			$save['notes']	= $this->input->post('notes');
			$save['status']	= $this->input->post('status');
			
			$data['message']	= lang('message_order_updated');
			
			$this->Order_model->save_order($save);
		}
		//get the order information, this way if something was posted before the new one gets queried here
		$data['page_title']	= "Process Order";
		$data['order']		= $this->Order_model->get_order($id);
		
		$currency = $this->Settings_model->get_settings('currency');
		$data['bnd_to_rmb']	= $currency['bnd_to_rmb'];
		
		$this->load->view($this->config->item('admin_folder').'/order_delivery', $data);
		
	}

	function shipping($id)
	{
		$this->load->helper(array('form', 'date'));
		
		//get the order information, this way if something was posted before the new one gets queried here
		$data['page_title']	= "Orders";

		if ($id) {
			$data['orders']	= $this->Order_model->get_shipment_orders($id);
			
			foreach($data['orders']	as $no=>$order)
			{
				$order_number = $this->Order_model->get_order($order->order_id)->order_number;
				$data['orders'][$no]->order_number = $order_number;
				$detail = $this->Order_model->get_items($order->order_id);
				$weight = $detail[0]['weight'];
				
				$data['orders'][$no]->weight = $weight;
			}
		} else {
			$data['orders']	= $this->Order_model->get_new_shipping_orders();

			foreach($data['orders'] as $no=>$order)
			{
				$detail = $this->Order_model->get_items($order->id);
				$weight = $detail[0]['weight'];
				
				$data['orders'][$no]->weight = $weight;
			}
		}

		$currency = $this->Settings_model->get_settings('currency');
		$data['bnd_to_rmb']	= $currency['bnd_to_rmb'];
		
		$this->load->view($this->config->item('admin_folder').'/shipping_order', $data);
		
	}

	function pack()
	{
		$orders	= $this->input->post('action');
	
		if($orders)
		{
			foreach($orders as $id=>$order)
	   		{
				$save['product_id']	= $order['product_id'];
				$save['id']	= $order['item_id'];
				$save['pack']	= $order['pack'];

	   			$this->Order_model->update_product_pack($save);


				$save2['id']	= $order['order_id'];
				$save2['is_packed']	= $this->input->post('pack_status');

				$this->Order_model->update_order_pack($save2);
				
	   		}
			$this->session->set_flashdata('message', "");
		}
		
		redirect($this->config->item('admin_folder').'/orders/pending_grouping');	
	}

	
	
	function packing_slip($order_id)
	{
		$this->load->helper('date');
		$data['order']		= $this->Order_model->get_order($order_id);
		
		$this->load->view($this->config->item('admin_folder').'/packing_slip.php', $data);
	}

	function invoice_slip($box_id, $view=false)
	{
		$this->load->helper('date');
		$this->load->helper('form');
		
		//$data['order']		= $this->Order_model->get_order($order_id);
		$data[box] = $this->Order_model->get_box($box_id);
		$data['orders']	= $this->Order_model->get_orders_by_box($box_id);

		$data['shipping_number'] = $this->input->post('shipping_number');
		$data['shipping_remark'] = $this->input->post('shipping_remark');

		if (!$data['shipping_number']) {
			$shipment = $this->Order_model->get_shipment_by_box($box_id);
			$data['shipping_number'] = $shipment->shipment_number;
		}


		$data['id']	= $box_id;

		if ($view) {
			$data['view'] = true;
		}
		
		$this->load->view($this->config->item('admin_folder').'/invoice_slip.php', $data);
	}
	
	function edit_status()
    {
    	$this->auth->is_logged_in();
    	$order['id']		= $this->input->post('id');
    	$order['status']	= $this->input->post('status');
    	
    	$this->Order_model->save_order($order);
    	
    	echo url_title($order['status']);
    }

	function edit_remark()
    {
    	$order['id']		= $this->input->post('id');
    	$order['remark']	= $this->input->post('remark');
    	
    	$this->Order_model->save_order($order);
    	
    	echo url_title($order['remark']);
    }
	
	function pay_seller($num_iid)
    {
		if ($num_iid) {	
			$paramArr = array(
					'method' => 'taobao.taobaoke.items.detail.get',   //API name
			     'timestamp' => date('Y-m-d H:i:s'),			
				    'format' => 'xml',  //Return format, this demo supports only XML
		    	   'app_key' => $this->config->item('taobao_appKey'),  //Appkey			
			    		 'v' => '2.0',   //API version number		   
				'sign_method'=> 'md5', //Signature method
					'fields' => 'iid,detail_url,num_iid,title,nick,type,cid,seller_cids,props,input_pids,input_str,desc,pic_url,num,valid_thru,list_time,delist_time,stuff_status,location,price,post_fee,express_fee,ems_fee,has_discount,freight_payer,has_invoice,has_warranty,has_showcase,modified,increment,auto_repost,approve,status,postage_id,product_id,auction_point,property_alias,item_imgs,prop_imgs,skus,outer_id,is_virtual,is_taobao,is_ex,videos,is_3D,score,volume,one_station,click_url,shop_click_url,seller_credit_score,approve_status', //Returns the field
			      'num_iids' => $num_iid, //num_iid
				      'nick' => $this->config->item('taobao_userNick'), //Promoter Nick
			);
			
			//Generating signatures
			$sign = $this->createSign($paramArr,$this->config->item('taobao_appSecret'));
			
			//Organizational parameters
			$strParam = $this->createStrParam($paramArr);
			$strParam .= 'sign='.$sign;
			
			//Construct Url
			$urls = $this->config->item('taobao_url').$strParam;
			
			//Connection timeout auto retry
			$cnt=0;	
			while($cnt < 3 && ($result=$this->vita_get_url_content($urls))===FALSE) $cnt++;
			
			//Parsing Xml data
			$result = $this->getXmlData($result);
			
			//Gets the error message
			//$sub_msg = $result['sub_msg'];
			
			//Return result
			$taobaokeItemdetail = $result['taobaoke_item_details']['taobaoke_item_detail']['item'];
			$taobaokeItem = $result['taobaoke_item_details']['taobaoke_item_detail'];
			
			$buy_link = mysql_real_escape_string(trim($taobaokeItem['click_url']));

			return $buy_link;
			
		}
		exit;
    	//$this->auth->is_logged_in();
    	//$order['id']		= $id;
    	//$order['status']	= "Pending Delivery";
    	
    	//$this->Order_model->save_order($order);
    	//redirect($this->config->item('admin_folder').'/orders/taobao/'.$id);	
    	//echo url_title($order['status']);
    }
	
    function send_notification($order_id='')
    {
    	
    	$send = $this->input->post('send');
    	if(!empty($send))
    	{
    		// send the message
    		$this->load->library('email');
			
			$config['mailtype'] = 'html';
			
			$this->email->initialize($config);
	
			$this->email->from($this->config->item('email'), $this->config->item('company_name'));
			$this->email->to($this->input->post('recipient'));
			
			$this->email->subject($this->input->post('subject'));
			$this->email->message(html_entity_decode($this->input->post('content')));
			
			$this->email->send();
			
			$this->load->view($this->config->item('admin_folder').'/iframe/order_notification.php', array('finished'=>1));
			
			return;
    	}
    	
    	
    	$this->load->model('Messages_model');
    	
    	// get the order details
    	$data['order'] = $this->Order_model->get_order($order_id);
    	
    	// get the list of canned messages (order)
    	$data['msg_templates'] = $this->Messages_model->get_list('order');
    	
    	// replace template variables
    	foreach($data['msg_templates'] as &$msg)
    	{
 			// fix html
 			$msg['content'] = str_replace("\n", '', html_entity_decode($msg['content']));
 			
 			// {order_number}
 			$msg['subject'] = str_replace('{order_number}', $data['order']->order_number, $msg['subject']);
			$msg['content'] = str_replace('{order_number}', $data['order']->order_number, $msg['content']);
    		
    		// {url}
			$msg['subject'] = str_replace('{url}', $this->config->item('base_url'), $msg['subject']);
			$msg['content'] = str_replace('{url}', $this->config->item('base_url'), $msg['content']);
			
			// {site_name}
			$msg['subject'] = str_replace('{site_name}', $this->config->item('company_name'), $msg['subject']);
			$msg['content'] = str_replace('{site_name}', $this->config->item('company_name'), $msg['content']);
			
    	}
    	
    	$this->load->view($this->config->item('admin_folder').'/iframe/order_notification.php', $data);
 
    }

	function add_remark($item_id='',$action=false)
    {
    	// get the order details
		$data['item_id'] = $item_id;
    	$data['order'] = $this->Order_model->get_order($order_id);
		$data[edit_mode] = $action;
    	

		if($this->input->post('send'))
		{

			$save[id] = $item_id;
			$save[new_supplier] = $this->input->post('new_supplier');
			$save[new_supplier_price] = $this->input->post('new_price');
			$save[bought_on] = $this->input->post('bought_on');
			$data[edit_mode] = false;

			$this->Order_model->update_paid($save);
			
			$data[new_supplier] = $save[new_supplier];
			$data[new_price] = $save[new_supplier_price];
			$data[bought_on] = $save[bought_on];
		}

		$item = $this->Order_model->get_item($item_id);
		$data[new_supplier] = $item->new_supplier;
		$data[new_price] 	= $item->new_supplier_price;
		$data[bought_on] 	= $item->bought_on;

		if ($action=="close") {
			$data[finished] = true;
		}
		
		$this->load->view($this->config->item('admin_folder').'/iframe/order_remark.php', $data);
 
    }
    
	function buy_remark($item_id='',$type,$action=false)
    {
    	// get the order details
		$data['item_id'] = $item_id;
    	$data['order'] = $this->Order_model->get_order($order_id);
		$data[edit_mode] = $action;
    	$data['supplier_type'] = $type;

		if($this->input->post('send'))
		{

			$save[id] = $item_id;
			$save[new_supplier] = $this->input->post('new_supplier');
			$save[new_supplier_price] = $this->input->post('new_price');
			$save[bought_on] = $this->input->post('bought_on');
			$data[edit_mode] = false;

			$this->Order_model->update_paid($save);
			
			$order[id] = $item_id;
			$order[bought] =	1;
			$order[bought_on] = date('Y-m-d H:i:s');
			$order[supplier_type] = $type;
			$this->Order_model->save_purchase($order);
			
			$data[new_supplier] = $save[new_supplier];
			$data[new_price] = $save[new_supplier_price];
			$data[bought_on] = $save[bought_on];
		}

		$item = $this->Order_model->get_item($item_id);
		$data[new_supplier] = $item->new_supplier;
		$data[new_price] 	= $item->new_supplier_price;
		$data[bought_on] 	= $item->bought_on;

		if ($action=="close") {
			$data[finished] = true;
		}
		
		$this->load->view($this->config->item('admin_folder').'/iframe/order_remark.php', $data);
 
    }

    function view_export ($item_id='',$action=false){
    	
    	$this->load->view($this->config->item('admin_folder').'/iframe/order_remark.php');
    }
    
	function purchase_status()
	{
		$order['id'] = $this->input->post('itemid');
		$order['bought'] =	1;
		$order['bought_on'] = date('Y-m-d H:i:s');
		$order['supplier_type'] = $this->input->post('supplier_type');
    	echo $this->Order_model->save_purchase($order);
	}

	function receive_status()
	{
		$order['id'] = $this->input->post('itemid');
		$order['arrive'] =	1;
		$batch_id = $this->input->post('batch_id');
		$arrive_on = $this->input->post('receive_on');
		list($yy,$mm,$dd) = explode("-",$arrive_on);
		$order['arrive_on'] = $yy."-".$mm."-".$dd." 00:00:00";
		
    	echo $this->Order_model->save_arrive($order,$batch_id);
    	//echo $this->Order_model->check_export_batch_completion($batch_id);
	}

	function save_bt_date()
	{
		$order['id'] = $this->input->post('orderid');
		$bt_on = $this->input->post('bt_on');
		list($mm,$dd,$yy) = explode("-",$bt_on);
		$order['bt_on'] = $yy."-".$mm."-".$dd;
		
    	echo $this->Order_model->update_order_pack($order);
	}

	function save_do_date()
	{
		$order['id'] = $this->input->post('orderid');
		$do_on = $this->input->post('do_on');
		list($mm,$dd,$yy) = explode("-",$do_on);
		$order['do_on'] = $yy."-".$mm."-".$dd;
		
    	echo $this->Order_model->update_order_pack($order);
	}

	function bulk_export()
    {
    	$orders	= $this->input->post('order');
    	
		if($orders)
		{
			// generate new batch number
			$batchid = $this->Order_model->save_batch();

			foreach($orders as $order_id)
	   		{
				$save['id'] = $order_id;
				$save['batch_id'] = $batchid;
	   			$this->Order_model->save_order($save);				
	   		}
			$this->session->set_flashdata('message', "Orders exported");
		}
		else
		{
			$this->session->set_flashdata('error', "No orders selected");
		}
   		//redirect as to change the url
		redirect($this->config->item('admin_folder').'/orders');	
    }
	
	function bulk_delete()
    {
    	$orders	= $this->input->post('order');
    	
		if($orders)
		{
			foreach($orders as $order)
	   		{
	   			$this->Order_model->delete($order);
	   		}
			$this->session->set_flashdata('message', lang('message_orders_deleted'));
		}
		else
		{
			$this->session->set_flashdata('error', lang('error_no_orders_selected'));
		}
   		//redirect as to change the url
		redirect($this->config->item('admin_folder').'/orders');	
    }

	function bulk_seller_save()
	{
		$orders	= $this->input->post('action');
	
		if($orders)
		{
			foreach($orders as $id=>$order)
	   		{
				$save['product_id']	= $id;
				$save['bought']	= $order['paid'];

	   			$this->Order_model->update_product_paid($save);

				
	   		}
			$this->session->set_flashdata('message', "");
		}
		else
		{
			$this->session->set_flashdata('error', "");
		}
		
		redirect($this->config->item('admin_folder').'/orders');
	}

	function bulk_group()
    {
    	$orders	= $this->input->post('order');
    	
		if($orders)
		{
			foreach($orders as $order_id=>$box_id)
	   		{
				if (!$box_id) continue;
				//print_r($save);
				//group_order($id, $orderid);
	   			//$this->Order_model->save_order($save);	

				$save['box_id'] = $box_id;
				$save['order_id'] = $order_id;

				$this->Order_model->save_box_item($save);			
	   		}
			$this->session->set_flashdata('message', "Orders grouping done.");
		}
		else
		{
			$this->session->set_flashdata('error', "No orders selected");
		}
   		//redirect as to change the url
		redirect($this->config->item('admin_folder').'/orders/pending_grouping');	
    }

	function status()
	{
		//check to see if shipping and payment modules are installed
		$data['payment_module_installed']	= (bool)count($this->Settings_model->get_settings('payment_modules'));
		$data['shipping_module_installed']	= (bool)count($this->Settings_model->get_settings('shipping_modules'));
		
		$data['page_title']	=  lang('dashboard');
		
		// get 5 latest orders
		$data['orders']	= $this->Order_model->get_orders(false, '' , 'DESC', 5);

		// get 5 latest customers
		$data['customers'] = $this->Customer_model->get_customers(5);
				
		
		$this->load->view($this->config->item('admin_folder').'/mini_status', $data);
	}

	function Newiconv($_input_charset, $_output_charset, $input) {
	    $output = "";
	    if (!isset($_output_charset))
	        $_output_charset = $this->parameter['_input_charset '];
	    if ($_input_charset == $_output_charset || $input == null) {
	        $output = $input;
	    } elseif (function_exists("m\x62_\x63\x6fn\x76\145\x72\164_\145\x6e\x63\x6f\x64\x69\x6e\147")) {
	        $output = mb_convert_encoding($input, $_output_charset, $_input_charset);
	    } elseif (function_exists("\x69\x63o\156\x76")) {
	        $output = iconv($_input_charset, $_output_charset, $input);
	    } else
	        //error
	    return $output;
	}
	
	//Gets the data compatible with filegetcontents and curl
	function vita_get_url_content($url) {
		if(!function_exists('file_get_contents')) {
			$file_contents = file_get_contents($url);
		} else {
		$ch = curl_init();
		$timeout = 5; 
		curl_setopt ($ch, CURLOPT_URL, $url);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		$file_contents = curl_exec($ch);
		curl_close($ch);
		}
		
	return $file_contents;
	}
	
	//Sign function 
	function createSign ($paramArr,$appSecret) { 
	    $sign = $appSecret; 
	    ksort($paramArr); 
	    foreach ($paramArr as $key => $val) { 
	       if ($key !='' && $val !='') { 
	           $sign .= $key.$val; 
	       } 
	    } 
	    $sign = strtoupper(md5($sign.$appSecret));
	    return $sign; 
	}

	//Group functions 
	function createStrParam ($paramArr) { 
	    $strParam = ''; 
	    foreach ($paramArr as $key => $val) { 
	       if ($key != '' && $val !='') { 
	           $strParam .= $key.'='.urlencode($val).'&'; 
	       } 
	    } 
	    return $strParam; 
	} 

	//Parse XML functions
	function getXmlData ($strXml) {
		$pos = strpos($strXml, 'xml');
		if ($pos) {
			$xmlCode=simplexml_load_string($strXml,'SimpleXMLElement', LIBXML_NOCDATA);
			$arrayCode=$this->get_object_vars_final($xmlCode);
			return $arrayCode ;
		} else {
			return '';
		}
	}
	
	function get_object_vars_final($obj){
		if(is_object($obj)){
			$obj=get_object_vars($obj);
		}
		if(is_array($obj)){
			foreach ($obj as $key=>$value){
				$obj[$key]=$this->get_object_vars_final($value);
			}
		}
		return $obj;
	}
}