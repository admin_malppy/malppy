<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// GoCart Theme
$config['theme']			= 'malppy';

// SSL support
$config['ssl_support']		= false;

// Business information
$config['company_name']		= 'Malppy';
$config['address1']			= '';
$config['address2']			= '';
$config['country']			= ''; // use proper country codes only
$config['city']				= ''; 
$config['state']			= '';
$config['zip']				= '';
$config['email']			= 'cweikiat@gmail.com';

// Store currency
$config['currency']			= 'BND';  // USD, EUR, etc
$config['currency_symbol']  = 'BND ';

// Shipping config units
$config['weight_unit']	    = 'KG'; // LB, KG, etc
$config['dimension_unit']   = 'CM'; // FT, CM, etc

// site logo path (for packing slip)
$config['site_logo']		= '/images/logo.png';

//change the name of the admin controller folder 
$config['admin_folder']		= 'admin';

//file upload size limit
$config['size_limit']		= intval(ini_get('upload_max_filesize'))*1024;

//are new registrations automatically approved (true/false)
$config['new_customer_status']	= true;

//do we require customers to log in 
$config['require_login']		= true;

//default order status
$config['order_status']			= 'Pending';

// default Status for non-shippable orders (downloads)
$config['nonship_status'] = 'Delivered';

$config['order_statuses']	= array(
	'Pending'  				=> 'Pending',
	//'Processing'    		=> 'Processing',
	'Pending Delivery'    	=> 'Pending Delivery',
	'QC'					=> 'QC',
	'Dispatched'			=> 'Dispatched',
	//'Shipped'				=> 'Shipped',
	//'On Hold'				=> 'On Hold',
	'Cancelled'				=> 'Cancelled',
	'Delivered'				=> 'Delivered'
);

// enable inventory control ?
$config['inventory_enabled']	= true;

// allow customers to purchase inventory flagged as out of stock?
$config['allow_os_purchase'] 	= true;

//do we tax according to shipping or billing address (acceptable strings are 'ship' or 'bill')
$config['tax_address']		= 'ship';

//do we tax the cost of shipping?
$config['tax_shipping']		= false;

//Taobao config
//$config['taobao_url']		= 'http://gw.api.taobao.com/router/rest?';  //Official environment submit URL
//$config['taobao_appKey']	= '12043535';
//$config['taobao_appSecret']	= '97655419cf98daa6d34b75bc7660401b';
//$config['taobao_userNick']	= 'nancy_tb88';

//$config['taobao_url']		= 'http://gw.api.taobao.com/router/rest?';  //Official environment submit URL
//$config['taobao_appKey']	= '21196018';
//$config['taobao_appSecret']	= '01180c81786bc4c625e622603855eb23';
//$config['taobao_userNick']	= 'nancy_tb88';

//$config['taobao_url']		= 'http://gw.api.taobao.com/router/rest?';  //Official environment submit URL
//$config['taobao_appKey']	= '21261320';
//$config['taobao_appSecret']	= '0b14f1c1b8ca65d95f2ed87d53315e61';
//$config['taobao_userNick']	= 'nancy_tb88';

$config['taobao_url']		= 'http://gw.api.taobao.com/router/rest?';  //Official environment submit URL
$config['taobao_appKey']	= '21064466';
$config['taobao_appSecret']	= 'c17ec2767f4273219601799105ad5326';
$config['taobao_userNick']	= 'nancy_tb88';


//$config['taobao_url']		= 'http://gw.api.tbsandbox.com/router/rest?';  //Official environment submit URL
//$config['taobao_appKey']	= '1021064466';
//$config['taobao_appSecret']	= 'sandbox67f4273219601799105ad5326';
//$config['taobao_userNick']	= 'sandbox_malppy';

$config['Facebook_APPID']	= '124958007662318';
$config['Facebook_APPSecret']	= '5064d62ba498aa46aedfd3c4e0695013';

$config['Twitter_Consumer_Key']	= '5vVMWCQ87qRHArHKkkcMmg';
$config['Twitter_Consumer_Secret']	= 'YsfPJYJn0Eulvkhu7NN95u2GdEgEOylc1eFhNsb6GA';