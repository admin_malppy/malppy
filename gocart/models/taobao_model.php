<?php
Class Taobao_model extends CI_Model
{
		
	function __construct()
	{
		parent::__construct();
	}
	
	function get_products($search=false, $sort_by='createdate', $sort_order='DESC', $limit=0, $offset=0)
	{
		//sort by alphabetically by default
		//$this->db->order_by('createdate', 'DESC');

		if ($search)
		{
			if(!empty($search->term))
			{
				//support multiple words
				$term = explode(' ', $search->term);

				foreach($term as $t)
				{
					$not		= '';
					$operator	= 'OR';
					if(substr($t,0,1) == '-')
					{
						$not		= 'NOT ';
						$operator	= 'AND';
						//trim the - sign off
						$t		= substr($t,1,strlen($t));
					}

					$like	= '';
					$like	.= "( `order_number` ".$not."LIKE '%".$t."%' " ;
					$like	.= $operator." `bill_firstname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `bill_lastname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `ship_firstname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `ship_lastname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `status` ".$not."LIKE '%".$t."%' ";
					$like	.= $operator." `notes` ".$not."LIKE '%".$t."%' )";

					$this->db->where($like);
				}	
			}
		}
		
		if($limit>0)
		{
			$this->db->limit($limit, $offset);
		}
		if(!empty($sort_by))
		{
			$this->db->order_by($sort_by, $sort_order);
		}
		
		$this->db->where("is_saved = 0");

		$result	= $this->db->get('taobao');
		
		$return = $result->result();
		
		return $return;

	}

	function get_products_count($search=false)
	{			
		if ($search)
		{
			if(!empty($search->term))
			{
				//support multiple words
				$term = explode(' ', $search->term);

				foreach($term as $t)
				{
					$not		= '';
					$operator	= 'OR';
					if(substr($t,0,1) == '-')
					{
						$not		= 'NOT ';
						$operator	= 'AND';
						//trim the - sign off
						$t		= substr($t,1,strlen($t));
					}

					$like	= '';
					$like	.= "( `order_number` ".$not."LIKE '%".$t."%' " ;
					$like	.= $operator." `bill_firstname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `bill_lastname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `ship_firstname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `ship_lastname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `status` ".$not."LIKE '%".$t."%' ";
					$like	.= $operator." `notes` ".$not."LIKE '%".$t."%' )";

					$this->db->where($like);
				}	
			}			
		}

		$this->db->where("is_saved = 0");
		
		return $this->db->count_all_results('taobao');
	}

	function get_saved_list($search=false, $sort_by='createdate', $sort_order='DESC', $limit=0, $offset=0)
	{
		//sort by alphabetically by default
		//$this->db->order_by('createdate', 'DESC');

		if ($search)
		{
			if(!empty($search->term))
			{
				//support multiple words
				$term = explode(' ', $search->term);

				foreach($term as $t)
				{
					$not		= '';
					$operator	= 'OR';
					if(substr($t,0,1) == '-')
					{
						$not		= 'NOT ';
						$operator	= 'AND';
						//trim the - sign off
						$t		= substr($t,1,strlen($t));
					}

					$like	= '';
					$like	.= "( `order_number` ".$not."LIKE '%".$t."%' " ;
					$like	.= $operator." `bill_firstname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `bill_lastname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `ship_firstname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `ship_lastname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `status` ".$not."LIKE '%".$t."%' ";
					$like	.= $operator." `notes` ".$not."LIKE '%".$t."%' )";

					$this->db->where($like);
				}	
			}
		}
		
		if($limit>0)
		{
			$this->db->limit($limit, $offset);
		}
		if(!empty($sort_by))
		{
			$this->db->order_by($sort_by, $sort_order);
		}

		$this->db->where("is_saved = 1");

		$result	= $this->db->get('taobao');
		
		$return = $result->result();
		
		return $return;

	}

	function get_saved_list_count($search=false)
	{			
		if ($search)
		{
			if(!empty($search->term))
			{
				//support multiple words
				$term = explode(' ', $search->term);

				foreach($term as $t)
				{
					$not		= '';
					$operator	= 'OR';
					if(substr($t,0,1) == '-')
					{
						$not		= 'NOT ';
						$operator	= 'AND';
						//trim the - sign off
						$t		= substr($t,1,strlen($t));
					}

					$like	= '';
					$like	.= "( `order_number` ".$not."LIKE '%".$t."%' " ;
					$like	.= $operator." `bill_firstname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `bill_lastname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `ship_firstname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `ship_lastname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `status` ".$not."LIKE '%".$t."%' ";
					$like	.= $operator." `notes` ".$not."LIKE '%".$t."%' )";

					$this->db->where($like);
				}	
			}			
		}

		$this->db->where("is_saved = 1");
		
		return $this->db->count_all_results('taobao');
	}

	function count_products($id)
	{
		return $this->db->select('product_id')->from('category_products')->join('products', 'category_products.product_id=products.id')->where(array('category_id'=>$id, 'enabled'=>1))->count_all_results();
	}

	function get_num_iid($id)
	{
		$result = $this->db->select('num_iid')->where('id', $id)->get('taobao')->row();
		return $result->num_iid;
	}

	function get_product($num_iid)
	{
		$result	= $this->db->get_where('taobao', array('num_iid'=>$num_iid))->row();
		if(!$result)
		{
			return false;
		}
		
		$result->categories = $this->get_product_categories($result->id);
		
		return $result;
	}

	function save($taobao, $options=false, $categories=false)
	{
		$db_product = $this->Taobao_model->get_product($taobao['num_iid']);
			
		if ($db_product && $db_product->num_iid)
		{
			$taobao['id'] = $db_product->id;
			$this->db->where('num_iid', $taobao['num_iid']);
			
			$this->db->update('taobao', $taobao);

			$id	= $taobao['id'];
		}
		else
		{
			$this->db->insert('taobao', $taobao);
			$id	= $this->db->insert_id();
		}

		//loop through the product options and add them to the db
		if($options !== false)
		{
			$obj =& get_instance();
			$obj->load->model('Option_model');

			// wipe the slate
			$obj->Option_model->clear_options($id);

			// save edited values
			$count = 1;
			foreach ($options as $option)
			{
				$values = $option['values'];
				unset($option['values']);
				$option['product_id'] = $id;
				$option['sequence'] = $count;

				$obj->Option_model->save_option($option, $values);
				$count++;
			}
		}
		
		if($categories !== false)
		{
			if($taobao['id'])
			{
				//get all the categories that the product is in
				$cats	= $this->get_product_categories($id);
				
				//eliminate categories that products are no longer in
				foreach($cats as $c)
				{
					if(!in_array($c, $categories))
					{
						$this->db->delete('category_products', array('product_id'=>$id,'category_id'=>$c));
					}
				}
				
				//add products to new categories
				foreach($categories as $c)
				{
					if(!in_array($c, $cats))
					{
						$this->db->insert('category_products', array('product_id'=>$id,'category_id'=>$c));
					}
				}
			}
			else
			{
				//new product add them all
				foreach($categories as $c)
				{
					$this->db->insert('category_products', array('product_id'=>$id,'category_id'=>$c));
				}
			}
		}

		//return the product id
		return $id;
	}
	
	function get_product_categories($id)
	{
		$cats	= $this->db->where('product_id', $id)->get('category_products')->result();
		
		$categories = array();
		foreach ($cats as $c)
		{
			$categories[] = $c->category_id;
		}
		return $categories;
	}
	
	function delete_product($num_iid)
	{
		$product = $this->get_product($num_iid);
		$id = $product->id;

		//delete references in the product to category table
		$this->db->where('product_id', $id);
		$this->db->delete('category_products');

		// delete product 
		$this->db->where('num_iid', $num_iid);
		$this->db->delete('taobao');

		
	}

}