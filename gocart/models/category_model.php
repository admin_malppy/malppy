<?php
Class Category_model extends CI_Model
{

	function get_categories($parent = false)
	{
		if ($parent !== false)
		{
			$this->db->where("parent_id = $parent");
		}
		$this->db->select('id');
		$this->db->order_by('categories.sequence', 'ASC');
		
		//this will alphabetize them if there is no sequence
		$this->db->order_by('name', 'ASC');
		$result	= $this->db->get('categories');
		
		$categories	= array();
		foreach($result->result() as $cat)
		{
			$categories[]	= $this->get_category($cat->id);
		}
		return $categories;
	}
	function get_breadcrumb($id) {						$result = $this->db->query("SELECT t1.name AS level1, t1.slug AS slug1, t2.name as level2, t2.slug AS slug2, t3.name as level3, t3.slug AS slug3, t4.name as level4, t4.slug AS slug4				FROM mp_categories AS t1				LEFT JOIN mp_categories AS t2 ON t2.parent_id = t1.id				LEFT JOIN mp_categories AS t3 ON t3.parent_id = t2.id				LEFT JOIN mp_categories AS t4 ON t4.parent_id = t3.id				WHERE t2.id =".$id." OR t3.id =".$id." OR t4.id =".$id)->row();				return $result;			}
	//this is for building a menu
	function get_categories_tierd($parent=0)	{		$categories	= array();		$result	= $this->get_categories($parent);		foreach ($result as $category)		{			$categories[$category->id]['category']	= $category;			$categories[$category->id]['children']	= $this->get_categories_tierd($category->id);		}		return $categories;	}	function get_all_categories()	{				$result = $this->db->select('*')->from('categories')->get()->result();				return $result;	}	
	function get_categories_tierd_filtered($parent=0) //For front end use only
	
	{
	
		$categories	= array();
	
		$result	= $this->get_categories($parent);
	
		foreach ($result as $category)
	
		{
	
			$counts = $this->count_products($category->id);
	
			if ($counts) {
	
				$categories[$category->id]['category']	= $category;
	
				$categories[$category->id]['children']	= $this->get_categories_tierd_filtered($category->id);
	
			}
	
				
	
		}
	
		return $categories;
	
	}
	function count_products($id)
	{
		return $this->db->select('product_id')->from('category_products')->join('products', 'category_products.product_id=products.id')->where(array('category_id'=>$id, 'enabled'=>1))->count_all_results();
	}
	
	function get_category($id)
	{
		return $this->db->get_where('categories', array('id'=>$id))->row();
	}
	
	function get_sku($id)
	{
		if (!$id) return;
		//if ($this->get_category($id)->counter != 0)
		//{
		//	return $this->get_category($id)->code.str_pad($this->get_category($id)->counter, 5, 0, STR_PAD_LEFT);
		//}
		//else
		//{
			$cat = $this->get_category($id);
			$category['counter'] = $cat->counter + 1;
			$this->db->where('code', $cat->code);
			$this->db->update('categories', $category);
			
			return $this->get_category($id)->code.str_pad($this->get_category($id)->counter, 5, 0, STR_PAD_LEFT);
		//}
	}
	
	function get_category_products_admin($id)
	{
		$this->db->order_by('sequence', 'ASC');
		$result	= $this->db->get_where('category_products', array('category_id'=>$id));
		$result	= $result->result();
		$contents	= array();
		foreach ($result as $product)
		{
			$result2	= $this->db->get_where('products', array('id'=>$product->product_id));
			$result2	= $result2->row();
	
			if ($result2) $contents[]	= $result2;	
		}
		
		return $contents;
	}

	function get_category_products_promo($id)
	{
		$this->db->order_by('sequence', 'ASC');
		$result	= $this->db->get_where('category_products', array('category_id'=>$id));
		$result	= $result->result();
		
		$contents	= array();
		foreach ($result as $product)
		{
			$result2	= $this->db->get_where('products', array('id'=>$product->product_id));
			$result2	= $result2->row();
	
			if ($result2 && $result2->promoprice!="0.00"  && $result2->enabled!="0") $contents[]	= $result2;	
		}
		
		return $contents;
	}

	function get_category_products_taobao($id)
	{
		$this->db->order_by('sequence', 'ASC');
		$result	= $this->db->get_where('category_products', array('category_id'=>$id));
		$result	= $result->result();
		
		$contents	= array();
		foreach ($result as $product)
		{
			$result2	= $this->db->get_where('taobao', array('id'=>$product->product_id));
			$result2	= $result2->row();
	
			if ($result2) $contents[]	= $result2;	
		}
		
		return $contents;
	}

	function get_category_products_empty()
	{
		$this->db->where('sku = ""')->order_by('id', 'DESC');
		$result	= $this->db->get('taobao');
		
		$return = $result->result();
		
		return $return;
	}
	
	function get_category_products($id, $limit, $offset)
	{
		$this->db->order_by('sequence', 'ASC');
		$result	= $this->db->get_where('category_products', array('category_id'=>$id), $limit, $offset);
		$result	= $result->result();
		
		$contents	= array();
		$count		= 1;
		foreach ($result as $product)
		{
			$result2	= $this->db->get_where('products', array('id'=>$product->product_id));
			$result2	= $result2->row();
			
			$contents[$count]	= $result2;
			$count++;
		}
		
		return $contents;
	}
	
	function organize_contents($id, $products)
	{
		//first clear out the contents of the category
		$this->db->where('category_id', $id);
		$this->db->delete('category_products');
		
		//now loop through the products we have and add them in
		$sequence = 0;
		foreach ($products as $product)
		{
			$this->db->insert('category_products', array('category_id'=>$id, 'product_id'=>$product, 'sequence'=>$sequence));
			$sequence++;
		}
	}
	
	function save($category)
	{
		if ($category['id'])
		{
			$this->db->where('id', $category['id']);
			$this->db->update('categories', $category);
			
			return $category['id'];
		}
		else
		{
			$this->db->insert('categories', $category);
			return $this->db->insert_id();
		}
	}
	
	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('categories');
		
		//delete references to this category in the product to category table
		$this->db->where('category_id', $id);
		$this->db->delete('category_products');
	}
}