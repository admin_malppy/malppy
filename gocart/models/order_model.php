<?php
Class order_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	function get_orders($search=false, $sort_by='', $sort_order='DESC', $limit=0, $offset=0, $status='')
	{			
		if ($search)
		{
			if(!empty($search->term))
			{
				//support multiple words
				$term = explode(' ', $search->term);

				foreach($term as $t)
				{
					$not		= '';
					$operator	= 'OR';
					if(substr($t,0,1) == '-')
					{
						$not		= 'NOT ';
						$operator	= 'AND';
						//trim the - sign off
						$t		= substr($t,1,strlen($t));
					}

					$like	= '';
					$like	.= "( `order_number` ".$not."LIKE '%".$t."%' " ;
					$like	.= $operator." `bill_firstname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `bill_lastname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `ship_firstname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `ship_lastname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `status` ".$not."LIKE '%".$t."%' ";
					$like	.= $operator." `notes` ".$not."LIKE '%".$t."%' )";

					$this->db->where($like);
				}	
			}
			if(!empty($search->start_date))
			{
				$this->db->where('ordered_on >=',$search->start_date);
			}
			if(!empty($search->end_date))
			{
				//increase by 1 day to make this include the final day
				//I tried <= but it did not function. Any ideas why?
				$search->end_date = date('Y-m-d', strtotime($search->end_date)+86400);
				$this->db->where('ordered_on <',$search->end_date);
			}
			
		}
		
		if($limit>0)
		{
			$this->db->limit($limit, $offset);
		}
		if(!empty($sort_by))
		{
			$this->db->order_by($sort_by, $sort_order);
		}

		if(!empty($status))
		{
			$this->db->where('status =',$status);
		}

		return $this->db->get('orders')->result();
	}

	function get_orders_by_seller($search=false, $sort_by='', $sort_order='DESC', $limit=0, $offset=0, $status='', $type='')
	{			
		if ($search)
		{
			if(!empty($search->term))
			{
				//support multiple words
				$term = explode(' ', $search->term);

				foreach($term as $t)
				{
					$not		= '';
					$operator	= 'OR';
					if(substr($t,0,1) == '-')
					{
						$not		= 'NOT ';
						$operator	= 'AND';
						//trim the - sign off
						$t		= substr($t,1,strlen($t));
					}

					$like	= '';
					$like	.= "( `order_number` ".$not."LIKE '%".$t."%' " ;
					$like	.= $operator." `bill_firstname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `bill_lastname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `ship_firstname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `ship_lastname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `status` ".$not."LIKE '%".$t."%' ";
					$like	.= $operator." `notes` ".$not."LIKE '%".$t."%' )";

					$this->db->where($like);
				}	
			}
			if(!empty($search->start_date))
			{
				$this->db->where('ordered_on >=',$search->start_date);
			}
			if(!empty($search->end_date))
			{
				//increase by 1 day to make this include the final day
				//I tried <= but it did not function. Any ideas why?
				$search->end_date = date('Y-m-d', strtotime($search->end_date)+86400);
				$this->db->where('ordered_on <',$search->end_date);
			}
			
		}
		
		if($limit>0)
		{
			$this->db->limit($limit, $offset);
		}
		if(!empty($sort_by))
		{
			$this->db->order_by($sort_by, $sort_order);
		}

		if(!empty($status))
		{
			$this->db->where('status =',$status);			
		}

		if(!empty($type))
		{
			if($type=="Current") {
				$this->db->where('status != "Closed"');
			} else if($type=="Existing") {
				$this->db->where('status = "Closed"');
			}
		}
				
		return $this->db->get('orders')->result();
	}

	function get_orders_by_box($id, $sort_by='', $sort_order='DESC', $limit=0, $offset=0, $status='')
	{			
		if(!empty($id))
		{
			$this->db->join('order_box_items', 'orders.id = order_box_items.order_id', 'left')->where('order_box_items.box_id = '.$id);
		} else {
			$this->db->where('id NOT IN (SELECT order_id FROM mp_order_box_items)');
		}

		if($limit>0)
		{
			$this->db->limit($limit, $offset);
		}
		if(!empty($sort_by))
		{
			$this->db->order_by($sort_by, $sort_order);
		}

		if(!empty($status))
		{
			$this->db->where('status =',$status);
		}

		return $this->db->get('orders')->result();
	}

	function get_new_shipping_orders() 
	{

		$this->db->select('orders.id, orders.order_number')->join('shipment_items', 'orders.id = shipment_items.order_id', 'left')->where('shipment_items.order_id is null AND is_delivered = 0');
		$result = $this->db->get('orders')->result();

		return $result;
	}
	
	function get_orders_count($search=false)
	{			
		if ($search)
		{
			if(!empty($search->term))
			{
				//support multiple words
				$term = explode(' ', $search->term);

				foreach($term as $t)
				{
					$not		= '';
					$operator	= 'OR';
					if(substr($t,0,1) == '-')
					{
						$not		= 'NOT ';
						$operator	= 'AND';
						//trim the - sign off
						$t		= substr($t,1,strlen($t));
					}

					$like	= '';
					$like	.= "( `order_number` ".$not."LIKE '%".$t."%' " ;
					$like	.= $operator." `bill_firstname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `bill_lastname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `ship_firstname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `ship_lastname` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `status` ".$not."LIKE '%".$t."%' ";
					$like	.= $operator." `notes` ".$not."LIKE '%".$t."%' )";

					$this->db->where($like);
				}	
			}
			if(!empty($search->start_date))
			{
				$this->db->where('ordered_on >=',$search->start_date);
			}
			if(!empty($search->end_date))
			{
				$this->db->where('ordered_on <',$search->end_date);
			}
			
		}

		if ($search=='today') {
			$this->db->where('ordered_on >=',date("Y-m-d"));
		}
		
		return $this->db->count_all_results('orders');
	}

	function get_items_count($search=false)
	{
		if ($search=='today') {
			$this->db->where('ordered_on >=',date("Y-m-d"));
		}
		
		$item_count = 0;

		// just fetch a list of order id's
		$orders	= $this->db->select('id')->get('orders')->result();
		
		$items = array();
		foreach($orders as $order)
		{
			// get a list of product id's and quantities for each
			$order_items	= $this->db->select('product_id, quantity')->where('order_id', $order->id)->get('order_items')->result_array();
			
			foreach($order_items as $i)
			{
				
				$item_count	+= $i['quantity'];
				
			}
		}

		return $item_count;
	}

	function get_items_summary($search=false)
	{
		if ($search=='today') {
			$this->db->where('ordered_on >=',date("Y-m-d"));
		}

		$items['weight'] = 0;
		$items['price'] = '0.00';

		// just fetch a list of order id's
		$orders	= $this->db->select('id,total')->get('orders')->result();
		
		$items = array();
		foreach($orders as $order)
		{
			// get a list of product id's and quantities for each
			$order_items	= $this->db->select('product_id, quantity')->where('order_id', $order->id)->get('order_items')->result_array();
			
			foreach($order_items as $i)
			{
				
				
				
			}

			foreach($order_items as $key=>$item)
			{
				$product				= $this->db->where('id', $item['product_id'])->get('products')->row();
				if($product)
				{
					$items['weight']	+= $product->weight;
				}
			}
			
			$items['price']	+= $order->total;
		}


		return $items;
	}

	function get_pending_orders_count($search=false)
	{			
		if ($search=='today') {
			$this->db->where('ordered_on >=',date("Y-m-d"));
		}

		$this->db->where('status','Pending');
		
		return $this->db->count_all_results('orders');
	}

	function get_pending_items_count($search=false)
	{
		if ($search=='today') {
			$this->db->where('ordered_on >=',date("Y-m-d"));
		}
		
		$this->db->where('status','Pending');
		
		$item_count = 0;

		// just fetch a list of order id's
		$orders	= $this->db->select('id')->get('orders')->result();
		
		$items = array();
		foreach($orders as $order)
		{
			// get a list of product id's and quantities for each
			$order_items	= $this->db->select('product_id, quantity')->where('order_id', $order->id)->get('order_items')->result_array();
			
			foreach($order_items as $i)
			{
				
				$item_count	+= $i['quantity'];
				
			}
		}

		return $item_count;
	}

	function get_pending_items_summary($search=false)
	{
		if ($search=='today') {
			$this->db->where('ordered_on >=',date("Y-m-d"));
		}

		$this->db->where('status','Pending');

		$items['weight'] = 0;
		$items['price'] = '0.00';

		// just fetch a list of order id's
		$orders	= $this->db->select('id,total')->get('orders')->result();
		
		$items = array();
		foreach($orders as $order)
		{
			// get a list of product id's and quantities for each
			$order_items	= $this->db->select('product_id, quantity')->where('order_id', $order->id)->get('order_items')->result_array();
			
			foreach($order_items as $key=>$item)
			{
				$product				= $this->db->where('id', $item['product_id'])->get('products')->row();
				if($product)
				{
					$items['weight']	+= $product->weight;
				}
			}
			
			$items['price']	+= $order->total;
		}


		return $items;
	}
	
	//get an individual customers orders
	function get_customer_orders($id, $offset=0)
	{
		$this->db->join('order_items', 'orders.id = order_items.order_id');
		$this->db->order_by('ordered_on', 'DESC');
		return $this->db->get_where('orders', array('customer_id'=>$id), 15, $offset)->result();
	}
	
	function count_customer_orders($id)
	{
		$this->db->where(array('customer_id'=>$id));
		return $this->db->count_all_results('orders');
	}
	
	function get_order($id)
	{
		$this->db->where('id', $id);
		$result 			= $this->db->get('orders');
		
		$order				= $result->row();
		$order->contents	= $this->get_items($order->id);
		
		return $order;
	}
	
	function get_items($id)
	{
		$this->db->select('id AS itemid, order_id, batch_id, new_supplier, supplier_type, new_supplier_price, bought, bought_on, arrive, arrive_on, contents');
		$this->db->where('order_id', $id);
		$result	= $this->db->get('order_items');
		
		$items	= $result->result_array();
		
		$return	= array();
		$count	= 0;
		foreach($items as $item)
		{

			$item_content	= unserialize($item['contents']);
			
			//remove contents from the item array
			unset($item['contents']);
			$return[$count]	= $item;
			
			//merge the unserialized contents with the item array
			$return[$count]	= array_merge($return[$count], $item_content);
			
			$count++;
		}
		return $return;
	}

	function get_item($id)
	{
		$this->db->select('id AS itemid, order_id, batch_id, new_supplier, supplier_type, new_supplier_price, bought, bought_on, arrive, arrive_on, contents');
		$this->db->where('id', $id);
		$result	= $this->db->get('order_items');
		
		$item	= $result->row();

		
		return $item;
	}

	function get_bought_status($id)
	{			
		$this->db->where(array('product_id'=>$id,'bought'=>1));
		$result = $this->db->count_all_results('order_items');

		if ($result > 0)  {
			return 1;
		} else {
			return 0;
		}	
		 
	}

	function get_pack_status($id)
	{			
		$this->db->where(array('product_id'=>$id,'pack'=>1));
		$result = $this->db->count_all_results('order_items');

		if ($result > 0)  {
			return 1;
		} else {
			return 0;
		}	
		 
	}
	
	function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('orders');
		
		//now delete the order items
		$this->db->where('order_id', $id);
		$this->db->delete('order_items');
	}

	function delete_shipment($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('shipment');
	}

	function update_paid($data)
	{
		if (isset($data['id']))
		{
			$this->db->where('id', $data['id']);
			$result = $this->db->update('order_items', $data);
			$id = $data['id'];
			//echo $result;
		}
	}

	function update_product_paid($data)
	{
		if (isset($data['product_id']))
		{
			$this->db->where('product_id', $data['product_id']);
			$result = $this->db->update('order_items', $data);
		}
	}

	function update_product_pack($data)
	{
		if (isset($data['id']))
		{
			$this->db->where('id', $data['id']);
			$result = $this->db->update('order_items', $data);
		}
	}

	function update_order_pack($data)
	{
		if (isset($data['id']))
		{
			$this->db->where('id', $data['id']);
			$result = $this->db->update('orders', $data);
		}
	}

	function save_purchase($data)
	{
		if (isset($data['id']))
		{
			$this->db->where('id', $data['id']);
			$this->db->update('order_items', $data);
			return true;
		}		
		return false;
	}

	function save_arrive($data,$batch_id)
	{
		if (isset($data['id']))
		{
			$this->db->where('id', $data['id']);
			$this->db->update('order_items', $data);
			$this->check_export_batch_completion($batch_id);
			return true;
		}		
		return false;
	}
	
	function check_export_batch_completion($id)
	{
		
		if (isset($id))
		{
			$result = $this->db->query("SELECT CASE 
					WHEN NOT EXISTS  (
					SELECT oi.arrive FROM mp_order_items oi JOIN mp_orders o ON o.id = oi.order_id
					WHERE oi.arrive = 0 AND o.batch_id =".$id.") THEN 1 ELSE 0
					END AS completed")->row();
			
			
			if ($result->completed == 1) {
				
				$this->db->query("UPDATE mp_order_batches SET status = 'complete' WHERE id =".$id);
				
			}
		
		}
	}
	
	function save_order($data, $contents = false)
	{
		if (isset($data['id']))
		{
			$this->db->where('id', $data['id']);
			$this->db->update('orders', $data);
			$id = $data['id'];

			// we don't need the actual order number for an update
			$order_number = $id;
		}
		else
		{
			$this->db->insert('orders', $data);
			$id = $this->db->insert_id();
			
			//create a unique order number
			//unix time stamp + unique id of the order just submitted.
			//$order	= array('order_number'=> date('U').$id);
			$order	= array('order_number'=> 'MOA'.str_pad($id, 5, "0", STR_PAD_LEFT));
			
			//update the order with this order id
			$this->db->where('id', $id);
			$this->db->update('orders', $order);
						
			//return the order id we generated
			$order_number = $order['order_number'];
		}
		
		//if there are items being submitted with this order add them now
		if($contents)
		{
			// clear existing order items
			$this->db->where('order_id', $id)->delete('order_items');
			// update order items
			foreach($contents as $item)
			{
				$save				= array();
				$save['contents']	= $item;
				
				$item				= unserialize($item);
				$save['product_id'] = $item['id'];
				$save['quantity'] 	= $item['quantity'];
				$save['order_id']	= $id;
				$this->db->insert('order_items', $save);
			}
		}
		
		return $order_number;

	}
	
	function save_item($data)
	{
		if (isset($data['id']) && $data['id'] != 0)
		{
			$this->db->where('id', $data['id']);
			$this->db->update('items', $data);
			
			return $data['id'];
		}
		else
		{
			$this->db->insert('items', $data);
			return $this->db->insert_id();
		}
	}

	function save_batch($data)
	{
		if (isset($data['id']) && $data['id'] != 0)
		{
			$this->db->where('id', $data['id']);
			$this->db->update('order_batches', $data);
			
			return $data['id'];
		}
		else
		{
			$count = $this->db->count_all_results('order_batches');
			++$count;
			$data['batch_number']	= 'EXP'.str_pad($count, 4, "0", STR_PAD_LEFT);
			$this->db->insert('order_batches', $data);
			return $this->db->insert_id();
		}
	}

	function get_batches()
	{
		return $this->db->order_by('id', 'DESC')->get('order_batches')->result();
	}

	function get_batch($id)
	{
		$this->db->where('id', $id);
		$result 			= $this->db->get('order_batches');
		
		$order				= $result->row();

		
		return $order;
	}

	function get_batch_items($id)
	{
		return $this->db->where('batch_id', $id)->get('orders')->result();
	}
	
	
	
	function get_best_sellers($start, $end)
	{
		if(!empty($start))
		{
			$this->db->where('ordered_on >=', $start);
		}
		if(!empty($end))
		{
			$this->db->where('ordered_on <',  $end);
		}
		
		// just fetch a list of order id's
		$orders	= $this->db->select('id')->get('orders')->result();
		
		$items = array();
		foreach($orders as $order)
		{
			// get a list of product id's and quantities for each
			$order_items	= $this->db->select('product_id, quantity')->where('order_id', $order->id)->get('order_items')->result_array();
			
			foreach($order_items as $i)
			{
				
				if(isset($items[$i['product_id']]))
				{
					$items[$i['product_id']]	+= $i['quantity'];
				}
				else
				{
					$items[$i['product_id']]	= $i['quantity'];
				}
				
			}
		}
		arsort($items);
		
		// don't need this anymore
		unset($orders);
		
		$return	= array();
		foreach($items as $key=>$quantity)
		{
			$product				= $this->db->where('id', $key)->get('products')->row();
			if($product)
			{
				$product->quantity_sold	= $quantity;
				$return[] = $product; // FIX, only show existed product
			}
			else
			{
				//$product = (object) array('sku'=>'Deleted', 'name'=>'Deleted', 'quantity_sold'=>$quantity);
			}
			
			//$return[] = $product;
		}
		
		return $return;
	}

	function forecast()
	{		
		// just fetch a list of order id's
		$orders	= $this->db->select('id')->get('orders')->result();
		
		$items = array();
		foreach($orders as $order)
		{
			// get a list of product id's and quantities for each
			$order_items	= $this->db->select('product_id, quantity')->where('order_id', $order->id)->get('order_items')->result_array();
			
			foreach($order_items as $i)
			{
				
				if(isset($items[$i['product_id']]))
				{
					$items[$i['product_id']]	+= $i['quantity'];
				}
				else
				{
					$items[$i['product_id']]	= $i['quantity'];
				}
				
			}
		}
		arsort($items);
		
		// don't need this anymore
		unset($orders);
		
		$return	= array();
		foreach($items as $key=>$quantity)
		{
			$product				= $this->db->where('id', $key)->get('products')->row();
			if($product)
			{
				$product->quantity_sold	= $quantity;
				$return[] = $product; // FIX, only show existed product
			}
			else
			{
				//$product = (object) array('sku'=>'Deleted', 'name'=>'Deleted', 'quantity_sold'=>$quantity);
			}
			
			//$return[] = $product;
		}
		
		return $return;
	}

	function add_box()
	{
		$count = $this->db->count_all_results('order_boxes');
		++$count;
		$save['box_number']	= 'BOX'.str_pad($count, 4, "0", STR_PAD_LEFT);
		$save['created_on'] = date("Y-m-d H:i:s");
		$save['total_order'] = 	0;
		$save['status'] = 	"new";

		$this->db->insert('order_boxes', $save);
		$id = $this->db->insert_id();
		
		return $save['box_number'];		
	}

	function save_box($data)
	{
		if (isset($data['id']))
		{
			$this->db->where('id', $data['id']);
			$this->db->update('order_boxes', $data);
			$id = $data['id'];

			// we don't need the actual order number for an update
			$box_number = $id;
		}

		return $box_number;
	}

	function save_box_item($data)
	{
		if (isset($data['box_id']) && $data['box_id'] != 0)
		{
			if ($old_box_id = $this->db->where('order_id', $data['order_id'])->get('order_box_items')->row()->box_id) {
				$new['box_id'] = $data['box_id'];
				$this->db->where('order_id', $data['order_id']);
				$this->db->update('order_box_items', $new);

				$box_detail = $this->db->where('box_id', $old_box_id)->get('order_box_items')->result();

				foreach($box_detail AS $orders) {
					$order = $this->Order_model->get_items($orders->order_id);
					$box[$old_box_id]['total_item'] += count($order);
					foreach($order AS $prod) {
						$box[$old_box_id]['quantity'] += $prod['quantity'];
						$box[$old_box_id]['total_weight'] += $prod['weight'];				
					}
				}
	
				$box[$old_box_id]['total_order'] = count($box_detail);
	
				$this->db->where('id', $old_box_id);		
				$this->db->update('order_boxes', $box[$old_box_id]);
	
			} else {		

				$this->db->insert('order_box_items', $data);

			}

			$box_detail = $this->db->where('box_id', $data['box_id'])->get('order_box_items')->result();

			foreach($box_detail AS $orders) {
				$order = $this->Order_model->get_items($orders->order_id);
				$box[$data['box_id']]['total_item'] += count($order);
				foreach($order AS $prod) {
					$box[$data['box_id']]['quantity'] += $prod['quantity'];
					$box[$data['box_id']]['total_weight'] += $prod['weight'];				
				}
			}
	
			$box[$data['box_id']]['total_order'] = count($box_detail);
			$box[$data['box_id']]['status']	= "packing";
	
			$this->db->where('id', $data['box_id']);		
			$this->db->update('order_boxes', $box[$data['box_id']]);

			return $this->db->insert_id();
		}
	}
	
	function get_boxes($search=false)
	{
		if ($search)
		{
			$like	.= "( `box_number` ".$not."LIKE '%".$search."%' )" ;
			$this->db->where($like);	
		}

		return $this->db->get('order_boxes')->result();
	}

	function get_box($id)
	{
		$this->db->where('id', $id);
		$result = $this->db->get('order_boxes');
		
		$box = $result->row();
		
		return $box;
	}

	function get_shipments($search=false)
	{
		if ($search)
		{
			$like	.= "( `shipment_number` ".$not."LIKE '%".$search."%' )" ;
			$this->db->where($like);	
		}
		
		return $this->db->order_by("id", "DESC")->get('shipment')->result();
	}

	function add_shipping($data)
	{
		if (isset($data['id']))
		{
			//$this->db->where('id', $data['id']);
			//$this->db->update('orders', $data);
			//$id = $data['id'];

			// we don't need the actual shipment number for an update
			$shipment_number = $id;
		}
		else
		{
			$save['box_id'] = $data['box_id'];
			$save['shipment_number'] = $data['shipment_number'];
			$save['remark'] = $data['remark'];
			$save['shipped_on'] = $data['shipped_on'];
			$save['total_order'] = 	$data['total_order'];
			$save['total_item'] = 	$data['total_item'];

			$this->db->insert('shipment', $save);
			$id = $this->db->insert_id();

			foreach($data['orders'] as $order_no=>$order)
			{
				$save2['order_id']		= $order->order_id;
				
				$save2['shipment_id']	= $id;

				$detail = $this->get_items($order->order_id);

				$weight = $detail[0]['weight'];
				
				$total_weight += $weight;
				
				$this->add_shipping_item($save2);

				
				$save3['id'] = $data['box_id'];
				$save3['shipment_id'] = $id;
				$save3['status'] = "shipping";
				$this->save_box($save3);
			}
			$save['total_weight'] = $total_weight;
			$this->db->where('id', $id);
			$this->db->update('shipment', $save);
					
			//return the order id we generated
			$shipment_number = $data['shipment_number'];
		}
		
		return $shipment_number;
		
	}

	function add_shipping_item($data)
	{
		$this->db->insert('shipment_items', $data);
	}

	function get_shipment($shipment_id)
	{
		$this->db->where('id =',$shipment_id);
		
		return $this->db->get('shipment')->row();
	}

	function get_shipment_by_box($box_id)
	{
		$this->db->where('box_id =',$box_id);
		
		return $this->db->get('shipment')->row();
	}

	function get_shipment_orders($shipment_id)
	{			
		

		$this->db->where('shipment_id =',$shipment_id);
		
		return $this->db->get('shipment_items')->result();
	}

	function get_shipment_by_order($order_id)
	{
		$this->db->where('order_id =',$order_id);
		
		$shipment_item = $this->db->get('shipment_items')->row();

		if ($shipment_item) {
			$shipment = $this->get_shipment($shipment_item->shipment_id);

			return $shipment;
		}

		return false;
	}

	function save_shipment($data)
	{
		if (isset($data['id']))
		{
			$this->db->where('id', $data['id']);
			$result = $this->db->update('shipment', $data);
		}
	}

	function close_order($data)
	{
		if (isset($data['id']))
		{
			$this->db->where('id', $data['id']);
			$result = $this->db->update('orders', $data);
		}
	}
	
	function get_orders_summary () {
		
		$result = $this->db->query("SELECT p.name, p.sku, SUM(oi.quantity) as total_quantity, mpp.arrived_quantity FROM mp_products p
				JOIN mp_order_items oi ON p.id = oi.product_id
				JOIN mp_orders o ON oi.order_id = o.id
				JOIN mp_order_batches ob ON o.batch_id = ob.id
				LEFT JOIN (SELECT product_id, SUM(quantity) as arrived_quantity FROM mp_order_items WHERE arrive = 1 GROUP BY product_id) AS mpp ON mpp.product_id = p.id
				WHERE ob.status = 'open' AND pack = 0 GROUP BY p.name
				")->result();
		
		
		return $result;
		
		
	}
	
	function get_batch_summary($id)
	{

		$result = $this->db->query("SELECT SUM(p.weight) as total_weight ,COUNT(DISTINCT p.sku) as unique_products, COUNT(DISTINCT p.seller) as unique_sellers FROM mp_orders o
				JOIN mp_order_items oi ON o.id = oi.order_id
				JOIN mp_products p ON oi.product_id = p.id
				WHERE o.batch_id = ".$id)->row();
		return $result;
	
	}
	
	function get_batch_expected_cost($id)
	{
		$result = $this->db->query("SELECT SUM(oi.quantity*p.price) as expected_cost FROM mp_orders o
				JOIN mp_order_items oi ON o.id = oi.order_id
				JOIN mp_products p ON oi.product_id = p.id
				WHERE o.batch_id = ".$id)->row();
		
		return $result;
		
	}
	
	function get_batch_actual_cost($id)
	{
		$result = $this->db->query("SELECT p.price, oi.supplier_type, oi.new_supplier_price, oi.quantity FROM mp_orders o
				JOIN mp_order_items oi ON o.id = oi.order_id
				JOIN mp_products p ON oi.product_id = p.id
				WHERE o.batch_id = ".$id)->result();
		
		$actual_cost = 0;
		
		foreach ($result as $item) {
			
			if ($item->supplier_type == 'Original') {
				
				
				$actual_cost += $item->price;
				
			}
			elseif ($item->supplier_type == 'Others'){
				$actual_cost += $item->new_supplier_price;
			}
			
		}
		
		return $actual_cost;
	}
}